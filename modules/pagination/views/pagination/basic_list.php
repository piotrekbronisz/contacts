	<?php if ($previous_page !== FALSE): ?>
		<li class="previous"><a href="<?php echo $page->url($previous_page) ?>"><?php echo __('Previous') ?></a></li>
	<?php else: ?>
		<li class="previous-off"><?php echo __('Previous') ?></li>
	<?php endif ?>

	<?php for ($i = 1; $i <= $total_pages; $i++): ?>

		<?php if ($i == $current_page): ?>
			<li class="active"><?php echo $i ?></li>
		<?php else: ?>
			<li><a href="<?php echo $page->url($i) ?>"><?php echo $i ?></a></li>
		<?php endif ?>

	<?php endfor ?>

	<?php if ($next_page !== FALSE): ?>
		<li class="next"><a href="<?php echo $page->url($next_page) ?>"><?php echo __('Next') ?></a></li>
	<?php else: ?>
		<li class="next-off"><?php echo __('Next') ?></li>
	<?php endif ?>