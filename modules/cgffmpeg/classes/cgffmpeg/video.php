<?php defined('SYSPATH') or die('No direct script access.');
/**
 *  this CgFFMpeg_Video class cuts / converts / thumbs video sources
 *
 *  @autor      f.fiebig <webpiraten>
 *  @version    2.0
 *  @since      05/2011
 *
 *  first you have to install ffmpeg on your server side system
 *
 *  @see    http://ffmpeg.org/
 *  @see    http://ffmpeg.org/documentation.html
 *
 *  this module is for kohana 3 framework
 *
 *  @see http://kohanaframework.org/
 *  @see http://kohanaframework.org/documentation
 *
 *  checkout this repo into /modules/cgffmpeg/
 *  enable cgffmpeg as a module in your /application/bootstrap.php
 *  once the module is up and running - use like this:
 *
 *  $version    = CgFFMpeg_Video::version(); // get ffmpeg version
 *  $info       = CgFFMpeg_Video::info('test.mp4'); // get file info (source_file)
 *  $thumb      = CgFFMpeg_Video::thumb($source, $target, '00:00:30', '300x220'); // create thumbnail (source_file, target_file, grab_from, image_size)
 *  $clip       = CgFFMpeg_Video::clip($source, $target, '00:00:30', '00:00:30'); // cut a clip (source_file, target_file, start_from, clip_length)
 *  $preset     = CgFFMpeg_Video::preset($source, $target, 'flv_320x180'); // use a preset (source_file, target_file, preset_name_from_config)
 */
class CgFFMpeg_Video
{
    public static function version()
    {
        $retval     = array();
        $version    = explode(PHP_EOL, self::execute('avconv -version'));
        foreach($version AS $value)
        {
            $value = trim($value);
            if(!empty($value))
            {
                $retval[] = $value;
            }
        }
        return $retval;
    }

    public static function info($source)
    {
        if(!is_file($source))
        {
            throw new Exception('File not available: '.$source);
        }

        $result = self::execute('avconv -i '.$source);
        print_r('avconv -i '.$source);
        $search     = '/Duration: (.*?)[.]/';
        preg_match($search, $result, $matches, PREG_OFFSET_CAPTURE);
        $duration   = trim($matches[1][0]);

        $search     = '/Video: (.*?)[\n]/';
        preg_match($search, $result, $matches, PREG_OFFSET_CAPTURE);
        $video      = explode(', ', trim($matches[1][0]));

        $search     = '/Audio: (.*?)[\n]/';
        preg_match($search, $result, $matches, PREG_OFFSET_CAPTURE);
        $audio      = explode(', ', trim($matches[1][0]));

        return array(
            'source'    => $source,
            'video'     => $video,
            'audio'     => $audio,
            'duration'  => $duration,
        );
    }

    public static function thumb($source, $target, $offset = '00:00:50', $format = '100x80')
    {
        $result = self::execute('ffmpeg -y -i '.$source.' -f mjpeg -ss '.$offset.' -vframes 1 -s '.$format.' -an '.$target);
        if(is_file($target))
        {
            return $target;
        }
        throw new Exception('Thumbnail could not be created!');
    }

    public static function clip($source, $target, $offset = '00:00:50', $duration = '00:00:50')
    {
        $result = self::execute('ffmpeg -i '.$source.' -ss '.$offset.' -t '.$duration.' '.$target);
        if(is_file($target))
        {
            return $target;
        }
        throw new Exception('Clip could not be created!');
    }

    public static function preset($source, $target, $preset)
    {
        $presets = Kohana::$config->load('cgffmpeg.presets');
        if(array_key_exists($preset, $presets))
        {
            $command    = $presets[$preset]['command'];
            $extension  = $presets[$preset]['extension'];
            if(!strrchr($target, '.'.$extension))
            {
                $target = $target.'.'.$extension;
            }
            $result = self::execute('ffmpeg -i '.$source.' '.$command.' '.$target);
            if(is_file($target))
            {
                return $target;
            }
            throw new Exception('Converted file could not be created!');
        }
        throw new Exception($preset.' not available!');
    }

    protected static function execute($command)
    {
        ob_start();
        passthru(Kohana::$config->load('cgffmpeg.ffmpeg_path').$command.' 2>&1');
        $retval = ob_get_contents();
        ob_end_clean();
        return $retval;
    }
}