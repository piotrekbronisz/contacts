<?php defined('SYSPATH') or die('No direct script access.');

class Controller_CgFFMpeg extends Controller
{
    public function action_index()
    {
        /* be aware that video conversion can consume a lot of cpu / ram / time usage */

        /* get ffmpeg version info */
        $version = CgFFMpeg_Video::version();
        var_dump($version);

        /* get video file info */
        $info = CgFFMpeg_Video::info(Kohana::$config->load('cgffmpeg.asset_path').'test.mp4');
        var_dump($info);

        /* create thumbnail from video file */
        $source = Kohana::$config->load('cgffmpeg.asset_path').'test.mp4';
        $target = Kohana::$config->load('cgffmpeg.asset_path').'test.jpg';
        $thumb = CgFFMpeg_Video::thumb($source, $target, '00:00:30', '300x220');
        var_dump($thumb);

        /* create preview clip from video file */
        $source = Kohana::$config->load('cgffmpeg.asset_path').'test.mp4';
        $target = Kohana::$config->load('cgffmpeg.asset_path').'test_clip.mp4';
        $clip = CgFFMpeg_Video::clip($source, $target, '00:00:30', '00:00:30');
        var_dump($clip);

        /* use presets to convert video file */
        $source = $clip;
        $target = Kohana::$config->load('cgffmpeg.asset_path').'test_clip_flv_converted.flv';
        $preset = CgFFMpeg_Video::preset($source, $target, 'flv_320x180');
        var_dump($preset);
    }
}
