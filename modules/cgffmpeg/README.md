# CgFFMpeg

CgFFMpeg class convert / cut / thumb video sources

- @autor       f.fiebig <webpiraten>
- @version     1.5
- @since       05/2011

# License

CgFFMpeg is licensed under the terms of MIT. See LICENSE for more information.