<?php defined('SYSPATH') or die('No direct script access.');

/* catch all routes for CgFFMpeg */
Route::set('cgffmpeg', 'cgffmpeg(.*)')
    ->defaults(array(
        'controller' => 'cgffmpeg',
        'action'     => 'index'
    ));
