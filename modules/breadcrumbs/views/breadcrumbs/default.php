<ul id="navi-bar" class="reset-list">
	<?php for($i = 0; $i < $items_count; $i++) : ?>
    <li<?php echo ($items[$i]['current']) ? ' class="active"' : ''; ?>>
    	<?php if($i == ($items_count - 1) && $last_linkable == false) { ?>
    	<a href="<?php echo $items[$i]['url']; ?>"><?php echo $items[$i]['label']; ?></a>
    	<?php } else { ?>
		<a href="<?php echo $items[$i]['url']; ?>"><?php echo $items[$i]['label']; ?></a>
    	<?php } ?>
    </li>
    <?php endfor; ?>
</ul>