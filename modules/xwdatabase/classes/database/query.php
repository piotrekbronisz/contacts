<?php defined('SYSPATH') OR die('No direct script access.');
/**
 * Database query wrapper.  See [Parameterized Statements](database/query/parameterized) for usage and examples.
 *
 * @package    Kohana/Database
 * @category   Query
 * @author     Kohana Team
 * @copyright  (c) 2008-2009 Kohana Team
 * @license    http://kohanaphp.com/license
 */
class Database_Query extends Kohana_Database_Query {

	// Execute the query during a cache hit
	protected $_force_execute = FALSE;

	// Cache lifetime
	protected $_lifetime = NULL;

	protected $_cache_object = NULL;
    
    protected $_cache_tags = NULL;

	/**
	 * Enables the query to be cached for a specified amount of time.
	 *
	 * @param   integer  $lifetime  number of seconds to cache, 0 deletes it from the cache
	 * @param   boolean  whether or not to execute the query during a cache hit
	 * @param   string   $type type of cache driver
	 * @return  $this
	 * @uses    Kohana::$cache_life
	 */
	public function cached($lifetime = NULL, $force = FALSE, $type = NULL)
	{
		if ($lifetime === NULL)
		{
			// Use the global setting
			$lifetime = Kohana::$cache_life;
		}

		$this->_force_execute = $force;
		$this->_lifetime = $lifetime;

		if (! isset($this->_cache_object))
		{

			// TODO: is this the best way to check for the "cache" module?
			$modules = Kohana::modules();
			if (isset($modules['cache']) && !is_null($type))
			{
				// Use the "unofficial" Kohana cache module.
				
                if (is_array($type))
                {
                    $this->_cache_object = Cache::instance($type[0]);
                    
                    $this->_cache_tags = $type[1];
                }
                else
                {
                    $this->_cache_object = Cache::instance($type);
                }
			}
			else
			{
				// Default internal Kohana cache.
				$this->_cache_object = true;
			}
		}

		return $this;
	}


	/**
	 * Execute the current query on the given database.
	 *
	 * @param   mixed    $db  Database instance or name of instance
	 * @param   string   result object classname, TRUE for stdClass or FALSE for array
	 * @param   array    result object constructor arguments
	 * @return  object   Database_Result for SELECT queries
	 * @return  mixed    the insert id for INSERT queries
	 * @return  integer  number of affected rows for all other queries
	 */
	public function execute($db = NULL, $as_object = NULL, $object_params = NULL)
	{
		if ( ! is_object($db))
		{
			// Get the database instance
			$db = Database::instance($db);
		}

		if ($as_object === NULL)
		{
			$as_object = $this->_as_object;
		}

		if ($object_params === NULL)
		{
			$object_params = $this->_object_params;
		}

		// Compile the SQL query
		$sql = $this->compile($db);

		if ($this->_lifetime !== NULL AND $this->_type === Database::SELECT)
		{
			// Set the cache key based on the database instance name and SQL
			$cache_key = 'Database::query("'.$db.'", "'.$sql.'")';
			
			//
			if (is_object($this->_cache_object))
			{
				if (($result = $this->_cache_object->get(sha1($cache_key))) !== NULL
					AND ! $this->_force_execute)
				{
					return new Database_Result_Cached($result, $sql, $as_object, $object_params);
				}
			}
			else
			{
				// Read the cache first to delete a possible hit with lifetime <= 0
				if (($result = Kohana::cache($cache_key, NULL, $this->_lifetime)) !== NULL
					AND ! $this->_force_execute)
				{
					// Return a cached result
					return new Database_Result_Cached($result, $sql, $as_object, $object_params);
				}
			}
		}

		// Execute the query
		$result = $db->query($this->_type, $sql, $as_object, $object_params);

		if (isset($cache_key) AND $this->_lifetime > 0)
		{
			if (is_object($this->_cache_object))
			{
				// Cache the result array
                if ($this->_cache_object instanceof Kohana_Cache_Tagging AND $this->_cache_tags)
                {
                    $this->_cache_object->set_with_tags(sha1($cache_key), $result->as_array(), $this->_lifetime, $this->_cache_tags);
                }
                else
                {
                    $this->_cache_object->set(sha1($cache_key), $result->as_array(), $this->_lifetime);
                }
			}
			else
			{
				// Cache the result array
				Kohana::cache($cache_key, $result->as_array(), $this->_lifetime);
			}
		}

		return $result;
	}

} // End Database_Query
