<?php defined('SYSPATH') or die('No direct script access.');
?>
<ul>
<?php
$level = $nodes->current()->level;

foreach ($nodes as $node) {
    
    if ($node->has_children()) {
        if ($level > $node->level) {
            echo str_repeat("</ul></li>\n", ($level - $node->level));
            if ($node->type == 'static') {
                echo '<li id="item_'.$node->id.'" class="folder"><a href="'.url::site('admin/pages_content/edit/'.$node->id).'">'.$node->title.'</a>'."\n";
            }
            elseif ($node->type == 'module') {
                if ($node->content == 'articles') {
                    echo '<li id="item_'.$node->id.'" class="folder"><a href="'.url::site('admin/pages_articles/index/'.$node->id).'">'.$node->title.'</a>'."\n";
                }
            }
            elseif ($node->type == 'redirect') {
                echo '<li id="item_'.$node->id.'" class="folder"><a href="'.url::site('admin/pages/edit/'.$node->id).'">'.$node->menu_title.'</a>'."\n";
            }
            elseif ($node->type == 'internal') {
                echo '<li id="item_'.$node->id.'" class="folder"><a href="'.url::site('admin/pages/edit/'.$node->id).'">'.$node->title.'</a>'."\n";
            }
            echo '<ul>' . "\n";
        }
        else {
            if ($node->type == 'static') {
                echo '<li id="item_'.$node->id.'" class="folder"><a href="'.url::site('admin/pages_content/edit/'.$node->id).'">'.$node->title.'</a>'."\n";
            }
            elseif ($node->type == 'module') {
                if ($node->content == 'articles') {
                    echo '<li id="item_'.$node->id.'" class="folder"><a href="'.url::site('admin/pages_articles/index/'.$node->id).'">'.$node->title.'</a>'."\n";
                }
            }
            elseif ($node->type == 'redirect') {
                echo '<li id="item_'.$node->id.'" class="folder"><a href="'.url::site('admin/pages/edit/'.$node->id).'">'.$node->title.'</a>'."\n";
            }
            elseif ($node->type == 'internal') {
                echo '<li id="item_'.$node->id.'" class="folder"><a href="'.url::site('admin/pages/edit/'.$node->id).'">'.$node->title.'</a>'."\n";
            }
            echo '<ul>' . "\n";
        }
    }
    elseif ($level > $node->level) {
        echo str_repeat("</ul></li>\n", ($level - $node->level));
        if ($node->type == 'static') {
            echo '<li id="item_'.$node->id.'" class="doc"><a href="'.url::site('admin/pages_content/edit/'.$node->id).'">'.$node->title.'</a></li>'."\n";
        }
        elseif ($node->type == 'module') {
            if ($node->content == 'articles') {
                echo '<li id="item_'.$node->id.'" class="doc"><a href="'.url::site('admin/pages_articles/index/'.$node->id).'">'.$node->title.'</a></li>'."\n";
            }
        }
        elseif ($node->type == 'redirect') {
            echo '<li id="item_'.$node->id.'" class="doc"><a href="'.url::site('admin/pages/edit/'.$node->id).'">'.$node->title.'</a></li>'."\n";
        }
        elseif ($node->type == 'internal') {
            echo '<li id="item_'.$node->id.'" class="doc"><a href="'.url::site('admin/pages/edit/'.$node->id).'">'.$node->title.'</a></li>'."\n";
        }
    }
    else {
        if ($node->type == 'static') {
            echo '<li id="item_'.$node->id.'" class="doc"><a href="'.url::site('admin/pages_content/edit/'.$node->id).'">'.$node->title.'</a></li>'."\n";
        }
        elseif ($node->type == 'module') {
            if ($node->content == 'articles') {
                echo '<li id="item_'.$node->id.'" class="doc"><a href="'.url::site('admin/pages_articles/index/'.$node->id).'">'.$node->title.'</a></li>'."\n";
            }
        }
        elseif ($node->type == 'redirect') {
            echo '<li id="item_'.$node->id.'" class="doc"><a href="'.url::site('admin/pages/edit/'.$node->id).'">'.$node->title.'</a></li>'."\n";
        }
        elseif ($node->type == 'internal') {
            echo '<li id="item_'.$node->id.'" class="doc"><a href="'.url::site('admin/pages/edit/'.$node->id).'">'.$node->title.'</a></li>'."\n";
        }
    }
    
    $level = $node->level;
}

//echo str_repeat("</ul></li>\n", $level);
?>
</ul>