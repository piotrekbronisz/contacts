<?php defined('SYSPATH') or die('No direct script access.');
if (Request::instance()->param('id')) {
    $items = Jelly::select('modules_shortcut')->order_by('id','ASC')->where('item_id','=',Request::instance()->param('id'))->execute();
    $pagesArray = array();
    foreach ($items as $pages) {
        $pagesArray[] = $pages->page_id;
    }
}
else {
    $pagesArray = array();
}

$checked1 = (in_array('-1', $pagesArray)) ? 'class="jstree-checked"' : '';
$checked2 = (in_array('-2', $pagesArray)) ? 'class="jstree-checked"' : '';
$checked3 = (in_array('-3', $pagesArray)) ? 'class="jstree-checked"' : '';
?>
<ul>
<li id="-1"><a href="javascript:void(0);" <?php echo $checked1; ?>>Strona główna</a></a></li>
<li id="-2"><a href="javascript:void(0);" <?php echo $checked2; ?>>Mapa strony</a></a></li>
<li id="-3"><a href="javascript:void(0);" <?php echo $checked3; ?>>Wyszukiwarka</a></a></li>
<?php
$level = $nodes->current()->lvl;

foreach ($nodes as $node) {
    $checked = (in_array($node->id, $pagesArray)) ? 'class="jstree-checked"' : '';
    
    if ($node->has_children()) {
        if ($level > $node->lvl) {
            echo str_repeat("</ul></li>\n", ($level - $node->lvl));
            echo '<li id="'.$node->id.'" rel="'.$node->content->type.'"><a href="'.url::site('admin/pages/edit/'.$node->id).'" '.$checked.'>'.$node->content->menu_title.'</a>'."\n";
            echo '<ul>' . "\n";
        }
        else {
            echo '<li id="'.$node->id.'" rel="'.$node->content->type.'"><a href="'.url::site('admin/pages/edit/'.$node->id).'" '.$checked.'>'.$node->content->menu_title.'</a>'."\n";
            echo '<ul>' . "\n";
        }
    }
    elseif ($level > $node->lvl) {
        echo str_repeat("</ul></li>\n", ($level - $node->lvl));
        echo '<li id="'.$node->id.'" rel="'.$node->content->type.'"><a href="'.url::site('admin/pages/edit/'.$node->id).'" '.$checked.'>'.$node->content->menu_title.'</a></li>'."\n";
    }
    else {
        echo '<li id="'.$node->id.'" rel="'.$node->content->type.'"><a href="'.url::site('admin/pages/edit/'.$node->id).'" '.$checked.'>'.$node->content->menu_title.'</a></li>'."\n";
    }
    
    $level = $node->lvl;
}

?>
</ul>