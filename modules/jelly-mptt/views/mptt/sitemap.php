<?php defined('SYSPATH') or die('No direct script access.');
?>
<ul class="verticalUl" id="map_k1">
    <li><a href="<?php echo URL::site('/'); ?>">Strona główna</a></li>
    <li><a href="<?php echo URL::site('artykul/mapa-strony'); ?>">Mapa strony</a></li>
<?php
$level = $nodes->current()->lvl;

foreach ($nodes as $node) {

    if ($node->has_children()) {
        if ($level > $node->lvl) {
            echo str_repeat("</ul></li>\n", ($level - $node->lvl));
            echo '<li>'."\n";
            if ($node->id <> '8' && $node->id <> '38' && $node->id <> '23') {
                echo '<a href="'.url::site($node->uri()).'">'.$node->content->menu_title.'</a>'."\n";
            }
            echo '<ul class="map_k2">' . "\n";
        }
        else {
            echo '<li>'."\n";
            if ($node->id <> '8' && $node->id <> '38' && $node->id <> '23') {
                echo '<a href="'.url::site($node->uri()).'">'.$node->content->menu_title.'</a>'."\n";
            }
            echo '<ul class="map_k2">' . "\n";
        }
    }
    elseif ($level > $node->lvl) {
        echo str_repeat("</ul></li>\n", ($level - $node->lvl));
        if ($node->id <> '8' && $node->id <> '38' && $node->id <> '23') {
            echo '<li><a href="'.url::site($node->uri()).'">'.$node->content->menu_title.'</a></li>'."\n";
        }
    }
    else {
        if ($node->id <> '8' && $node->id <> '38' && $node->id <> '23') {
            echo '<li><a href="'.url::site($node->uri()).'">'.$node->content->menu_title.'</a></li>'."\n";
        }
    }
    
    $level = $node->lvl;
}

//echo str_repeat("</ul></li>\n", $level);
?>
</ul>