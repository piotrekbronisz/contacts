<?php
class Model_Builder_Tracker extends Jelly_Builder {
    public function filter($term = null) {
        $query = $this;
        
        if (!is_null($term) && is_array($term) && array_filter($term)) {
            $query->where_open();
            
            if (!is_null($term['ip']) && !empty($term['ip'])) {
                $query->and_where('user_ip', 'LIKE', '%'.$term['ip'].'%');
            }

            if (!is_null($term['user']) && !empty($term['user'])) {
                $query->and_where_open()
                    ->where(':user.firstname', 'LIKE', '%'.$term['user'].'%')
                    ->or_where(':user.lastname', 'LIKE', '%'.$term['user'].'%')
                    ->and_where_close();
            }

            
            if (!is_null($term['date_from']) && !empty($term['date_from'])) {
                $query->and_where('created_on', '>=', $term['date_from']);
            }
            
            if (!is_null($term['date_to']) && !empty($term['date_to'])) {
                $query->and_where('created_on', '<=', $term['date_to']);
            }

                
            if (!is_null($term['content']) && !empty($term['content'])) {
                $query->and_where('message', 'LIKE', '%'.$term['content'].'%');
            }
                        

            $query->where_close();
        }
        
        return $query;
    }
}