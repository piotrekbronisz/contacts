<?php

class Model_Tracker extends Jelly_Model {
    public static function initialize(Jelly_Meta $meta) {
    	$meta->sorting(array('id' => 'DESC'))
        ->fields(array(
    		'id' => Jelly::field('primary'),
    		'user_ip' => Jelly::field('string'),
    		'created_on' => Jelly::field('string', array(
                'default' => date('Y-m-d H:i:s', time())
            )),
            'post' => Jelly::field('text'),
            'get' => Jelly::field('text'),
            'message' => Jelly::field('text'),
    		'user' => Jelly::field('belongsto')
        ));
    }
}