<?php defined('SYSPATH') OR die('No direct access allowed.');

/**
 * Action tracker core class.
 *
 * @package    Tracker
 * @author     Michał Młodziński
 * @copyright  (c) 2008-20010 xweb//software
 */
class Tracker_Core {
    protected static $_instance;
    
    public static function instance() {
		if (!isset(self::$_instance)) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}
    
    public static function track($message = NULL) {
        $track = Jelly::factory('tracker');
        $track->set(array(
            'user'    => Session::instance()->get('auth_user')->id,
            'user_ip' => $_SERVER['REMOTE_ADDR'],
            'post'    => (!empty($_POST)) ? Debug::dump($_POST) : '',
            'get'     => (!empty($_GET)) ? Debug::dump($_GET) : '',
            'message' => $message
        ))->save();
	}
}