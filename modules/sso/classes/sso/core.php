<?php defined('SYSPATH') or die('No direct access allowed.');
/**
 * Single Sign On module for Kohana Auth
 *
 * @package     SSO
 * @author      creatoro
 * @copyright   (c) 2010 creatoro
 * @credits		Geert De Deckere
 * @license     http://creativecommons.org/licenses/by-sa/3.0/legalcode
 */
abstract class SSO_Core {

	// SSO parameters
	protected $sso_service;
	protected $sso_config;

	public function __construct()
	{
		// Set SSO config
		$this->sso_config = Kohana::$config->load('sso.'.$this->sso_service);
        
        // Include Hybrid Auth
		require_once Kohana::find_file('vendor', 'hybridauth/Hybrid/Auth');
        require_once Kohana::find_file('vendor', 'hybridauth/Hybrid/Endpoint');
	}

	/**
	 * Return a new SSO object
	 *
	 * @param   string  name of the OAuth provider
	 * @param   string  name of ORM
	 * @return  object  SSO object
	 */
	public static function factory($provider, $orm)
	{
		$class = 'SSO_Service_'.$provider.'_'.$orm;

		return new $class;
	}

	abstract public function login($callback);

	abstract protected function complete_login();

} // End SSO_Core