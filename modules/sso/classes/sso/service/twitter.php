<?php defined('SYSPATH') or die('No direct access allowed.');

abstract class SSO_Service_Twitter extends SSO_Core {

	// SSO parameters
	protected $sso_service = 'twitter';
    
    protected $sso_auth;

	public function __construct()
	{
		parent::__construct();
        
        $config = array(
            //"base_url" => Kohana::config('sso.facebook.callback'),
            'base_url' => URL::site('user/endpoint', 'http'),
            'providers' => array ( 
                'Twitter' => array ( 
                    'enabled' => true,
                    'keys'    => array(
                        'key' => 'uOtS5otrfClj9hAfq3sgsw',
                        'secret' => '8jkXborvxTUME19SJow2708ArWLE1OiXvurYIeECF0Q'
                    )
                )
            )
        );
        
        $this->sso_auth = new Hybrid_Auth( $config );
	}

	/**
	 * Attempt to log in a user by using an OAuth provider
	 *
	 * @return  boolean
	 */
	public function login($callback)
	{
        
        $this->sso_auth->authenticate( 'Twitter', array(
            'hauth_return_to' => $callback
        ));
		
        if (Hybrid_Auth::isConnectedWith('Twitter')) {
			return $this->complete_login($this->sso_service);
		}
		else {
			return FALSE;
		}
	}

}