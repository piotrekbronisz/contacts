<?php defined('SYSPATH') or die('No direct access allowed.');

abstract class SSO_Service_Facebook extends SSO_Core {

	// SSO parameters
	protected $sso_service = 'facebook';
    
    protected $sso_auth;

	public function __construct()
	{
		parent::__construct();
        
        $config = array(
            //"base_url" => Kohana::config('sso.facebook.callback'),
            'base_url' => URL::site('user/endpoint', 'http'),
            'providers' => array ( 
                'Facebook' => array ( 
                    'enabled' => true,
                    'keys'   => array(
                        'id' => Kohana::$config->load('cms.facebook.keys.id'),
                        'secret' => Kohana::$config->load('cms.facebook.keys.secret')
                    ),
                    'scope' => 'email'
                )
            ),
		    "debug_mode" => false,
		    "debug_file" => "hybridauth.log",
        );
        
        $this->sso_auth = new Hybrid_Auth( $config );
	}

	/**
	 * Attempt to log in a user by using an OAuth provider
	 *
	 * @return  boolean
	 */
	public function login($callback)
	{
        
        $this->sso_auth->authenticate( 'Facebook', array(
            'hauth_return_to' => $callback
        ));
		
        if (Hybrid_Auth::isConnectedWith('Facebook'))
        {
			return $this->complete_login($this->sso_service);
		}
		else
        {
			return FALSE;
		}
	}
}