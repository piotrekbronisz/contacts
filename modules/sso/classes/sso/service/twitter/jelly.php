<?php defined('SYSPATH') or die('No direct access allowed.');

class SSO_Service_Twitter_Jelly extends SSO_Service_Twitter {

	/**
	 * Complete the login
	 *
	 * @return  boolean
	 */
	protected function complete_login()
	{
		if (Hybrid_Auth::isConnectedWith('Twitter')) {
            // Get user details
            $data = Hybrid_Auth::getAdapter('Twitter')->getUserProfile();
        }
        else {
            return false;
        }

        // Set provider field
        $provider_field = $this->sso_service . '_id';

        // Check whether that id exists in our users table (provider id field)
        $user = Jelly::query('user_sso_jelly')
            ->where($provider_field, '=', $data->identifier)
            ->or_where('email', '=', $data->email)
            ->limit(1)
            ->select();

        // Signup if necessary
        Jelly::factory('user_sso_jelly')->signup_sso($user, $data, $provider_field);

        // Give the user a normal login session
        Auth::instance()->force_login_sso($user->{$provider_field}, $this->sso_service);

        return true;
	}

} // End SSO_Service_Twitter_Jelly