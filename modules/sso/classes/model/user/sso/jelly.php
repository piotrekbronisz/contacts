<?php defined('SYSPATH') or die ('No direct script access.');
/**
 * Jelly auth user for saving users without required fields
 *
 * @package     Jelly/SSO
 * @author      creatoro
 * @copyright   (c) 2010 creatoro
 * @license     http://creativecommons.org/licenses/by-sa/3.0/legalcode
 */
class Model_User_SSO_Jelly extends Model_Auth_User
{
	public static function initialize(Jelly_Meta $meta)
    {
        $meta->table('users');

        // Fields defined by the model
        $meta->fields(array(
            'id' 	=> Jelly::field('primary'),
			'email' => Jelly::field('email', array(
				'unique' => TRUE,
			)),
            'username' => Jelly::field('string'),
            'firstname' => Jelly::field('string'),
            'lastname' => Jelly::field('string'),
			'twitter_id' => Jelly::field('string', array(
				'unique' => TRUE,
				'default' => NULL,
			)),
			'facebook_id' => Jelly::field('string', array(
				'unique' => TRUE,
				'default' => NULL,
			)),
            'yahoo_id' => Jelly::field('string', array(
                'unique' => TRUE,
                'default' => NULL
            )),
            'google_id' => Jelly::field('string', array(
                'unique' => TRUE,
				'default' => NULL,
            )),
            'nk_id' => Jelly::field('string', array(
                'unique' => TRUE,
                'default' => NULL,
            )),
            'icon' => Jelly::field('string'),
            'created_on' => Jelly::field('timestamp', array(
                'format' => 'Y-m-d',
                'auto_now_create' => TRUE,
                'auto_now_update' => FALSE
            )),
            'active' => Jelly::field('integer', array(
                'default' => 1
            ))
        ));
    }
    
    /**
	 * Sign-up using data from OAuth provider
	 * Override this method to add your own sign up process
	 *
	 * @param   object  user
	 * @param   array   available data (provider id, email, etc.)
	 * @param   string  the field of the OAuth provider
	 * @return  object
	 */
	public function signup_sso($user, $data, $provider_field) {
		if ( ! $user->loaded())
		{
			// Add user
			$user->{$provider_field} = $data->identifier;

			// Set email if it's available via OAuth provider
			if (isset($data->email))
			{
				$user->email = $data->email;
                $user->username = $data->email;
			}
            
            // Set firstname if it's available via OAuth provider
			if (isset($data->firstName))
			{
				$user->firstname = $data->firstName;
			}
            
            // Set lastname if it's available via OAuth provider
            if (isset($data->lastName))
			{
				$user->lastname = $data->lastName;
			}
            
            if (isset($data->country))
            {
                //echo $data->country;
                //exit;
            }

            if (isset($data->photoURL))
            {
                $filename = Text::random('alnum', 8);
                $user->icon = 'upload/avatars/'.$filename.'.jpg';

                File::download_remote($data->photoURL, DOCROOT.'upload/avatars/'.$filename.'.jpg');
            }
            
            $user->registered = date('Y-m-d H:i:s');
			$user->save();
		}
		elseif ($user->loaded() AND empty($user->{$provider_field}))
		{
			// If user is found, but provider id is missing add it to details.
			// We can do this merge, because this means user is found by email address,
			// that is already confirmed by this OAuth provider, so it's considered trusted.
			$user->{$provider_field} = $data->identifier;
            
            // Set email if it's available via OAuth provider
			if (isset($data->email))
			{
				$user->email = $data->email;
                $user->username = $data->email;
			}
            
            // Set firstname if it's available via OAuth provider
			if (isset($data->firstName))
			{
				$user->firstname = $data->firstName;
			}
            
            // Set lastname if it's available via OAuth provider
            if (isset($data->lastName))
			{
				$user->lastname = $data->lastName;
			}
            
            if (isset($data->country))
            {
                //echo $data->country;
                //exit;
            }

            if (isset($data->photoURL))
            {
                $filename = Text::random('alnum', 8);
                $user->icon = 'upload/avatars/'.$filename.'.jpg';

                File::download_remote($data->photoURL, DOCROOT.'upload/avatars/'.$filename.'.jpg');
            }

			$user->save();
		}
        elseif ($user->loaded() AND ! empty($user->{$provider_field}))
        {
            // If user is found, but provider id is missing add it to details.
            // We can do this merge, because this means user is found by email address,
            // that is already confirmed by this OAuth provider, so it's considered trusted.
            $user->{$provider_field} = $data->identifier;

            // Set firstname if it's available via OAuth provider
            if (isset($data->firstName))
            {
                $user->firstname = $data->firstName;
            }

            // Set lastname if it's available via OAuth provider
            if (isset($data->lastName))
            {
                $user->lastname = $data->lastName;
            }

            if (isset($data->country))
            {
                //echo $data->country;
                //exit;
            }

            if (isset($data->photoURL))
            {
                $filename = Text::random('alnum', 8);
                $user->icon = 'upload/avatars/'.$filename.'.jpg';

                File::download_remote($data->photoURL, DOCROOT.'upload/avatars/'.$filename.'.jpg');
            }

            $user->save();
        }
    }

} // End Model_User_SSO_Jelly