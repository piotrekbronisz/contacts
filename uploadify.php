<?php
ini_set('max_input_time', 500);
set_time_limit(0);

$filename	= $_FILES['Filedata']['name'];
$temp_name	= $_FILES['Filedata']['tmp_name'];
$error		= $_FILES['Filedata']['error'];
$size		= $_FILES['Filedata']['size'];

if (! $error)
{
    $directory = '';

    if (isset($_POST['directory']))
    {
        $directory = '/'.$_POST['directory'];
    }

    //Get file extension
    $extension = pathinfo($filename, PATHINFO_EXTENSION);

    //Get filename withour extension
    $filename_without_extension = pathinfo($filename, PATHINFO_FILENAME);

    //Generate a unique filename based on the date and add file extension of the uploaded file
    $filename = sprintf('%s/%s/%s/%s/%s_%s.%s', 'upload'.$directory, date('Y'), date('m'), date('d'), $filename_without_extension, uniqid(), $extension);

    //Get upload path
    $path = pathinfo($filename, PATHINFO_DIRNAME);

    if (! is_dir($path))
    {
        $old = umask(0);
        mkdir($path, 0777, TRUE);
        umask($old);
    }

    copy($temp_name, $filename);

    $old = umask(0);
    chmod($filename, 0777);
    umask($old);

    echo $filename;
}