<?php defined('SYSPATH') or die('No direct script access.');

Route::set('module/news/read', 'module/news/<category>/<id>,<slug>', array('category' => '[0-9]+', 'id' => '[0-9]+', 'slug' => '.+'))
	->defaults(array(
		'controller' => 'news',
		'action' => 'read',
        'category' => NULL,
        'id' => NULL,
		'slug' => NULL,
	));

Route::set('module/news', 'module/news/<category>', array('category' => '[0-9]+'))
	->defaults(array(
		'controller' => 'news',
		'action' => 'list',
        'category' => NULL
	));