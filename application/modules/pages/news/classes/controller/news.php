<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Controller_News
 * @version 3.3
 * @author Piotr Bronisz <piotrekbronisz@gmail.com>
 */
class Controller_News extends Relio_Controller_Website {
    /**
     * Lista
     * @author Piotr Bronisz <piotrekbronisz@gmail.com>
     */
    public function action_list()
    {
    	$category = $this->request->param('category');

    	$this->content = View::factory('frontend/pages/news/list.tpl')
            ->bind('breadcrumbs', $breadcrumbs)
            ->bind('page', $page)
            ->bind('years', $years)
            ->bind('items', $items)
            ->bind('pagination', $pagination);

        $items = Jelly::query('pages_news')
            ->by_page($category)
            ->order_by('publish_on', 'DESC')
            ->filter(array(
                'active' => 1
            ))
            ->select();

        $years = array();
        foreach($items as $it) {
            $years[date('Y', strtotime($it->publish_on))] = date('Y', strtotime($it->publish_on));
        }

        $builder = Jelly::query('pages_news')
            ->where('publish_on', '<=', date('Y-m-d'))
            ->by_page($category)
            ->order_by('publish_on', 'DESC')
            ->filter(array(
                'active' => 1,
                'year' => Request::$initial->query('year')
            ));
        
        $limit = 30;
        $pagination = Pagination::factory();
        $pagination->setup(array(
              'total_items' => $builder->count(),
              'items_per_page' => $limit
        ));
        $items = $builder->offset($pagination->offset)->limit($limit)->select();
		
		$page = Jelly::query('page', $category)->select();
        
        //naglowek
        View::set_global('page_header', $page->header);

        if ($page->level == 1)
        {
            View::set_global('top_page_header', $page->header);
        }
        else
        {
            foreach ($page->parents() as $parent) {
                if ($parent->level == 1)
                {
                    View::set_global('top_page_header', $parent->header);
                }
            }
        }

        //Okruszki
        $breadcrumb = Breadcrumbs::getInstance()
            ->setView('frontend/breadcrumbs');

        foreach($page->path(FALSE) as $path) {
            $breadcrumb->addItem($path->title, URL::site($path->uri()));
        }

        $breadcrumbs = $breadcrumb->render();
        
        $this->title = $page->title;
        $this->description = (($page->description) ? $page->description : CFG_META_DESC);
        $this->keywords = (($page->keywords) ? $page->keywords : CFG_META_KEYS);
	}

    /**
     * Czytanie
     * @author Piotr Bronisz <piotrekbronisz@gmail.com>
     */
    public function action_read()
    {
        $category = $this->request->param('category');
		$slug = $this->request->param('slug');

		$this->content = View::factory('frontend/pages/news/read.tpl')
            ->bind('breadcrumbs', $breadcrumbs)
            ->bind('page', $page)
            ->bind('item', $item)
            ->bind('gallery', $gallery)
            ->bind('attachments', $attachments);
            
        $item = Jelly::query('pages_news', $this->request->param('id'))
            ->select();
        
        if ($item->loaded())
        {
            $gallery = Jelly::query('pages_news_gallery')
                ->where('news_id', '=', $item->id)
                ->select();

            $attachments = Jelly::query('attachment')
                ->order_by('posy','ASC')
                ->filter($item->id, 'news')
                ->select();
			
            $page = Jelly::query('page', $category)->select();
        
            //naglowek
            View::set_global('page_header', $page->header);

            if ($page->level == 1)
            {
                View::set_global('top_page_header', $page->header);
            }
            else
            {
                foreach ($page->parents() as $parent) {
                    if ($parent->level == 1)
                    {
                        View::set_global('top_page_header', $parent->header);
                    }
                }
            }

            //Okruszki
            $breadcrumb = Breadcrumbs::getInstance()
                ->setView('frontend/breadcrumbs');

            foreach($page->path(FALSE) as $path) {
                $breadcrumb->addItem($path->title, URL::site($path->uri()));
            }

            $breadcrumbs = $breadcrumb->render();
            
            $this->title = (($item->meta_title) ? $item->meta_title : $item->name);;
            $this->description = (($item->meta_description) ? $item->meta_description : CFG_META_DESC);
            $this->keywords = (($item->meta_keywords) ? $item->meta_keywords : CFG_META_KEYS);
        }
        else
        {
            $this->request->redirect('/');
        }
	}
}