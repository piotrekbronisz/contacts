<?php defined('SYSPATH') or die('No direct script access.');

Route::set('module/faq/read', 'module/faq/<category>/<id>,<slug>', array('category' => '[0-9]+', 'id' => '[0-9]+', 'slug' => '.+'))
    ->defaults(array(
        'controller' => 'faq',
        'action' => 'read',
        'category' => NULL,
        'id' => NULL,
        'slug' => NULL,
    ));

Route::set('module/faq', 'module/faq/<category>', array('category' => '[0-9]+'))
    ->defaults(array(
        'controller' => 'faq',
        'action' => 'list',
        'category' => NULL
    ));