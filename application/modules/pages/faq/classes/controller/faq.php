<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Controller_Faq
 * @version 2.3
 * @author Piotr Bronisz <piotrekbronisz@gmail.com>
 */
class Controller_Faq extends Relio_Controller_Website {
    /**
     * Wyświetlanie widoku
     * @author Piotr Bronisz <piotrekbronisz@gmail.com>
     */
    public function action_list()
    {
        $category = $this->request->param('category');

        $this->content = View::factory('frontend/pages/faq/list.tpl')
            ->bind('breadcrumbs', $breadcrumbs)
            ->bind('page', $page)
            ->bind('items', $items)
            ->bind('pagination', $pagination);

        $builder = Jelly::query('pages_faq')
            ->by_page($category)
            ->filter(array(
                'active' => 1
            ));

        $limit = 999;

        $pagination = Pagination::factory();
        $pagination->setup(array(
            'total_items' => $builder->count(),
            'items_per_page' => $limit
        ));
        $items = $builder->offset($pagination->offset)->limit($limit)->select();

        $page = Jelly::query('page', $category)->select();

        //naglowek
        View::set_global('page_header', $page->header);

        if ($page->level == 1)
        {
            View::set_global('top_page_header', $page->header);
        }
        else
        {
            foreach ($page->parents() as $parent) {
                if ($parent->level == 1)
                {
                    View::set_global('top_page_header', $parent->header);
                }
            }
        }

        //Okruszki
        $breadcrumb = Breadcrumbs::getInstance()
            ->setView('frontend/breadcrumbs');

        foreach($page->path(FALSE) as $path) {
            $breadcrumb->addItem($path->title, URL::site($path->uri()));
        }

        $breadcrumbs = $breadcrumb->render();

        $this->title = $page->title;
        $this->description = $page->description ? $page->description : CFG_META_DESC;
        $this->keywords = $page->keywords ? $page->keywords : CFG_META_KEYS;
    }
}