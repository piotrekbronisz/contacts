<?php defined('SYSPATH') or die('No direct script access.');

Route::set('module/articles/read', 'module/articles/<category>/<id>,<slug>', array('category' => '[0-9]+', 'id' => '[0-9]+', 'slug' => '.+'))
	->defaults(array(
		'controller' => 'articles',
		'action' => 'read',
        'category' => NULL,
        'id' => NULL,
		'slug' => NULL,
	));

Route::set('module/articles', 'module/articles/<category>', array('category' => '[0-9]+'))
	->defaults(array(
		'controller' => 'articles',
		'action' => 'list',
        'category' => NULL
	));