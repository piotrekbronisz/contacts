<?php defined('SYSPATH') or die('No direct script access.');

Route::set('module/contact', 'module/contact/<category>', array('category' => '[0-9]+'))
	->defaults(array(
		'controller' => 'contact',
		'action' => 'list',
        'category' => NULL
	));