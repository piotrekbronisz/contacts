<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Controller_Contact
 * @version 2.3
 * @author Piotr Bronisz <piotrekbronisz@gmail.com>
 */
class Controller_Contact extends Relio_Controller_Website {
    /**
     * Wyświetlanie widoku
     * @author Piotr Bronisz <piotrekbronisz@gmail.com>
     */
    public function action_list()
    {
    	$category = $this->request->param('category');

    	$this->content = View::factory('frontend/pages/contact/list.tpl')
            ->bind('breadcrumbs', $breadcrumbs)
            ->bind('errors', $errors)
            ->bind('page', $page)
            ->bind('subjects', $subjects)
            ->bind('item', $item)
            ->bind('gmap', $gmap);

        $subjects = Jelly::query('pages_contact_subject')
            ->where('page_id', '=', $category)
            ->select();

        $item = Jelly::query('pages_contact')
            ->where('page_id', '=', $category)
            ->limit(1)
            ->select();
        
        $gmap = Gmap::factory(array(
            'zoom' => 12,
            'sensor' => FALSE,
   		))
        ->add_marker('Lokalizacja', $item->google_lat, $item->google_long)
        ->set_pos($item->google_lat, $item->google_long)
    	->set_gmap_size('100%', 360);
        
        if ($_POST)
        {
            $post = Validation::factory($_POST)
                ->rule('name', 'not_empty')
                ->rule('email', 'not_empty')
                ->rule('email', 'email')
                ->rule('subject', 'not_empty')
                ->rule('content', 'not_empty');
            
            if ($post->check())
            {
                $item = Jelly::factory('pages_contact_history');
                $item->subject = Security::xss_clean(Request::initial()->post('subject'));
                $item->name = Security::xss_clean(Request::initial()->post('name'));
                $item->email = Security::xss_clean(Request::initial()->post('email'));
                $item->phone = Security::xss_clean(Request::initial()->post('phone'));
                $item->content = Security::xss_clean(Request::initial()->post('content'));
                $item->file = Security::xss_clean(Request::initial()->post('file_upload_hidden'));
                $item->page = $category;
                $item->save();
                
                xwContentTemplateWrapper::instance()
                    ->setParameters(array(
                        '%name%'    => Security::xss_clean(Request::initial()->post('name')),
                        '%email%'   => Security::xss_clean(Request::initial()->post('email')),
                        '%phone%'   => Security::xss_clean(Request::initial()->post('phone')),
                        '%subject%' => $item->subject->name,
                        '%content%' => Security::xss_clean(Request::initial()->post('content'))
                    ))
                    ->setTemplate('contact')
                    ->setFrom(CFG_WEBMASTER_EMAIL)
                    ->setTo(CFG_WEBMASTER_EMAIL)
                    ->send();
                
                Hint::set(Hint::SUCCESS, 'Wiadomość została wysłana pomyślnie');

                $this->request->redirect(Request::initial()->uri());
            }
            else
            {
                $errors = $post->errors('frontend/pages/contact/form');
            }
        }
		
        
        $page = Jelly::query('page', $category)->select();
        
        //naglowek
        View::set_global('page_header', $page->header);

        if ($page->level == 1)
        {
            View::set_global('top_page_header', $page->header);
        }
        else
        {
            foreach ($page->parents() as $parent) {
                if ($parent->level == 1)
                {
                    View::set_global('top_page_header', $parent->header);
                }
            }
        }

        //Okruszki
        $breadcrumb = Breadcrumbs::getInstance()
            ->setView('frontend/breadcrumbs');

        foreach($page->path(FALSE) as $path) {
            $breadcrumb->addItem($path->title, URL::site($path->uri()));
        }

        $breadcrumbs = $breadcrumb->render();
        
        $this->title = $page->title;
        $this->description = $page->description ? $page->description : CFG_META_DESC;
        $this->keywords = $page->keywords ? $page->keywords : CFG_META_KEYS;
	}
}