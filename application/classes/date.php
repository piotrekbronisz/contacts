<?php defined('SYSPATH') or die('No direct script access.');
 
class Date extends Kohana_Date {
    public static function data($data = NULL) {
        if ($data === NULL || $data == '0000-00-00 00:00:00') {
            return '---';
        }
        
        $data  = strtotime($data);
        $data2 = mktime(0,0,0, date('m',$data), date('j',$data), date('Y',$data));
        
        if (date('Ymd') == date('Ymd', $data2)) {
            return 'dzisiaj, '.date('G:i', $data);
        }
        elseif (strtotime('-2 day') < $data2) {
            return 'wczoraj, '.date('G:i', $data);
        }
        elseif (strtotime('-7 day') < $data2) {
            $dni = floor((time()-$data2)/(60*60*24));
            
            return $dni.' '.text::odmiana($dni, 'dzień', 'dni', 'dni').' temu';
        }
        elseif (strtotime('-1 month') < $data2) {
            $tygodni = floor((time()-$data2)/(60*60*24*7));
            
            if (!$tygodni) {
                $tygodni = 1;
            }
            
            $out = text::odmiana($tygodni, 'tydzień', 'tygodnie', 'tygodni').' temu';
            
            return ($tygodni == 1) ? $out : $tygodni.' '.$out;
        }
        elseif (strtotime('-1 year') < $data2) {
            $miesiecy = floor((time()-$data2)/(60*60*24*30));
            
            if (!$miesiecy) {
                $miesiecy = 1;
            }
            
            $out = text::odmiana($miesiecy, 'miesiąc', 'miesiące', 'miesięcy').' temu';
            
            return ($miesiecy == 1) ? $out : $miesiecy.' '.$out; 
        }
        else {
            $lat = date::age($data2);
            
            if (!$lat) {
                $lat = 1;
            }
            
            $out = text::odmiana($lat, 'rok', 'lata', 'lat');
            
            return ($lat == 1) ? $out : $lat.' '.$out;
        }
        
        return date('Y-m-d H:i:s', $data);
    }
    
    public static function future_date($data = NULL) {
        if($data === NULL || $data=='0000-00-00 00:00:00') {
            return '---';
        }
        
        $data = strtotime($data);
        $now = time();
        
        if($data<=$now) {
            return 0;
        }
        
        $dzien = 24*60*60;
        $tydzien = $dzien*7;
        $miesiac = $dzien*30;
        $rok = $dzien*356;
        
        if ($data <= ($now+$dzien)) {
            return date::seconds2Time($data-$now);
        }
        
        //do tygodnia
        if ($data <= ($now+$tydzien)) {
            $dni = floor(($data-$now)/$dzien);
          
            return $dni.' '.text::odmiana($dni, 'dzień', 'dni', 'dni');
        }
        
        //do miesiaca
        if ($data <= ($now+$miesiac)) {
            $tygodni =  floor(($data-$now)/$tydzien);
            
            $out = text::odmiana($tygodni, 'tydzień', 'tygodnie', 'tygodni');
            
            return $tygodni.' '.$out;
        }
        
        //do roku
        if ($data <= ($now+$rok)) {
            $miesiecy = floor(($data-$now)/$miesiac);
            
            if (!$miesiecy) {
               $miesiecy = 1;
            }
            
            $out = text::odmiana($miesiecy, 'miesiąc', 'miesiące', 'miesięcy');
            
            return $miesiecy.' '.$out;
        }
        else {
            $lat = floor(($data-$now)/$rok);
            
            if (!$lat) {
                $lat = 1;
            }
            
            $out = text::odmiana($lat, 'rok', 'lata', 'lat');
            
            return $lat.' '.$out;
        }
    }
    
    public static function age($data = NULL)
    {
        if ($data === NULL)
        {
            return '-';
        }
        
        $data = strtotime($data);
        
        return  date('Y') - date('Y',$data) - ( (float)date('m.d') < (float)date('m.d',$data) );
    }
    
    public static function seconds2time($sekundy)
    {
        $data['godzin'] =  floor($sekundy/3600);
		$sekundy -= $data['godzin']*3600;
		$data['minut'] =  floor($sekundy/60);
		$sekundy -= $data['minut']*60;
		$data['sekund'] =  $sekundy;

		foreach($data as $k => $v) {
			$data[$k] = str_pad($v, 2, 0, STR_PAD_LEFT);
		}

		return implode(':',$data);
	}
    
    public static function datediff($start, $end)
    {
        $uts['start'] = $start;
        $uts['end'] = $end;
        
        if( !empty($uts['start']) && !empty($uts['end']))
        {
            if ($uts['end'] >= $uts['start'])
            {
                //$diff = $uts['end']–$uts['start'];
                $diff = $uts['end']-$uts['start'];
                if( $days=intval((floor($diff/86400))) )
                {
                    $diff = $diff % 86400;
                }
                
                if( $hours=intval((floor($diff/3600))) )
                {
                    $diff = $diff % 3600;
                }
                
                if( $minutes=intval((floor($diff/60))) )
                {
                    $diff = $diff % 60;
                }
                
                $diff = intval( $diff );
                
                return( array('days' => $days, 'hours' => $hours, 'minutes' => $minutes, 'seconds' => $diff) );
            }
            else
            {
                return 'Koniec jest mniejszy od start';
            }
        }
        else
        {
            return '-3';
        }
        
        return false;
    }

    public static function get_days($start, $end)
    {  
        // Vars
        $day = 86400; // Day in seconds
        $format = 'Y-m-d'; // Output format (see PHP date funciton)
        $sTime = strtotime($start); // Start as time
        $eTime = strtotime($end); // End as time
        $numDays = round(($eTime - $sTime) / $day) + 1;
        $days = array();
        
        // Get days
        for ($d = 0; $d < $numDays; $d++) {
            $days[] = date($format, ($sTime + ($d * $day)));
        }
        
        // Return days
        return $days; 
    }
}