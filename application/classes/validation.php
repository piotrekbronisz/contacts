<?php defined('SYSPATH') or die('No direct script access.');

require_once Kohana::find_file('../captcha', 'securimage');

class Validation extends Kohana_Validation {
    /**
     * Sprawdzanie poprawności numeru NIP
     * @author Unknown
     */
    public static function nip($value)
    {
        $str = preg_replace("/[^0-9]+/","", $value);
        
        if (strlen($str) != 10)
        {
            return FALSE;
        }

        $arrSteps = array(6, 5, 7, 2, 3, 4, 5, 6, 7);
        $intSum=0;
        for ($i = 0; $i < 9; $i++) {
            $intSum += $arrSteps[$i] * $str[$i];
        }
        $int = $intSum % 11;

        $intControlNr=($int == 10)?0:$int;
        
        return ($intControlNr == $str[9]) ? TRUE : FALSE;
    }
    
    /**
     * Sprawdzanie poprawności numeru PESEL
     * @author Unknown
     */
    public static function pesel($value)
    {
        $map = array(1, 3, 7, 9, 1, 3, 7, 9, 1, 3);

        $checksum = 0;
		for ( $i = 0; $i < 10; ++$i ) {
			$checksum += ($value{$i} * $map[$i]);
		}

		$standarized_checksum = ( (10 - $checksum % 10) == 10 ? 0 : (10 - $checksum % 10) );
        
        return ($standarized_checksum == $value{10}) ? TRUE : FALSE;
    }
    
    /**
     * Sprawdzanie poprawności numeru IBAN
     * @author Unknown
     */
    public static function iban($nrb)
    {
         if (strlen($nrb)!=26)
         {
            return FALSE;
         }
         
         $W = array(1,10,3,30,9,90,27,76,81,34,49,5,50,15,53,45,62,38,89,17,73,51,25,56,75,71,31,19,93,57);
         
         $nrb .= "2521";
         $nrb = substr($nrb,2).substr($nrb,0,2);
         $Z =0;
         for ($i=0;$i<30;$i++) {
            $Z += $nrb[29-$i] * $W[$i];
         }
         
         return ($Z % 97 == 1) ? TRUE : FALSE;
    }
	
    /**
	 * Checks that at least $needed of $fields are not_empty
	 *
	 * @param   array    array of values
	 * @param   integer  Number of fields required
	 * @param   array    Field names to check.
	 * @return  boolean
	 */
	public static function at_least(Validation $array, $field, $params)
    {
	    $found = 0;
        $needed = $params[0];
        $fields = $params[1];
        
        foreach ($fields as $field) {
			if (isset($array[$field]) AND Valid::not_empty($array[$field]))
            {
				$found++;
			}
		}
        
        //echo $found.'>='.$needed.'<br>';
        if ($found >= $needed) {
			return TRUE;
        }
        else
        {
            $array->error($fields[0], 'at_least', array($needed, $fields));
        }
	}

    /**
     * Sprawdzanie czy wymiary obrazka mieszczą sie w określonym zakresie
     *
     * @param string Ścieżka do pliku
     * @param integer Szerokość
     * @param integer Wysokość
     * @param string Typ sprawdzania. Dostępne typy:
     *      - max - Wymiary muszą mieścić się w podanym zakresie
     *      - exact - Wymiary muszą wynościć dokładnie tyle ile podano w parametrach $max_width i $max_height
     *      - min - Minimalne wymiary obrazka
     */
    public static function image($file, $max_width = NULL, $max_height = NULL, $type = 'max')
    {
        try
        {
            // Get the width and height from the uploaded image
            list($width, $height) = getimagesize(trim($file, '/'));
        }
        catch (ErrorException $e)
        {
            // Ignore read errors
        }

        if (empty($width) OR empty($height))
        {
            // Cannot get image size, cannot validate
            return FALSE;
        }

        if ( ! $max_width)
        {
            // No limit, use the image width
            $max_width = $width;
        }

        if ( ! $max_height)
        {
            // No limit, use the image height
            $max_height = $height;
        }

        if ($type == 'exact')
        {
            // Check if dimensions match exactly
            return ($width === $max_width AND $height === $max_height);
        }
        else if ($type == 'max')
        {
            // Check if size is within maximum dimensions
            return ($width <= $max_width AND $height <= $max_height);
        }
        else if ($type == 'min')
        {
            // Check if size is within minimum dimensions
            return ($width >= $max_width AND $height >= $max_height);
        }

        return FALSE;
    }

    /**
     * Sprawdzanie poprawności numeru PWZ dla lekarzy
     * @see http://www.nil.org.pl/rejestry/centralny-rejestr-lekarzy/zasady-weryfikowania-nr-prawa-wykonywania-zawodu
     * @author Piotr Bronisz <piotrekbronisz@gmail.com>
     */
    public static function pwz($value)
    {
        $str = preg_replace("/[^0-9]+/","", $value);

        if (strlen($str) != 7)
        {
            return FALSE;
        }

        $map = array(1, 2, 3, 4, 5, 6);

        $checksum = 0;
        for ($i = 1; $i < 7; $i++) {
            $checksum += @($value{$i} * $map[$i]);
        }

        $checksum = $checksum % 11;

        return ($checksum == $value{0}) ? TRUE : FALSE;
    }

    /**
     * Sprawdzenie czy uzyktownik ma odpowiedni wiek
     * @author Piotr Bronisz <piotrekbronisz@gmail.com>
     */
    public static function age($value, $age)
    {
        if (Date::age($value) >= $age)
        {
            return TRUE;
        }

        return FALSE;
    }

    /**
     * Sprawdzenie popraności kodu captcha
     * @author Piotr Bronisz <piotrekbronisz@gmail.com>
     */
    public static function captcha($value)
    {
        $securimage = new Securimage(array(
            'session_name' => 's'
        ));

        if ($securimage->check($value) == false)
        {
            return false;
        }

        return true;
    }

    /**
     * Sprawdzenie czy link jest linkiem do youtube
     * @author Piotr Bronisz <piotrekbronisz@gmail.com>
     */
    public static function youtube($value)
    {
        $rx = '~
            ^(?:https?://)?              # Optional protocol
             (?:www\.)?                  # Optional subdomain
             (?:youtube\.com|youtu\.be)  # Mandatory domain name
             /watch\?v=([^&]+)           # URI with video id as capture group 1
             ~x';

        if (preg_match($rx, $value, $matches))
        {
            return TRUE;
        }

        return FALSE;
    }
}