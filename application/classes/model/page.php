<?php

class Model_Page extends Jelly_Model_MPTT {

    public $arguments = array();
    public $breadcrumbs = array();

    public static function initialize(Jelly_Meta $meta) {
        $meta->sorting(array('lft' => 'ASC'))
                ->fields = array(
            'id' => Jelly::field('primary'),
            'parent_id' => Jelly::field('integer'),
            'title' => Jelly::field('string'),
            'header' => Jelly::field('string'),
            'header2' => Jelly::field('string'),
            'content_short' => Jelly::field('text'),
            'content' => Jelly::field('text'),
            'icon' => Jelly::field('string'),
            'slug' => Jelly::field('string'),
            'type' => Jelly::field('string'),
            'template' => Jelly::field('string'),
            'created_on' => Jelly::field('string', array(
                'default' => date('Y-m-d H:i:s', time())
            )),
            'keywords' => Jelly::field('string'),
            'description' => Jelly::field('text'),
            'menu' => Jelly::field('integer', array(
                'default' => 0
            )),
            'options' => Jelly::field('serialized'),
            'active' => Jelly::field('integer', array(
                'default' => 1
            )),
            'heading' => Jelly::field('hasone', array(
                'foreign' => 'pages_header'
            ))
        );

        parent::initialize($meta);
    }

    public function mod_url($slug) {
        $original_slug = $slug;

        $iCounter = 0;

        while (true) {
            $check_page = Jelly::query('page')
                    ->where('slug', '=', $slug)
                    ->select();

            if ($check_page->count()) {
                $iCounter++;

                $slug = $original_slug . '-' . $iCounter;
            } else {
                break;
            }
        }

        return $slug;
    }

    public function move_up() {
        $above = Jelly::query('page')
                ->where('scope', '=', $this->scope)
                ->where('lvl', '=', $this->level)
                ->where('rgt', '=', $this->left - 1)
                ->limit(1)
                ->select();

        $this->move_to_prev_sibling($above->id);
    }

    public function move_down() {
        $below = Jelly::query('page')
                ->where('scope', '=', $this->scope)
                ->where('lvl', '=', $this->level)
                ->where('lft', '=', $this->right + 1)
                ->limit(1)
                ->select();

        $this->move_to_next_sibling($below->id);
    }

    public function permalink($permalink, $root) {
        $current_page = Jelly::factory('page');

        if (empty($permalink)) {
            $current_page = $current_page->root($root);
        } else {
            $segments = explode('/', $permalink);
            $children = Jelly::factory('page')
                    ->root($root)
                    ->children();

            for ($i = 0; $i < count($segments); $i++) {
                foreach ($children as $child) {
                    if ($child->slug === $segments[$i]) {
                        $current_page = $child;

                        if ($child->has_children()) {
                            $children = $child->children();

                            continue 2;
                        } else {
                            break 2;
                        }
                    }
                }
            }

            // if we've found a page, how many arguments are left?
            if (count($segments) > $current_page->level) {
                $current_page->arguments = array_slice($segments, -(count($segments) - $current_page->level));
            }
        }

        return $current_page;
    }

    public function uri() {
        $uri = array();

        foreach ($this->parents() as $parent) {
            if ($parent->level > 0) {
                $uri[] = urlencode($parent->slug);
            }
        }

        if ($this->level > 0) {
            $uri[] = urlencode($this->slug);
        }

        return empty($uri) ? '/' : 'page/' . implode('/', $uri);
    }

}
