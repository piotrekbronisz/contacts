<?php

class Model_Pages_News extends Jelly_Model {
    public static function initialize(Jelly_Meta $meta)
    {
    	$meta->sorting(array('publish_on' => 'DESC'))
        ->fields(array(
    		'id' => Jelly::field('primary'),
            'name' => Jelly::field('string'),
            'content_short' => Jelly::field('text'),
            'content' => Jelly::field('text'),
            'icon' => Jelly::field('string'),
            'meta_title' => Jelly::field('string'),
            'meta_keywords' => Jelly::field('text'),
            'meta_description' => Jelly::field('text'),
            'slug' => Jelly::field('string'),
            'publish_on' => Jelly::field('timestamp', array(
                'format' => 'Y-m-d H:i:s',
                'auto_now_create' => FALSE,
                'auto_now_update' => FALSE
            )),
            'updated_on' => Jelly::field('timestamp', array(
                'format' => 'Y-m-d H:i:s',
                'auto_now_create' => TRUE,
                'auto_now_update' => TRUE
            )),
            'created_on' => Jelly::field('timestamp', array(
                'format' => 'Y-m-d H:i:s',
                'auto_now_create' => TRUE,
                'auto_now_update' => FALSE
            )),
            'posy' => Jelly::field('integer', array(
                'default' => 1
            )),
            'active' => Jelly::field('integer', array(
                'default' => 1
            )),
            'page' => Jelly::field('belongsto')
        ));
    }
    
    public function uri()
    {
        if (($uri = Cache::instance()->get('news_uri_'.$this->id)) === NULL) {
            $page = Jelly::query('page', $this->page->id)->select();
            
            $uri = URL::site($page->uri().'/'.$this->id.','.$this->slug.'.html', 'http');
            
            Cache::instance()->set('news_uri_'.$this->id, $uri, 3600, array('news_uris'));
        }
        
        return $uri;
    }
    
    public function mod_url($slug)
    {
        $original_slug = $slug;
        
        $iCounter = 0;
        
        while ( true ) {
            $check_page = Jelly::query('pages_news')
                ->where('slug', '=', $slug)
                ->select();
                
            if ($check_page->count())
            {
                $iCounter++ ;
                
                $slug = $original_slug.'-'.$iCounter;
            }
            else
            {
                break;
            }
        }
        
        return $slug;
    }
}