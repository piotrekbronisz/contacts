<?php

class Model_Pages_Contact extends Jelly_Model {
    public static function initialize(Jelly_Meta $meta)
    {
    	$meta->sorting(array('id' => 'DESC'))
        ->fields(array(
    		'id' => Jelly::field('primary'),
            'content' => Jelly::field('string'),
            'google_long' => Jelly::field('string'),
            'google_lat' => Jelly::field('string'),
            'page' => Jelly::field('belongsto')
        ));
    }
}