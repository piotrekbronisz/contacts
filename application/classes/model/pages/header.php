<?php

class Model_Pages_Header extends Jelly_Model {
    public static function initialize(Jelly_Meta $meta)
    {
    	$meta->fields(array(
    		'id' => Jelly::field('primary'),
            'content' => Jelly::field('text'),
            'content2' => Jelly::field('text'),
            'content3' => Jelly::field('text'),
            'page' => Jelly::field('belongsto')
        ));
    }
}