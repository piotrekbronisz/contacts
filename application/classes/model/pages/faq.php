<?php

class Model_Pages_Faq extends Jelly_Model {
    public static function initialize(Jelly_Meta $meta)
    {
    	$meta->fields(array(
    		'id' => Jelly::field('primary'),
            'name' => Jelly::field('string'),
            'content' => Jelly::field('text'),
            'updated_on' => Jelly::field('timestamp', array(
                'format' => 'Y-m-d H:i:s',
                'auto_now_create' => TRUE,
                'auto_now_update' => TRUE
            )),
            'created_on' => Jelly::field('timestamp', array(
                'format' => 'Y-m-d H:i:s',
                'auto_now_create' => TRUE,
                'auto_now_update' => FALSE
            )),
            'posy' => Jelly::field('integer', array(
                'default' => 1
            )),
            'active' => Jelly::field('integer', array(
                'default' => 1
            )),
            'page' => Jelly::field('belongsto')
        ));
    }
}