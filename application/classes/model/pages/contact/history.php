<?php

class Model_Pages_Contact_History extends Jelly_Model {
    public static function initialize(Jelly_Meta $meta)
    {
    	$meta->sorting(array('id' => 'DESC'))
        ->fields(array(
    		'id' => Jelly::field('primary'),
            'name' => Jelly::field('string'),
            'email' => Jelly::field('string'),
            'phone' => Jelly::field('string'),
            'content' => Jelly::field('text'),
            'file' => Jelly::field('string'),
            'created_on' => Jelly::field('timestamp', array(
                'format' => 'Y-m-d H:i:s',
                'auto_now_create' => TRUE,
                'auto_now_update' => FALSE
            )),
            'subject' => Jelly::field('belongsto', array(
                'foreign' => 'pages_contact_subject',
                'column' => 'subject_id'
            )),
            'page' => Jelly::field('belongsto')
        ));
    }
}