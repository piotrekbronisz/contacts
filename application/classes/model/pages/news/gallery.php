<?php

class Model_Pages_News_Gallery extends Jelly_Model {
    public static function initialize(Jelly_Meta $meta)
    {
    	$meta->sorting(array('posy' => 'ASC'))
        ->fields(array(
    		'id' => Jelly::field('primary'),
            'icon' => Jelly::field('string'),
            'updated_on' => Jelly::field('timestamp', array(
                'format' => 'Y-m-d H:i:s',
                'auto_now_create' => TRUE,
                'auto_now_update' => TRUE
            )),
            'created_on' => Jelly::field('timestamp', array(
                'format' => 'Y-m-d H:i:s',
                'auto_now_create' => TRUE,
                'auto_now_update' => FALSE
            )),
            'posy' => Jelly::field('integer', array(
                'default' => 1
            )),
            'news' => Jelly::field('belongsto', array(
                'allow_null' => TRUE,
                'foreign' => 'pages_news',
                'column' => 'news_id'
            ))
        ));
    }
}