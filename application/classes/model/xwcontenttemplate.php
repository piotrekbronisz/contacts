<?php

class Model_Xwcontenttemplate extends Jelly_Model {
    public static function initialize(Jelly_Meta $meta) {
    	$meta->table('xw_content_template')
        ->sorting(array('id' => 'DESC'))
        ->fields(array(
    		'id' => Jelly::field('primary'),
            'hash' => Jelly::field('string'),
    		'name' => Jelly::field('string'),
            'description' => Jelly::field('text'),
            'tags' => Jelly::field('text'),
            'title' => Jelly::field('string'),
            'body' => Jelly::field('text'),
            'body_txt' => Jelly::field('text')
        ));
    }
}