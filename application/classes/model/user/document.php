<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_User_Document extends Jelly_Model {
	public static function initialize(Jelly_Meta $meta)
    {
        $meta->fields(array(
            'id' => Jelly::field('primary'),
            'filename' => Jelly::field('string'),
            'filesize' => Jelly::field('string'),
            'ext' => Jelly::field('string'),
            'created_on' => Jelly::field('timestamp', array(
                'format' => 'Y-m-d H:i:s',
                'auto_now_create' => TRUE,
                'auto_now_update' => FALSE
            )),
            'updated_on' => Jelly::field('timestamp', array(
                'format' => 'Y-m-d H:i:s',
                'auto_now_create' => TRUE,
                'auto_now_update' => TRUE
            )),
            'user' => Jelly::field('belongsto'),
        ));
    }

    public function preview($size = '16x16')
    {
        if (file_exists(DOCROOT.$this->filename))
        {
            $extension = File::ext($this->filename);

            $possible_location =  "assets/img/file-types/$size/$extension.png";

            if (!is_file($possible_location))
            {
                return '/assets/img/file-types/'.$size.'/default.png';
            }
            else
            {
                return '/assets/img/file-types/'.$size.'/'.$extension.'.png';
            }
        }
    }
}