<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_User_Credit extends Jelly_Model {
    public static function initialize(Jelly_Meta $meta)
    {
    	$meta->sorting(array('created_on' => 'DESC'))
        ->fields(array(
    		'id' => Jelly::field('primary'),
            'amount' => Jelly::field('float'),
            'account_amount' => Jelly::field('float'),
            'kind' => Jelly::field('string'),
            'description' => Jelly::field('text'),
            'updated_on' => Jelly::field('timestamp', array(
                'format' => 'Y-m-d H:i:s',
                'auto_now_create' => TRUE,
                'auto_now_update' => TRUE
            )),
            'created_on' => Jelly::field('timestamp', array(
                'format' => 'Y-m-d H:i:s',
                'auto_now_create' => TRUE,
                'auto_now_update' => FALSE
            )),
            'user' => Jelly::field('belongsto')
        ));
    }

    public function change_user_account($user_id, $amount, $description, $kind)
    {
        $user = Jelly::query('user', $user_id)->select();

        if (is_null($user->amount))
        {
            $user->amount = $amount;
            $user->save();
        }
        else
        {
            $user->amount = round($user->amount + $amount, 2);
            $user->save();
        }

        $account_amount = round($user->balance() + $amount, 2);

        $credit = Jelly::factory('user_credit');
        $credit->user = $user_id;
        $credit->amount = $amount; //kwota jaka operujemy
        $credit->account_amount = $account_amount; //saldo po operacji
        $credit->kind = $kind;
        $credit->description = $description;
        $credit->created_on = date('Y-m-d H:i:s', time());
        $credit->save();
    }
}