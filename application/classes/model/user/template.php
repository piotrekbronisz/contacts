<?php

class Model_User_Template extends Jelly_Model {
    public static function initialize(Jelly_Meta $meta) {
    	$meta->fields(array(
    		'id' => Jelly::field('primary'),
    		'name' => Jelly::field('string'),
            'template' => Jelly::field('serialized'),
            'updated_on' => Jelly::field('timestamp', array(
                'format' => 'Y-m-d H:i:s',
                'auto_now_create' => TRUE,
                'auto_now_update' => TRUE
            )),
            'created_on' => Jelly::field('timestamp', array(
                'format' => 'Y-m-d H:i:s',
                'auto_now_create' => TRUE,
                'auto_now_update' => FALSE
            ))
        ));
    }
}