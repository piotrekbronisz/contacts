<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_User_Addressbook extends Jelly_Model {

	public static function initialize(Jelly_Meta $meta)
    {
        $meta->fields(array(
            'id' => Jelly::field('primary'),
            'name' => Jelly::field('string'),
            'firstname' => Jelly::field('string'),
            'lastname' => Jelly::field('string'),
            'company' => Jelly::field('string'),
            'street' => Jelly::field('string'),
            'home' => Jelly::field('string'),
            'flat' => Jelly::field('string'),
            'postcode' => Jelly::field('string'),
            'city' => Jelly::field('string'),
            'phone' => Jelly::field('string'),
            'created_on' => Jelly::field('timestamp', array(
                'format' => 'Y-m-d H:i:s',
                'auto_now_create' => TRUE,
                'auto_now_update' => FALSE
            )),
            'updated_on' => Jelly::field('timestamp', array(
                'format' => 'Y-m-d H:i:s',
                'auto_now_create' => TRUE,
                'auto_now_update' => TRUE
            )),
            'user' => Jelly::field('belongsto'),
            'country' => Jelly::field('belongsto'),
            'province' => Jelly::field('belongsto'),
        ));
    }
}