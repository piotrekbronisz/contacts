<?php

class Model_User_Login extends Jelly_Model {
    public static function initialize(Jelly_Meta $meta) {
    	$meta->fields(array(
    		'id' => Jelly::field('primary'),
            'ua' => Jelly::field('string'),
            'ip' => Jelly::field('string'),
            'updated_on' => Jelly::field('timestamp', array(
                'format' => 'Y-m-d H:i:s',
                'auto_now_create' => TRUE,
                'auto_now_update' => TRUE
            )),
            'created_on' => Jelly::field('timestamp', array(
                'format' => 'Y-m-d H:i:s',
                'auto_now_create' => TRUE,
                'auto_now_update' => FALSE
            )),
            'status' => Jelly::field('integer', array(
                'default' => 1
            )),
            'user' => Jelly::field('belongsto')
        ));
    }
}