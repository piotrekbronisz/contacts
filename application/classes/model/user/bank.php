<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_User_Bank extends Jelly_Model {

	public static function initialize(Jelly_Meta $meta)
    {
        $meta->fields(array(
            'id' => Jelly::field('primary'),
            'name' => Jelly::field('string'),
            'account' => Jelly::field('string'),
            'created_on' => Jelly::field('timestamp', array(
                'format' => 'Y-m-d H:i:s',
                'auto_now_create' => TRUE,
                'auto_now_update' => FALSE
            )),
            'updated_on' => Jelly::field('timestamp', array(
                'format' => 'Y-m-d H:i:s',
                'auto_now_create' => TRUE,
                'auto_now_update' => TRUE
            )),
            'user' => Jelly::field('belongsto')
        ));
    }
}