<?php

class Model_Home extends Jelly_Model {
    public static function initialize(Jelly_Meta $meta)
    {
    	$meta->sorting(array('name' => 'ASC'))
        ->fields(array(
            'id' => Jelly::field('primary'),
    		'name' => Jelly::field('string'),
            'content' => Jelly::field('text'),
            'price' => Jelly::field('float'),
            'address' => Jelly::field('string'),
            'homepage' => Jelly::field('string'),
            'email' => Jelly::field('string'),
            'phone' => Jelly::field('string'),
            'lat' => Jelly::field('float'),
            'lng' => Jelly::field('float'),
            'updated_on' => Jelly::field('timestamp', array(
                'format' => 'Y-m-d H:i:s',
                'auto_now_create' => TRUE,
                'auto_now_update' => TRUE
            )),
            'created_on' => Jelly::field('timestamp', array(
                'format' => 'Y-m-d H:i:s',
                'auto_now_create' => TRUE,
                'auto_now_update' => FALSE
            )),
            'type' => Jelly::field('belongsto', array(
                'allow_null' => TRUE
            )),
            'city' => Jelly::field('belongsto', array(
                'allow_null' => TRUE
            ))
        ));
    }

    /**
     * Pobieranie tłumaczenia
     * @author Piotr Bronisz <piotrekbronisz@gmail.com>
     */
    public function get_translate($lang)
    {
        $item = Jelly::query('home_translate')
            ->where('language_id', '=', $lang)
            ->where('home_id', '=', $this->id)
            ->limit(1)
            ->select();

        return $item;
    }
	
	/**
     * Pobieranie ikony
     * @author Piotr Bronisz <piotrekbronisz@gmail.com>
     */
    public function get_icon()
    {
        $item = Jelly::query('home_gallery')
            ->where('activity_id', '=', $this->id)
            ->limit(1)
			->order_by('posy', 'ASC')
            ->select();

        return $item->icon;
    }
}