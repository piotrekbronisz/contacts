<?php

class Model_Search extends Jelly_Model {
    public static function initialize(Jelly_Meta $meta) {
    	$meta->fields(array(
    		'id' => Jelly::field('primary'),
    		'query' => Jelly::field('string'),
            'hash' => Jelly::field('string'),
            'created_on' => Jelly::field('timestamp', array(
                'format' => 'Y-m-d H:i:s',
                'auto_now_create' => TRUE,
                'auto_now_update' => FALSE
            ))
        ));
    }
}