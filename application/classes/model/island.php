<?php

class Model_Island extends Jelly_Model {
    public static function initialize(Jelly_Meta $meta)
    {
    	$meta->sorting(array('name' => 'ASC'))
        ->fields(array(
            'id' => Jelly::field('primary'),
    		'name' => Jelly::field('string'),
            'lat' => Jelly::field('float'),
            'lng' => Jelly::field('float'),
            'updated_on' => Jelly::field('timestamp', array(
                'format' => 'Y-m-d H:i:s',
                'auto_now_create' => TRUE,
                'auto_now_update' => TRUE
            )),
            'created_on' => Jelly::field('timestamp', array(
                'format' => 'Y-m-d H:i:s',
                'auto_now_create' => TRUE,
                'auto_now_update' => FALSE
            )),
        ));
    }

    /**
     * Pobieranie tłumaczenia
     * @author Piotr Bronisz <piotrekbronisz@gmail.com>
     */
    public function get_translate($lang)
    {
        $item = Jelly::query('island_translate')
            ->where('language_id', '=', $lang)
            ->where('island_id', '=', $this->id)
            ->limit(1)
            ->select();

        return $item;
    }
}