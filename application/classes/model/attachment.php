<?php

class Model_Attachment extends Jelly_Model {
    public static function initialize(Jelly_Meta $meta) {
    	$meta->sorting(array('id' => 'DESC'))
        ->fields(array(
    		'id' => Jelly::field('primary'),
    		'obj_id' => Jelly::field('integer'),
            'obj_type' => Jelly::field('string'),
            'name' => Jelly::field('string'),
            'filename' => Jelly::field('string'),
            'filesize' => Jelly::field('string'),
            'ext' => Jelly::field('string'),
            'slug' => Jelly::field('string'),
            'type' => Jelly::field('integer', array(
                'default' => 1
            ))
        ));
    }

    public function preview($size = '16x16')
    {
        if (file_exists(DOCROOT.$this->filename))
        {
            $extension = File::ext($this->filename);

            $possible_location =  "assets/img/file-types/$size/$extension.png";

            if (!is_file($possible_location))
            {
                return '/assets/img/file-types/'.$size.'/default.png';
            }
            else
            {
                return '/assets/img/file-types/'.$size.'/'.$extension.'.png';
            }
        }
    }
}