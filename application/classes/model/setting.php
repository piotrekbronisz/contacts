<?php

class Model_Setting extends Jelly_Model {
    public static function initialize(Jelly_Meta $meta) {
    	$meta->fields(array(
    		'id' => Jelly::field('primary'),
    		'section_id' => Jelly::field('integer'),
            'subsection_id' => Jelly::field('integer'),
    		'name' => Jelly::field('string'),
            'key' => Jelly::field('string'),
            'value' => Jelly::field('string'),
            'description' => Jelly::field('string'),
            'type' => Jelly::field('string')
        ));
    }
}
