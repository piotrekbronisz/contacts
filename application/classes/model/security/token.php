<?php

class Model_Security_Token extends Jelly_Model {
    public static function initialize(Jelly_Meta $meta)
    {
    	$meta->fields(array(
    		'id' => Jelly::field('primary'),
    		'token' => Jelly::field('string'),
            'created_on' => Jelly::field('timestamp', array(
                'format' => 'Y-m-d H:i:s',
                'auto_now_create' => TRUE,
                'auto_now_update' => FALSE
            )),
            'used_on' => Jelly::field('timestamp', array(
                'format' => 'Y-m-d H:i:s',
                'auto_now_create' => FALSE,
                'auto_now_update' => FALSE
            )),
            'user' => Jelly::field('belongsto')
        ));
    }
}