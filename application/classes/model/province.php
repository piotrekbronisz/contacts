<?php defined('SYSPATH') or die ('No direct script access.');

class Model_Province extends Jelly_Model {
	public static function initialize(Jelly_Meta $meta) {
		$meta->sorting(array('name' => 'ASC'))
        ->fields(array(
			'id' => Jelly::field('primary'),
			'name' => Jelly::field('string')
		));
    }
}