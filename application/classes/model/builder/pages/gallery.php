<?php
class Model_Builder_Pages_Gallery extends Jelly_Builder {
    public function by_page($category)
    {
        return $this->where('page_id','=',$category);
    }

    public function in_page($category)
    {
        return $this->where('page_id','IN',array(DB::expr($category)));
    }

    public function by_slug($slug)
    {
        return $this->where('slug','=',$slug);
    }

    public function filter($term = null)
    {
        $query = $this;

        if (! is_null($term) && is_array($term))
        {
            if (isset($term['active']))
            {
                if (! is_null($term['active']) && ! empty($term['active']))
                {
                    $query->where('active', '=', $term['active']);
                }
            }
        }

        return $query;
    }
}