<?php
class Model_Builder_Pages_Article extends Jelly_Builder {
    public function by_page($category)
    {
        return $this->where('page_id','=',$category);
    }
    
    public function in_page($category)
    {
        return $this->where('page_id','IN',array(DB::expr($category)));
    }
    
    public function by_slug($slug)
    {
        return $this->where('slug','=',$slug);
    }

    public function filter($term = null)
    {
        $query = $this;

        if (! is_null($term) && is_array($term))
        {
            if (isset($term['year']))
            {
                if (! is_null($term['year']) && ! empty($term['year']))
                {
                    $query->where(DB::expr('YEAR(publish_on)'), '=', $term['year']);
                }
            }

            if (isset($term['name']))
            {
                if (! is_null($term['name']) && ! empty($term['name']))
                {
                    $query->where('name', 'LIKE', '%'.$term['name'].'%');
                }
            }

            if (isset($term['content']))
            {
                if (! is_null($term['content']) && ! empty($term['content']))
                {
                    $query->where('content', 'LIKE', '%'.$term['content'].'%');
                }
            }

            if (isset($term['active']))
            {
                if (! is_null($term['active']) && ! empty($term['active']))
                {
                    $query->where('active', '=', $term['active']);
                }
            }

            if (isset($term['home']))
            {
                if (! is_null($term['home']) && ! empty($term['home']))
                {
                    $query->where('home', '=', $term['home']);
                }
            }
        }

        return $query;
    }
}