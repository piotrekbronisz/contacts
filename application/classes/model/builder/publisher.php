<?php
class Model_Builder_Publisher extends Jelly_Builder {
    public function by_slug($value)
    {
        return $this->where('slug', '=', $value);
    }

    public function filter($term = null)
    {
        $query = $this;

        if (!is_null($term) && is_array($term))
        {
            if (isset($term['name']))
            {
                if (! is_null($term['name']) && ! empty($term['name']))
                {
                    $query->where('name', 'LIKE', '%'.$term['name'].'%');
                }
            }

            if (isset($term['active']))
            {
                if (! is_null($term['active']) && is_numeric($term['active']))
                {
                    $query->where('active', '=', $term['active']);
                }
            }
        }

        return $query;
    }
}