<?php
class Model_Builder_Home extends Jelly_Builder {
    public function filter($term = null)
    {
        $query = $this;

        if (!is_null($term) && is_array($term))
        {
            if (isset($term['name']))
            {
                if (! is_null($term['name']) && ! empty($term['name']))
                {
                    $query->where('name', 'LIKE', '%'.$term['name'].'%');
                }
            }

            if (isset($term['city']))
            {
                if (! is_null($term['city']) && ! empty($term['city']))
                {
                    $query->where(':city.name', 'LIKE', '%'.$term['city'].'%');
                }
            }

            if (isset($term['type']))
            {
                if (! is_null($term['type']) && ! empty($term['type']))
                {
                    $query->where(':type.name', 'LIKE', '%'.$term['type'].'%');
                }
            }
        }

        return $query;
    }
}