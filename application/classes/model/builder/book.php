<?php
class Model_Builder_Book extends Jelly_Builder {
    public function by_slug($value)
    {
        return $this->where('slug', '=', $value);
    }

    public function filter($term = null)
    {
        $query = $this;

        if (!is_null($term) && is_array($term))
        {
            if (isset($term['subject_id']))
            {
                if (! is_null($term['subject_id']) && ! empty($term['subject_id']))
                {
                    $query->where('subject_id', '=', $term['subject_id']);
                }
            }

            if (isset($term['publisher_id']))
            {
                if (! is_null($term['publisher_id']) && ! empty($term['publisher_id']))
                {
                    $query->where('publisher_id', '=', $term['publisher_id']);
                }
            }

            if (isset($term['class']))
            {
                if (! is_null($term['class']) && ! empty($term['class']))
                {
                    $query->where('class_id', '=', $term['class']);
                }
            }

            if (isset($term['name']))
            {
                if (! is_null($term['name']) && ! empty($term['name']))
                {
                    $query->where('name', 'LIKE', '%'.$term['name'].'%');
                }
            }

            if (isset($term['active']))
            {
                if (! is_null($term['active']) && ! empty($term['active']))
                {
                    $query->where('active', '=', $term['active']);
                }
            }
        }

        return $query;
    }
}