<?php
class Model_Builder_Activity extends Jelly_Builder {
    public function filter($term = null)
    {
        $query = $this;

        if (!is_null($term) && is_array($term))
        {
            if (isset($term['name']))
            {
                if (! is_null($term['name']) && ! empty($term['name']))
                {
                    $query->where('name', 'LIKE', '%'.$term['name'].'%');
                }
            }

            if (isset($term['city']))
            {
                if (! is_null($term['city']) && ! empty($term['city']))
                {
                    $query->where(':city.name', 'LIKE', '%'.$term['city'].'%');
                }
            }

            if (isset($term['category']))
            {
                if (! is_null($term['category']) && ! empty($term['category']))
                {
                    $query->where(':category:category.id', '=', $term['category']);
                }
            }

            if (isset($term['subcategory']))
            {
                if (! is_null($term['subcategory']) && ! empty($term['subcategory']))
                {
                    $query->where(':category.id', '=', $term['subcategory']);
                }
            }
        }

        return $query;
    }
}