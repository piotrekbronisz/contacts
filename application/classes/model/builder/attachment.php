<?php
class Model_Builder_Attachment extends Jelly_Builder {

    public function filter($obj_id = null, $obj_type = null, $type = 1) {
        $query = $this->where('id','>',0);
        
        if (!is_null($obj_id) && !empty($obj_id)) {
            $query->where('obj_id','=',$obj_id);
        }
        
        if (!is_null($obj_type) && !empty($obj_type)) {
            $query->where('obj_type','=',$obj_type);
        }
        
        if (!is_null($type) && !empty($type)) {
            $query->where('type','=',$type);
        }
        
        return $query;
    }
}

?>