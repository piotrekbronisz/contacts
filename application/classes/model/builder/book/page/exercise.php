<?php
class Model_Builder_Book_Page_Exercise extends Jelly_Builder {
    public function filter($term = null)
    {
        $query = $this;

        if (!is_null($term) && is_array($term))
        {
            if (isset($term['page_id']))
            {
                if (! is_null($term['page_id']) && ! empty($term['page_id']))
                {
                    $query->where('page_id', '=', $term['page_id']);
                }
            }

            if (isset($term['name']))
            {
                if (! is_null($term['name']) && ! empty($term['name']))
                {
                    $query->where('name', 'LIKE', '%'.$term['name'].'%');
                }
            }

            if (isset($term['active']))
            {
                if (! is_null($term['active']) && ! empty($term['active']))
                {
                    $query->where('active', '=', $term['active']);
                }
            }
        }

        return $query;
    }
}