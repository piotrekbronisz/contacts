<?php
class Model_Builder_Book_Page extends Jelly_Builder {
    public function filter($term = null)
    {
        $query = $this;

        if (!is_null($term) && is_array($term))
        {
            if (isset($term['book_id']))
            {
                if (! is_null($term['book_id']) && ! empty($term['book_id']))
                {
                    $query->where('book_id', '=', $term['book_id']);
                }
            }

            if (isset($term['name']))
            {
                if (! is_null($term['name']) && ! empty($term['name']))
                {
                    $query->where('page', 'LIKE', '%'.$term['name'].'%');
                }
            }

            if (isset($term['active']))
            {
                if (! is_null($term['active']) && ! empty($term['active']))
                {
                    $query->where('active', '=', $term['active']);
                }
            }
        }

        return $query;
    }
}