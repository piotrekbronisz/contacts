<?php
class Model_Builder_Type extends Jelly_Builder {
    public function filter($term = null)
    {
        $query = $this;

        if (!is_null($term) && is_array($term))
        {
            if (isset($term['name']))
            {
                if (! is_null($term['name']) && ! empty($term['name']))
                {
                    $query->where('name', 'LIKE', '%'.$term['name'].'%');
                }
            }
        }

        return $query;
    }
}