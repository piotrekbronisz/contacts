<?php
class Model_Builder_User extends Jelly_Builder {    
    public function by_role($role = null) {
        if (!is_null($role) && !empty($role)) {
            $query = $this->where('roles_users.role_id', '=', DB::expr($role));
        }
        else {
            $query = $this;
        }
        
    	return $query;  	
    }
    
    public function by_roles($role = null) {
        if (!is_null($role) && !empty($role)) {
            $query = $this->where('roles_users.role_id', 'IN', $role);
        }
        else {
            $query = $this;
        }
        
    	return $query;  	
    }
    
    public function filter($term = null) {
        $query = $this;
        
        if (!is_null($term) && is_array($term))
        {
            if (isset($term['id']))
            {
                if (!is_null($term['id']) && !empty($term['id']))
                {
                    $query->and_where('id', 'LIKE', '%'.$term['id'].'%');
                }
            }

            if (isset($term['email']))
            {
                if (!is_null($term['email']) && !empty($term['email']))
                {
                    $query->and_where('email', 'LIKE', '%'.$term['email'].'%');
                }
            }

            if (isset($term['firstname']))
            {
                if (!is_null($term['firstname']) && !empty($term['firstname']))
                {
                    $query->and_where('firstname', 'LIKE', '%'.$term['firstname'].'%');
                }
            }

            if (isset($term['lastname']))
            {
                if (!is_null($term['lastname']) && !empty($term['lastname']))
                {
                    $query->and_where('lastname', 'LIKE', '%'.$term['lastname'].'%');
                }
            }


            if (isset($term['created_start']))
            {
                if (!is_null($term['created_start']) && !empty($term['created_start']))
                {
                    $query->and_where('created_on', '>=', $term['created_start']);
                }
            }

            if (isset($term['created_end']))
            {
                if (!is_null($term['created_end']) && !empty($term['created_end']))
                {
                    $query->and_where('created_on', '<=', $term['created_end']);
                }
            }

            if (isset($term['login_start']))
            {
                if (!is_null($term['login_start']) && !empty($term['login_start']))
                {
                    $query->and_where('last_login', '>=', $term['login_start']);
                }
            }

            if (isset($term['login_end']))
            {
                if (!is_null($term['login_end']) && !empty($term['login_end']))
                {
                    $query->and_where('last_login', '<=', $term['login_end']);
                }
            }
        }
                
        return $query;
    }
}