<?php
class Model_Builder_Activity_Subcategory extends Jelly_Builder {
    public function filter($term = null)
    {
        $query = $this;

        if (!is_null($term) && is_array($term))
        {
            if (isset($term['category']))
            {
                if (! is_null($term['category']) && ! empty($term['category']))
                {
                    $query->where('category_id', '=', $term['category']);
                }
            }

            if (isset($term['name']))
            {
                if (! is_null($term['name']) && ! empty($term['name']))
                {
                    $query->where('name', 'LIKE', '%'.$term['name'].'%');
                }
            }
        }

        return $query;
    }
}