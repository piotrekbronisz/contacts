<?php
class Model_Builder_City extends Jelly_Builder {
    public function filter($term = null)
    {
        $query = $this;

        if (!is_null($term) && is_array($term))
        {
            if (isset($term['island']))
            {
                if (! is_null($term['island']) && ! empty($term['island']))
                {
                    $query->where('island_id', '=', $term['island']);
                }
            }

            if (isset($term['name']))
            {
                if (! is_null($term['name']) && ! empty($term['name']))
                {
                    $query->where('name', 'LIKE', '%'.$term['name'].'%');
                }
            }
        }

        return $query;
    }
}