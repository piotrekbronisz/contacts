<?php defined('SYSPATH') or die('No direct access allowed.');

class Model_User extends Model_Auth_User {

	public static function initialize(Jelly_Meta $meta)
    {
        parent::initialize($meta);
        
        $meta->fields(array(
            'nk_id' => Jelly::field('string', array(
                'unique' => TRUE,
                'default' => NULL,
            )),
            'firstname' =>  Jelly::field('string'),
            'lastname' => Jelly::field('string'),
            'city' => Jelly::field('string'),
                        
            'phonenumber' => Jelly::field('string'),
            'address' => Jelly::field('string'),
            'zip' => Jelly::field('string'),
            'is_friend' => Jelly::field('integer', array(
                'default' => 0
            )),
            
            'icon' => Jelly::field('string'),
            'token' => Jelly::field('string'),
            'code' => Jelly::field('string'),
            'birth_on' => Jelly::field('timestamp', array(
                'format' => 'Y-m-d',
                'auto_now_create' => FALSE,
                'auto_now_update' => FALSE
            )),
            'created_on' => Jelly::field('timestamp', array(
                'format' => 'Y-m-d H:i:s',
                'auto_now_create' => TRUE,
                'auto_now_update' => FALSE
            )),
            'active' => Jelly::field('integer', array(
                'default' => 0
            )),
            'province' => Jelly::field('belongsto', array(
                'allow_null' => TRUE
            )),
            'country' => Jelly::field('belongsto', array(
                'allow_null' => TRUE
            )),
        ));
    }

    /**
     * @author Michal Mlodzinski
     * Sprawdzanie czy ten e-mail jest wolny
     */
    public static function is_email_uniq(Validation $array, $field)
    {
        $check  = Jelly::query('user')
        	->where('email', '=', $array[$field])
        	->limit(1)
        	->select();
        
        if ($check->loaded())
        {
            $array->error($field, 'not_uniq');
        }
    }
    
    /**
     * @author Michal Mlodzinski
     * Sprawdzanie czy ten login jest wolny
     */
    public static function is_username_uniq(Validation $array, $field)
    {
        $check  = Jelly::query('user')
            ->where('username', '=', $array[$field])
            ->limit(1)
            ->select();
        
        if ($check->loaded())
        {
            $array->error($field, 'not_uniq');
        }
    }

    /**
     * @author Michal Mlodzinski
     * Sprawdzanie czy mozemy tej osobie zmienic e-mail
     */
    public static function email_change(Validation $array, $field)
    {
		$exists = (bool) DB::select(array('COUNT("*")', 'total_count'))
            ->from('users')
		    ->where('email', '=', $array[$field])
			->where('id', '!=', $array['id'])
			->execute()
			->get('total_count');
 
		if ($exists)
        {
			$array->error($field, 'not_uniq', array($array[$field]));
        }
	}
    
    /**
     * @author Michal Mlodzinski
     * Sprawdzanie czy mozemy tej osobie zmienic login
     */
    public static function username_change(Validation $array, $field)
    {
		$exists = (bool) DB::select(array('COUNT("*")', 'total_count'))
            ->from('users')
		    ->where('username', '=', $array[$field])
			->where('id', '!=', $array['id'])
			->execute()
			->get('total_count');
 
		if ($exists)
        {
			$array->error($field, 'not_uniq', array($array[$field]));
        }
	}    
    
    /**
     * @author Michal Mlodzinski
     * Sprawdzanie czy uzytkownik posiada dana role
     */
    public function has_role($role)
    {
		// Check what sort of argument we have been passed
		if ($role instanceof Model_Role)
        {
			$key = 'id';
			$val = $role->id;
		}
		elseif (is_string($role))
        {
			$key = 'name';
			$val = $role;
		}
		else
        {
			$key = 'id';
			$val = (int) $role;
		}

		foreach ($this->roles as $user_role) {	
			if ($user_role->{$key} === $val)
            {
				return TRUE;
			}
		}
		
		return FALSE;
	}
}