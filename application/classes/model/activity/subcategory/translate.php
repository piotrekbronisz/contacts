<?php

class Model_Activity_Subcategory_Translate extends Jelly_Model {
    public static function initialize(Jelly_Meta $meta)
    {
    	$meta->sorting(array('name' => 'ASC'))
        ->fields(array(
            'id' => Jelly::field('primary'),
    		'name' => Jelly::field('string'),
            'category' => Jelly::field('belongsto', array(
                'foreign' => 'activity_subcategory',
                'column' => 'category_id'
            )),
            'language' => Jelly::field('belongsto')
        ));
    }
}