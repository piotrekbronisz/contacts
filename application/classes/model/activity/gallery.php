<?php

class Model_Activity_Gallery extends Jelly_Model {
    public static function initialize(Jelly_Meta $meta)
    {
    	$meta->sorting(array('posy' => 'ASC'))
        ->fields(array(
    		'id' => Jelly::field('primary'),
            'icon' => Jelly::field('string'),
            'updated_on' => Jelly::field('timestamp', array(
                'format' => 'Y-m-d H:i:s',
                'auto_now_create' => TRUE,
                'auto_now_update' => TRUE
            )),
            'created_on' => Jelly::field('timestamp', array(
                'format' => 'Y-m-d H:i:s',
                'auto_now_create' => TRUE,
                'auto_now_update' => FALSE
            )),
            'posy' => Jelly::field('integer', array(
                'default' => 1
            )),
            'activity' => Jelly::field('belongsto', array(
                'allow_null' => TRUE
            ))
        ));
    }
}