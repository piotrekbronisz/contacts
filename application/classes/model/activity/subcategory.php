<?php

class Model_Activity_Subcategory extends Jelly_Model {
    public static function initialize(Jelly_Meta $meta)
    {
    	$meta->sorting(array('name' => 'ASC'))
        ->fields(array(
            'id' => Jelly::field('primary'),
    		'name' => Jelly::field('string'),
            'updated_on' => Jelly::field('timestamp', array(
                'format' => 'Y-m-d H:i:s',
                'auto_now_create' => TRUE,
                'auto_now_update' => TRUE
            )),
            'created_on' => Jelly::field('timestamp', array(
                'format' => 'Y-m-d H:i:s',
                'auto_now_create' => TRUE,
                'auto_now_update' => FALSE
            )),
            'category' => Jelly::field('belongsto', array(
                'foreign' => 'activity_category',
                'column' => 'category_id'
            ))
        ));
    }

    /**
     * Pobieranie tłumaczenia
     * @author Piotr Bronisz <piotrekbronisz@gmail.com>
     */
    public function get_translate($lang)
    {
        $item = Jelly::query('activity_subcategory_translate')
            ->where('language_id', '=', $lang)
            ->where('category_id', '=', $this->id)
            ->limit(1)
            ->select();

        return $item;
    }
}