<?php

class Model_Activity_Translate extends Jelly_Model {
    public static function initialize(Jelly_Meta $meta)
    {
    	$meta->sorting(array('name' => 'ASC'))
        ->fields(array(
            'id' => Jelly::field('primary'),
    		'name' => Jelly::field('string'),
            'content' => Jelly::field('text'),
            'activity' => Jelly::field('belongsto'),
            'language' => Jelly::field('belongsto')
        ));
    }
}