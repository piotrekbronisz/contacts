<?php defined('SYSPATH') or die('No direct script access.');

class File extends Kohana_File {
    /**
     * Get extension of the original name of the file on the client machine.
     * 
     * @param string $name (optional) If set return extension of this string
     * @return string|null i.e. '.gif' or NULL
     * @access public
     */
    public static function ext($name = null)
	{
        $s = isset($name) ? $name : '';
        
        $ext = null;
        if (($pos = strrpos($s, '.')) !== false) {
            $len = strlen($s) - ($pos + 1); //+1 jezeli ma byc bez kropki, usunac jak z kropka
            $ext = substr($s, -$len);
        }
        
        return $ext;
    }
    
    public static function download_remote($file = null, $local = null) {
        $cp = curl_init($file);
		$fp = fopen($local, "w");
		
		curl_setopt($cp, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($cp, CURLOPT_FILE, $fp);
		curl_setopt($cp, CURLOPT_HEADER, 0);
        curl_setopt($cp, CURLOPT_SSL_VERIFYPEER, false);
        
        https://graph.facebook.com/1238809918/picture?type=square
		
		curl_exec($cp);
		curl_close($cp);
		fclose($fp);
     
    }
    
    public static function ffilesize($file){
        $ch = curl_init($file);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        $data = curl_exec($ch);
        curl_close($ch);
        if ($data === false) {
            return false;
        }
        
        if (preg_match('/Content-Length: (\d+)/', $data, $matches)) {
            return File::filesize2bytes($matches[1]);
        }
    }
    
    public static function directoryToArray($directory, $recursive) {
        $array_items = array();
        if ($handle = opendir($directory)) {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    if (is_dir($directory. "/" . $file)) {
                        if ($recursive) {
                            $array_items = array_merge($array_items, file::directoryToArray($directory. "/" . $file, $recursive));
                        }
                    }
                    else {
                        $file = basename($file);
                        $file = preg_replace("/\/\//si", "/", $file);
                        $array_items[$file]['path'] = $directory.'/';
                        $array_items[$file]['file'] = $file;
                    }
                }
            }
            
            closedir($handle);
        }
        
        return $array_items;
    }
}