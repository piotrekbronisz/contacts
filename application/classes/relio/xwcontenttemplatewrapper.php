<?php

/**
 * Class Relio_xwContentTemplateWrapper
 * @version 1.0
 * @author Piotr Bronisz <piotrekbronisz@gmail.com>
 */
class Relio_xwContentTemplateWrapper {
    private static $_instance;

    protected $parameters;
    
    protected $base_uri;
    
    protected $to;
    
    protected $cc;

    protected $from;

    protected $replyto;

    protected $template;

    public static function instance()
    {
        if (self::$_instance === NULL)
        {
            self::$_instance = new self;
        }

        return self::$_instance;
    }

    public function setParameters($parameters)
    {
        $this->parameters = $parameters;

        return $this;
    }

    public function getParameters()
    {
        return $this->parameters;
    }
    
    public function setBaseUri($uri)
    {
        $this->base_uri = $uri;

        return $this;
    }

    public function getBaseUri()
    {
        return $this->base_uri ? $this->base_uri : URL::site('/', 'http');
    }

    public function setReplyTo($replyto)
    {
        $this->replyto = $replyto;

        return $this;
    }

    public function getReplyTo()
    {
        return $this->replyto;
    }

    public function setTo($to)
    {
        $this->to = $to;

        return $this;
    }

    public function getTo()
    {
        return $this->to;
    }
    
    public function setCc($cc)
    {
        $this->cc = $cc;
        
        return $this;
    }
    
    public function getCc()
    {
        return $this->cc;
    }

    public function setFrom($from)
    {
        $this->from = $from;

        return $this;
    }

    public function getFrom()
    {
        return $this->from;
    }

    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }

    public function getTemplate()
    {
        return $this->template;
    }

    public function send()
    {
        $template = Jelly::query('xwcontenttemplate')
            ->where('hash', '=', $this->getTemplate())
            ->limit(1)
            ->select();
        
        if ( ! $template->loaded())
        {
            throw new Exception('No template with the given name');
        }

        $title = str_replace(array_keys($this->getParameters()), array_values($this->getParameters()), $template->title);
        $body = str_replace(array_keys($this->getParameters()), array_values($this->getParameters()), $template->body);
        $body_txt = str_replace(array_keys($this->getParameters()), array_values($this->getParameters()), $template->body_txt);

        $message = View::factory('emails/schema.tpl')
            ->set('base_uri', $this->getBaseUri())
            ->set('title', $title)
            ->set('content', $body)
            ->render();
        
        $message = $this->convert_fck($message);
        
        $email = Email::factory($title, $message, 'text/html');
        $email->message($body_txt, 'text/plain');
        $email->to($this->getTo());

        if ($this->getCc())
        {
            $email->cc($this->getCc());
        }

        if ($this->getReplyTo())
        {
            $email->reply_to($this->getReplyTo());
        }

        $email->from($this->getFrom());
        $email->send();
    }
    
    public function convert_fck($text)
    {
        $server = URL::base('http', false);
        $attributes = array('src');
        
        foreach ($attributes as $attr) {
            $pattern[] = '/'.$attr.'=["'."'".']??(.*)["'."'".']??([ >])/Uis';
            $replacement[] = $attr.'="'.$server.'\1"\2';
        }
        
        $text = preg_replace('<--.*-->', '', $text); 
        $text = str_replace('', '"', $text);
        $text = str_replace('\"', '"', $text);
        $text = str_replace('="http', 'httpXWMailUri', $text);
        $text = str_replace('="mailto', 'httpXWMailMail', $text); 
        
        $text = preg_replace($pattern, $replacement, $text);
        $text = str_replace( 'httpXWMailUri', '="http', $text);
        $text = str_replace( 'httpXWMailMail','="mailto', $text);
        
        return $text;
    }
}