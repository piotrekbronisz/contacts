<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Relio_Controller_Error
 * @version 1.0
 * @author Piotr Bronisz <piotrekbronisz@gmail.com>
 */
class Relio_Controller_Error extends Controller_Template {
     public $template = 'error/index';
     
     public function before()
     {
         parent::before();
     }
     
     public function after()
     {
         parent::after();
     }
}