<?php

defined('SYSPATH') or die('No direct access allowed.');

abstract class Relio_Controller_Website extends Controller_Template {

    protected $_ajax = false;

    /**
     * @var  string  page template
     */
    public $template = 'frontend/layout.tpl';

    /**
     * @var  string  page content
     */
    public $content;
    public static $_acl;

    public function before() {
        $session = Session::instance();

        if (Request::current()->is_ajax() or ! $this->request->is_initial()) {
            $this->_ajax = true;
        }

        if ($this->_ajax === false) {
            // Load the template
            $this->template = View::factory($this->template);
        }

        View::set_global('controller', $this->request->controller());
        View::bind_global('content', $this->content);

        $is_logged = false;
        if (Auth::instance()->logged_in()) {
            $is_logged = true;
        }

        $asset = Asset::instance('default')
                ->style('assets/css/jquery/jquery.toastmessage.css')
                ->style('assets/css/jquery/jquery.alerts.css')
                ->style('assets/css/jquery/jquery.colorbox.css')
                ->style('assets/css/jquery/jquery.uploadify.css')
                ->style('assets/css/jquery/jquery.tipsy.css')
                ->style('assets/css/bootstrap.min.css')
                ->style('assets/css/bootstrap-responsive.min.css')
                ->style('assets/css/jquery.ui.css')
                ->style('assets/css/jquery.ui.theme.css')
                ->style('assets/css/ui.filemanager.css')
                ->style('assets/css/bootstrap/bootstrap.timepicker.css')
                ->style('assets/css/bootstrap/bootstrap.datepicker.css')
                ->style('assets/css/bootstrap/bootstrap.colorpicker.css')
                ->style('assets/css/style.css')
                ->style('assets/css/themes.css')

                ->script('assets/js/jquery/jquery.js')
                ->script('assets/js/jquery/jquery.migrate.js')
                ->script('assets/js/jquery/jquery.ui.js')
                ->script('assets/js/jquery/jquery.toastmessage.js')
                ->script('assets/js/jquery/jquery.alerts.js')
                ->script('assets/js/jquery/jquery.colorbox.js')
                ->script('assets/js/jquery/jquery.uploadify.js')
                ->script('assets/js/jquery/jquery.form.js')
                ->script('assets/js/jquery/jquery.tipsy.js')
                ->script('assets/js/jquery/jquery.controls.js')
                ->script('assets/js/jquery/jquery.dialog2.js')
                ->script('assets/js/jquery/jquery.dialog2.helpers.js')
                ->script('assets/js/jquery/jquery.bootbox.js')
                ->script('assets/js/jquery/jquery.slimscroll.min.js')
                ->script('assets/js/jquery/jquery.simplyCountable.js')
                ->script('assets/js/tinymce/jquery.tinymce.min.js')
                ->script('assets/js/bootstrap.min.js')
                ->script('assets/js/bootstrap/bootstrap.datepicker.js')
                ->script('assets/js/bootstrap/bootstrap.timepicker.js')
                ->script('assets/js/bootstrap/bootstrap.editable.js')
                ->script('assets/js/bootstrap/bootstrap.colorpicker.js')
                ->script('assets/js/php.js')
                ->script('assets/js/swfobject.js')
                ->script('assets/js/date.js')
                ->script('assets/js/highcharts/highcharts.js')
                ->script('assets/js/dynamictree/DynamicTree.js')
                ->script('assets/js/jquery.custom.js')
                ->script('assets/js/application.min.js');

        $assets = $asset->render();

        View::bind_global('assets', $assets);
        View::bind_global('is_logged', $is_logged);

        View::set_global('user', Auth::instance()->get_user());
        View::set_global('session', $session);
    }

    public function after() {
        if ($this->_ajax === true) {
            $this->response->body($this->content);
        } else {
            parent::after();
        }
    }

}
