<?php defined('SYSPATH') or die('No direct access allowed.');

abstract class Relio_Controller_Api extends Controller {
    public $output_format = 'json'; // default
    public $input_content; // For handling incoming data from POST and PUT
    public $input_content_type;
    public $input_content_length;

    public function before()
    {
        // get output format from route file extension
        $this->output_format = $this->request->param('format');

        // get intput data for POST and PUT
        $this->input_content = $this->request->body();
        $this->input_content_type = (isset($_SERVER['CONTENT_TYPE'])) ? $_SERVER['CONTENT_TYPE'] : FALSE;
        $this->input_content_length = (isset($_SERVER['CONTENT_LENGTH'])) ? $_SERVER['CONTENT_LENGTH'] : FALSE;
    }

    protected function rest_output($data = array(), $http_status = 200)
    {
        if (empty($data))
        {
            $this->response->status(404);
            return;
        }

        $this->response->status($http_status);

        $mime = File::mime_by_ext($this->output_format);
        $format_method = '_format_' . $this->output_format;

        // If the format method exists, call and return the output in that format
        if (method_exists($this, $format_method))
        {
            $output_data = $this->$format_method($data);

            $this->response->headers('content-type', File::mime_by_ext($this->output_format));
            $this->response->headers('content-length', (string) strlen($output_data));
            $this->response->body($output_data);
        }
        else
        {
            $this->response->body(var_export( $data, true));
        }
    }

    private function _format_xml($data = array(), $structure = NULL, $basenode = 'xml')
    {
        //return Cambiata_Format::to_xml($data, $structure, $basenode);
    }

    private function _format_json($data = array())
    {
        return json_encode($data);
    }

    private function _format_html($data = array())
    {
        return '<pre>' . var_export($data, true) . '</pre>';
    }
}