<?php defined('SYSPATH') or die('No direct script access.');

class Relio_HttpAuth {
    const X_AUTH_TOKEN = "HTTP-X-AUTH-TOKEN";

    const LEGACY_X_AUTH_TOKEN = 'HTTP_X_AUTH_TOKEN';
    
    static public function basic_http_auth($callback_class, $callback_method)
    {
        $username = NULL;
        $password = NULL;
        
        if (isset($_SERVER['PHP_AUTH_USER']))
        {
            $username = $_SERVER['PHP_AUTH_USER'];
            $password = $_SERVER['PHP_AUTH_PW'];
        }
        elseif (isset($_SERVER['HTTP_AUTHENTICATION']))
        {
            if (strpos(strtolower($_SERVER['HTTP_AUTHENTICATION']), 'basic') === 0)
            {
                list($username, $password) = explode(':', base64_decode(substr($_SERVER['HTTP_AUTHENTICATION'], 6)));
            }
        }
        
        // check login result from callback function...
        $login_check = call_user_func(array($callback_class, $callback_method), $username, $password);
        
        // if not, use browser dialog
        if (!$login_check) self::http_auth_login_dialog();
    }
    
    static public function header_token_auth($callback_class, $callback_method, $token_header_tag)
    {
        // If token header isn't set, return false
        if (!isset($_SERVER[$token_header_tag]) AND !isset($_SERVER[self::LEGACY_X_AUTH_TOKEN])) 
        {
            self::login_fail();
        }
        
        // Get the token
        if (isset($_SERVER[$token_header_tag]))
        {
            $token = $_SERVER[$token_header_tag];
        }
        else if ($_SERVER[self::LEGACY_X_AUTH_TOKEN])
        {
            $token = $_SERVER[self::LEGACY_X_AUTH_TOKEN];
        }
        
        // Check if token is valid from callback function...
        
        $login_check = call_user_func(array($callback_class, $callback_method), $token);
        if (!$login_check) self::login_fail();
    }
    
    static private function http_auth_login_dialog()
    {
        header('WWW-Authenticate: Basic realm="REST API"');
        self::login_fail();
    }
    
    static private function login_fail()
    {
        header('HTTP/1.0 401 Unauthorized');
        header('HTTP/1.1 401 Unauthorized');
        echo json_encode(array('status' => '-99'));
        exit;
        //exit('Not authorized.');
    }
}