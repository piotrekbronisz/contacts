<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Menu
 * @version 3.0
 * @author Piotr Bronisz <piotrekbronisz@gmail.com>
 */
class Menu {
    /**
     * Stopka
     */
    public static function footer()
    {
        $item = Jelly::query('page', Kohana::$config->load('cms.root.'.Request::$lang))->select();
        $tree = $item->children();

        if (!isset($tree))
        {
            return FALSE;
        }

        if (count($tree) <= 0)
        {
            return FALSE;
        }

        return new View('frontend/menu/footer', array('menu' => $tree));
    }

    /**
     * Pobieranie menu dla danej kategorii. Kategoria rozpoznawana jest na podstawie permalink lub poprzez podanie parametru funkcji
     *
     * @param $category
     * @return View
     */
    public static function category_submenu($category = NULL)
    {
        if (is_null($category))
        {
            $route = Jelly::factory('page')->permalink(Request::initial()->uri(), Kohana::$config->load('cms.root.'.Request::$lang));
        }
        else
        {
            $route = Jelly::query('page', $category)->select();
        }

        
        $parents = $route->parents();

        if ($route->level === 0)
        {
            $tree = $route->descendants(false, 'ASC');
        }
        else if ($route->level === 1)
        {
            $tree = $route->descendants(false, 'ASC');
        }
        else if (count($parents) > 0)
        {
            foreach ($parents as $parent) {
                if ($parent->level == 1)
                {
                    $tree = $parent->descendants(false, 'ASC');
                }
            }
        }
        
        if (!isset($tree))
        {
            return FALSE;
        }

        if (count($tree) <= 0)
        {
            return FALSE;
        }

        return new View('frontend/menu/category_submenu', array('menu' => $tree));
    }
}
