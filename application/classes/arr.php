<?php defined('SYSPATH') or die('No direct script access.');

class Arr extends Kohana_Arr {
    /**
     * Metoda sluzy do nadpisania kluczy w tablicy
	 *
     * @param $array
     * @param $replacement_keys
     */
    public static function rename_keys(&$array, $replacement_keys)
    {
		$keys   = array_keys($array);
		$values = array_values($array);
		
		for ($i=0; $i < count($replacement_keys); $i++) { 
			$keys[$i] = $replacement_keys[$i];
		}
		
		$array = array_combine($keys, $values);
	}
}