<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Zadanie sluzy do nadania odpowiednich rol dla kont ktore byly przenoszone z wersji 1.0 odrabianka
 * @author Piotr Bronisz <piotrekbronisz@gmail.com>
 */
class Minion_Task_Misc_Fixrole extends Minion_Task {
    /**
     * An array of config options that this task can accept
     */
    protected $_config = array();

    /**
     * Clears the cache
     */
    public function execute(array $config)
    {
        $items = Jelly::query('user')->select();

        foreach ($items as $it) {
            $it->roles = array(1, 3);
            $it->save();
        }
    }
}