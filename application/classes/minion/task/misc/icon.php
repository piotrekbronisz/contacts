<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Zadanie sluzy do naprawienia sciezki do pliku graficznego
 * @author Piotr Bronisz <piotrekbronisz@gmail.com>
 */
class Minion_Task_Misc_Icon extends Minion_Task {
    /**
     * An array of config options that this task can accept
     */
    protected $_config = array();

    /**
     * Clears the cache
     */
    public function execute(array $config)
    {
        $items = Jelly::query('solution')->select();

        foreach ($items as $it) {
            if ($it->icon == 'upload/exercises/old/') {
                $it->icon = '';
                $it->save();
            }
            /*
            $it->icon = 'upload/exercises/old/'.$it->icon;
            $it->save();*/
        }
    }
}