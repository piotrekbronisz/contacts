<?php defined('SYSPATH') or die('No direct script access.');
 
class Text extends Kohana_Text {
    /**
     * Determine if a string starts with a given needle.
     * 
     * @param string $haystack
     * @param string|array $needles
     * @return bool
     */
    public static function starts_with($haystack, $needles)
    {
        foreach ((array) $needles as $needle) {
            if (strpos($haystack, $needle) === 0) return true;
        }
        
        return false;
    }
    
    /**
     * Determine if a given string ends with a given needle.
     * 
     * @param string $haystack
     * @param string|array $needles
     * @return bool
     */
    public static function ends_with($haystack, $needles)
    {
        foreach ((array) $needles as $needle) {
            if ($needle == substr($haystack, strlen($haystack) - strlen($needle))) return true;
        }
        
        return false;
    }
    
    /**
     * Return the length of the given string.
     * 
     * @param string $value
     * @return int
     */
    public static function length($value)
    {
        return mb_strlen($value);
    }
    
    /**
     * Determine if a given string contains a given sub-string.
     * 
     * @param string $haystack
     * @param string|array $needle
     * @return bool
     */
    public static function contains($haystack, $needle)
    {
        foreach ((array) $needle as $n) {
            if (strpos($haystack, $n) !== false) return true;
        }
        
        return false;
    }

    /**
     * Return first char as uppercase
     *
     * @param $string
     * @return string
     */
    public static function mb_ucfirst($string)
    {
        $string = mb_strtoupper(mb_substr($string, 0, 1)) . mb_substr($string, 1);
        return $string;
    }
    
    public static function odmiana($liczba, $jeden, $dwa, $siedem) {
        $liczba = trim($liczba);
        $sl = (int)substr('0' . $liczba, -2);
          
        if ($sl == 1 && $liczba != '0') {
            return $jeden;
        }
          
        if ($sl > 1 && $sl < 5) {
            return $dwa;
        }
          
        if ($sl > 21) {
            $sll = (int)substr($liczba, -1);
            if ($sll > 0 && $sll < 5) {
                return $dwa;
            }
        }
        
        return $siedem;
    }
     
    public static function clean_name($title, $separator = '-', $ascii_only = false) {
        if ($ascii_only === true) {
            // Transliterate non-ASCII characters
            $title = UTF8::transliterate_to_ascii($title);

            // Remove all characters that are not the separator, a-z, 0-9, or whitespace
            $title = preg_replace('![^' . preg_quote($separator) . 'a-z0-9\s]+!', '', strtolower($title));
        }
        else {
            // Remove all characters that are not the separator, letters, numbers, or whitespace
            $title = preg_replace('![^' . preg_quote($separator) . '\pL\pN\s]+!u', '', UTF8::strtolower($title));
        }

        // Replace all separator characters and whitespace by a single separator
        $title = preg_replace('![' . preg_quote($separator) . '\s]+!u', $separator, $title);

        $search = array('ć', 'ś', 'ą', 'ż', 'ó', 'ł', 'ś', 'ź', 'ń', 'ę');
        $replace = array('c', 's', 'a', 'z', 'o', 'l', 's', 'z', 'n', 'e');

        $title = str_replace($search, $replace, $title);

        // Trim separators from the beginning and end
        return trim($title, $separator);
    }
}