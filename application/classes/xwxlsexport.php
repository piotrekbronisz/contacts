<?php defined('SYSPATH') or die('No direct script access.');

require_once Kohana::find_file('vendor/', 'PHPExcel');
require_once Kohana::find_file('vendor/PHPExcel/Writer/', 'Excel5');

/**
 * Class Xwxlsexport
 * @version 1.0
 * @author Piotr Bronisz <piotrekbronisz@gmail.com>
 */
class Xwxlsexport {
    public static function export2xls($collection, $headers = array(), $filename = null)
    {
        error_reporting(0); //to avoid printing strict standards warnings
        
        $objPHPExcel = new PHPExcel();
        
        $objPHPExcel->getProperties()->setCreator("xwXLSexport");
        $objPHPExcel->getProperties()->setLastModifiedBy("xwXLSexport");
        $objPHPExcel->getProperties()->setTitle("Eksport XLS");
        $objPHPExcel->getProperties()->setSubject("Eksport XLS");
        $objPHPExcel->getProperties()->setDescription("Eksport XLS");
        
        $objPHPExcel->setActiveSheetIndex(0);
        
        if (! empty($headers))
        {
            $int_colCounter = 0;
            foreach ($headers as $head) {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($int_colCounter, 1, $head);
                
                $int_colCounter++;
            }
        }
        
        $int_colCounter = 0;
        $int_rowCounter = 2;
        
        foreach ($collection as $col) {
            foreach ($col as $c) {
                if ($c === true)
                {
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($int_colCounter, $int_rowCounter, 'v');
                }
                else if ($c === false)
                {
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($int_colCounter, $int_rowCounter, '--');
                }
                else
                {
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($int_colCounter, $int_rowCounter, $c);
                }
                
                $int_colCounter++;
            }
            
            $int_colCounter = 0;
            $int_rowCounter++;
        }
        header('Content-Type: application/vnd.ms-excel');
        
        if ($filename === NULL)
        {
            header('Content-Disposition: attachment;filename="eksport_'.date('Y_m_d').'.xls"');
        }
        else
        {
            header('Content-Disposition: attachment;filename="'.$filename.'"');
        }
        
        header('Cache-Control: max-age=0');
        
        $objWriter = new PHPExcel_Writer_Excel5($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        
        exit;
    }
}