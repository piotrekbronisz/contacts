<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Controller_Api_Country
 * @version 1.0
 * @author Piotr Bronisz <piotrekbronisz@gmail.com>
 */
class Controller_Api_Country extends Relio_Controller_Api {
    public function before()
    {
        parent::before();
    }

    /**
     * Lista
     * @author Piotr Bronisz <piotrekbronisz@gmail.com>
     */
    public function action_index()
    {
        if ($this->request->method() === Request::GET)
        {
            $items = Jelly::query('country')->select();

            $return = array();
            foreach ($items as $it) {
                $return[] = array(
                    'id' => $it->id,
                    'name' => $it->name
                );
            }

            $this->rest_output($return);
        }
        else
        {
            $this->rest_output(array(
                'status' => 0
            ));
        }
    }
}