<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Controller_Api_Faq
 * @version 1.0
 * @author Piotr Bronisz <piotrekbronisz@gmail.com>
 */
class Controller_Api_Faq extends Relio_Controller_Api {
    public function before()
    {
        parent::before();
    }

    /**
     * Lista
     * @author Piotr Bronisz <piotrekbronisz@gmail.com>
     */
    public function action_index()
    {
        if ($this->request->method() === Request::GET)
        {
            $return = array();

            $items = Jelly::query('pages_article')
                ->where('page_id', '=', 331)
                ->select();

            foreach ($items as $it) {
                $return[] = array(
                    'id' => $it->id,
                    'name' => $it->name,
                    'content' => $it->content_short
                );
            }

            $this->rest_output($return);
        }
        else
        {
            $this->rest_output(array(
                'status' => 0
            ));
        }
    }
}