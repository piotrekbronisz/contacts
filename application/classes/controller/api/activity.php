<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Controller_Api_Activity
 * @version 1.0
 * @author Piotr Bronisz <piotrekbronisz@gmail.com>
 */
class Controller_Api_Activity extends Relio_Controller_Api {
    public function before()
    {
        parent::before();
    }

    /**
     * Lista
     * @author Piotr Bronisz <piotrekbronisz@gmail.com>
     */
    public function action_index()
    {
        if ($this->request->method() === Request::POST)
        {
            $return = array();

            $items = Jelly::query('activity')
                ->with('city')
                ->with('category')
                ->with('category:category')
                ->filter($this->request->post('term'))
                ->select();

            foreach ($items as $it) {
                $return[] = array(
                    'id' => $it->id,
                    'name' => $it->name,
                    'content' => $it->content,
                    'city' => $it->city->name,
                    'price' => $it->price,
                    'address' => $it->address,
                    'homepage' => $it->homepage,
					'icon' => $it->get_icon(),
                    'email' => $it->email,
                    'phone' => $it->phone,
                    'lat' => $it->lat,
                    'lng' => $it->lng
                );
            }

            $this->rest_output($return);
        }
        else
        {
            $this->rest_output(array(
                'status' => 0
            ));
        }
    }
}