<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Controller_Api_Category
 * @version 1.0
 * @author Piotr Bronisz <piotrekbronisz@gmail.com>
 */
class Controller_Api_Category extends Relio_Controller_Api {
    public function before()
    {
        parent::before();
    }

    /**
     * Lista
     * @author Piotr Bronisz <piotrekbronisz@gmail.com>
     */
    public function action_index()
    {
        if ($this->request->method() === Request::GET)
        {
            $items = Jelly::query('activity_category')->select();

            $return = array();
            foreach ($items as $it) {
                $return[] = array(
                    'id' => $it->id,
                    'name' => $it->name
                );
            }

            $this->rest_output($return);
        }
        else
        {
            $this->rest_output(array(
                'status' => 0
            ));
        }
    }

    /**
     * Podkategorie
     * @author Piotr Bronisz <piotrekbronisz@gmail.com>
     */
    public function action_subcategory()
    {
        if ($this->request->method() === Request::POST)
        {
            $items = Jelly::query('activity_subcategory')
                ->filter($this->request->post('term'))
                ->select();

            $return = array();
            foreach ($items as $it) {
                $return[] = array(
                    'id' => $it->id,
                    'name' => $it->name
                );
            }

            $this->rest_output($return);
        }
        else
        {
            $this->rest_output(array(
                'status' => 0
            ));
        }
    }
}