<?php defined('SYSPATH') or die('No direct script access.');

/**
 * Class Controller_Api_User
 * @version 1.0
 * @author Piotr Bronisz <piotrekbronisz@gmail.com>
 */
class Controller_Api_User extends Relio_Controller_Api {
    public function before()
    {
        parent::before();

        //Authorize
        //Relio_HttpAuth::basic_http_auth($this, 'basic_auth_callback');

        if (!in_array($this->request->action(), array('login', 'lostpassword', 'register', 'logout')))
        {
            Relio_HttpAuth::header_token_auth($this, 'token_auth_callback', Relio_HttpAuth::X_AUTH_TOKEN);
        }
    }
    /*
    static public function basic_auth_callback($username, $password)
    {
        return true;
    }
    */
    static public function token_auth_callback($token)
    {
        if (Request::$user_agent == 'coco-api')
        {
            $check = Jelly::query('security_token')
                ->where('token', '=', $token)
                ->limit(1)
                ->select();

            if ($check->loaded())
            {
                $check->used_on = date('Y-m-d H:i:s', time());
                $check->save();

                return TRUE;
            }
            else
            {
                return FALSE;
            }
        }
        else
        {
            return FALSE;
        }
    }

    /**
     * Logowanie
     * @author Piotr Bronisz <piotrekbronisz@gmail.com>
     */
    public function action_login()
    {
        if ($this->request->method() === Request::POST)
        {
            if (Auth::instance()->login($this->request->post('username'), $this->request->post('password')))
            {
                $token = Text::random('alnum', 32);

                $item = Jelly::query('user', Auth::instance()->get_user()->id)->select();
                $item->token = $token;
                $item->save();

                $security = Jelly::factory('security_token');
                $security->token = $token;
                $security->user = $item->id;
                $security->used_on = date('Y-m-d H:i:s', time());
                $security->save();

                $return = array(
                    'id' => $item->id,
                    'firstname' => $item->firstname,
                    'lastname' => $item->lastname,
                    'username' => $item->username,
                    'token' => $token
                );
            }
            else
            {
                $return = array(
                    'status' => 0
                );
            }

            $this->rest_output($return);
        }
        else
        {
            $this->rest_output(array(
                'status' => 0
            ));
        }
    }

    /**
     * Rejestracja
     * @author Piotr Bronisz <piotrekbronisz@gmail.com>
     */
    public function action_register()
    {
        if ($this->request->method() === Request::POST)
        {
            $post = Validation::factory($this->request->post())
                ->rule('email', 'not_empty')
                ->rule('email', 'email')
                ->rule('email', 'Model_User::is_email_uniq', array(':validation', ':field'))
                ->rule('username', 'not_empty')
                ->rule('username', 'Model_User::is_username_uniq', array(':validation', ':field'));

            if ($post->check())
            {
                $item = Jelly::factory('user');
                $item->country = $this->request->post('country');
				$item->password = $this->request->post('password');
				$item->password_confirm = $this->request->post('password');
                $item->firstname = $this->request->post('firstname');
                $item->lastname = $this->request->post('lastname');
                $item->username = $this->request->post('username');
                $item->email = $this->request->post('email');
                $item->birth_on = $this->request->post('birth_on');
				$item->roles = array(1);
                $item->save();

                if ($item->saved())
                {
                    $return = array(
                        'status' => 1
                    );
                }
                else
                {
                    $return = array(
                        'status' => 0
                    );
                }
            }
            else
            {
                $errors = $post->errors();

                if (isset($errors['email']))
                {
                    $return = array(
                        'status' => 2
                    );
                }
                else if (isset($errors['username']))
                {
                    $return = array(
                        'status' => 3
                    );
                }
                else
                {
                    $return = array(
                        'status' => 0
                    );
                }
            }
        }
        else
        {
            $return = array(
                'status' => 0
            );
        }

        $this->rest_output($return);
    }

    /**
     * Przypomnienie hasła
     * @author Piotr Bronisz <piotrekbronisz@gmail.com>
     */
    public function action_lostpassword()
    {
        if ($this->request->method() === Request::POST)
        {
            $item = Jelly::query('user')
                ->where('email', '=', Security::xss_clean($this->request->post('email')))
                ->limit(1)
                ->select();

            if ($item->loaded())
            {
                $item->code = Text::random();
                $item->save();

                xwContentTemplateWrapper::instance()
                    ->setParameters(array(
                        '%link%' => URL::site('user/regenerate/'.$item->code, 'http', false)
                    ))
                    ->setTemplate('password_link')
                    ->setFrom(CFG_WEBMASTER_EMAIL)
                    ->setTo($item->email)
                    ->send();

                $return = array(
                    'status' => 1
                );
            }
            else
            {
                $return = array(
                    'status' => 0
                );
            }
        }
        else
        {
            $return = array(
                'status' => 0
            );
        }


        $this->rest_output($return);
    }

    /**
     * Wylogowanie
     * @author Piotr Bronisz <piotrekbronisz@gmail.com>
     */
    public function action_logout()
    {
        if ($this->request->method() === Request::POST)
        {
            $item = Jelly::query('user', $this->request->post('user_id'))->select();
            $item->token = '';
            $item->save();

            if ($item->saved())
            {
                $return = array(
                    'status' => 1
                );
            }
            else
            {
                $return = array(
                    'status' => 0
                );
            }
        }
        else
        {
            $return = array(
                'status' => 0
            );
        }

        $this->rest_output($return);
    }
}