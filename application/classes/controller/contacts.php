<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Contacts extends Relio_Controller_Website {

    public function action_index() {
        $this->template->content = View::factory('frontend/contacts/list.tpl')
                ->bind('items', $items)
                ->bind('pagination', $pagination)
                ->bind('current_sort', $current_sort)
                ->bind('sort', $sort);

        $builder = Jelly::query('user')
                ->join('roles_users', 'LEFT')
                ->on('users.id', '=', 'roles_users.user_id')
                ->by_role('2')
                ->group_by('users.id');

        $order_how = $this->request->query('how') ? $this->request->query('how') : 'DESC';

        $order_direction = ($order_how == 'DESC') ? 'ASC' : 'DESC';

        $sort = array(
            'username' => URL::query(array('sort' => 'username', 'how' => $order_direction)),
            'firstname' => URL::query(array('sort' => 'firstname', 'how' => $order_direction)),
            'lastname' => URL::query(array('sort' => 'lastname', 'how' => $order_direction)),
            'email' => URL::query(array('sort' => 'email', 'how' => $order_direction)),
            'phonenumber' => URL::query(array('sort' => 'phonenumber', 'how' => $order_direction)),
            'address' => URL::query(array('sort' => 'address', 'how' => $order_direction)),
            'zip' => URL::query(array('sort' => 'zip', 'how' => $order_direction)),
            'city' => URL::query(array('sort' => 'city', 'how' => $order_direction))
        );

        switch ($this->request->query('sort')) {
            case 'username':
                $builder->order_by('username', $order_how);
                $current_sort = array('type' => 'username', 'how' => $order_how);
                break;
            case 'firstname':
                $builder->order_by('firstname', $order_how);
                $current_sort = array('type' => 'firstname', 'how' => $order_how);
                break;
            case 'lastname':
                $builder->order_by('lastname', $order_how);
                $current_sort = array('type' => 'lastname', 'how' => $order_how);
                break;
            case 'email':
                $builder->order_by('email', $order_how);
                $current_sort = array('type' => 'email', 'how' => $order_how);
                break;
            case 'phonenumber':
                $builder->order_by('phonenumber', $order_how);
                $current_sort = array('type' => 'phonenumber', 'how' => $order_how);
                break;
            case 'address':
                $builder->order_by('address', $order_how);
                $current_sort = array('type' => 'address', 'how' => $order_how);
                break;
            case 'zip':
                $builder->order_by('zip', $order_how);
                $current_sort = array('type' => 'zip', 'how' => $order_how);
                break;
            case 'city':
                $builder->order_by('city', $order_how);
                $current_sort = array('type' => 'city', 'how' => $order_how);
                break;
            case 'status':
                $builder->order_by('active', $order_how);
                $current_sort = array('type' => 'status', 'how' => $order_how);
                break;
            default:
                break;
        }

        $limit = $this->request->query('limit') ? $this->request->query('limit') : 10;

        $pagination = Pagination::factory();
        $pagination->setup(array(
            'total_items' => $builder->count(),
            'items_per_page' => $limit
        ));
        $items = $builder->offset($pagination->offset)->limit($limit)->execute();
    }

    /**
     * Dodawanie
     * @author Piotr Bronisz <piotrekbronisz@gmail.com>
     */
    public function action_add() {
        $this->template->content = View::factory('frontend/contacts/add.tpl')
                ->bind('errors', $errors);

        if ($_POST) {
            $post = Validation::factory($_POST)
                    ->rule('email', 'not_empty')
                    ->rule('email', 'email')
                    ->rule('email', 'Model_User::is_email_uniq', array(':validation', ':field'))
                    ->rule('firstname', 'not_empty')
                    ->rule('lastname', 'not_empty')
                    ->rule('phonenumber', 'not_empty')
                    ->rule('address', 'not_empty')
                    ->rule('zip', 'not_empty')
                    ->rule('city', 'not_empty');


            if ($post->check()) {
                $item = Jelly::factory('user');
                $item->username = $this->request->post('email');
                $item->email = $this->request->post('email');
                $item->firstname = $this->request->post('firstname');
                $item->lastname = $this->request->post('lastname');
                $item->phonenumber = $this->request->post('phonenumber');
                $item->address = $this->request->post('address');
                $item->zip = $this->request->post('zip');
                $item->city = $this->request->post('city');
                $item->active = 1;
                $item->roles = array('1', '2');
                $item->save();

                Tracker::track('Add new Contact ID: ' . $item->id);

                Hint::success('backend.common.success.add');

                $this->request->redirect('contacts');
            } else {
                $errors = $post->errors('contacts/form');
            }
        }
    }

    /**
     * Edycja
     * @author Piotr Bronisz <piotrekbronisz@gmail.com>
     */
    public function action_edit() {
        $this->template->content = View::factory('frontend/contacts/edit.tpl')
                ->bind('errors', $errors)
                ->bind('item', $item);

        $item = Jelly::query('user', $this->request->param('id'))->select();

        if ($_POST) {
            $post = Validation::factory($_POST)
                    ->rule('id', 'not_empty')
                    ->rule('firstname', 'not_empty')
                    ->rule('lastname', 'not_empty')
                    ->rule('email', 'not_empty')
                    ->rule('email', 'email')
                    ->rule('email', 'Model_User::email_change', array(':validation', ':field'))
                    ->rule('phonenumber', 'not_empty')
                    ->rule('address', 'not_empty')
                    ->rule('city', 'not_empty')
                    ->rule('zip', 'not_empty');

            if ($post->check()) {
                $item->username = $this->request->post('email');
                $item->email = $this->request->post('email');
                $item->firstname = $this->request->post('firstname');
                $item->lastname = $this->request->post('lastname');
                $item->phonenumber = $this->request->post('phonenumber');
                $item->address = $this->request->post('address');
                $item->zip = $this->request->post('zip');
                $item->city = $this->request->post('city');

                $item->save();

                Tracker::track('Edit Contact ID: ' . $item->id);

                Hint::success('backend.common.success.edit');

                $this->request->redirect('contacts');
            } else {
                $errors = $post->errors('contacts/form');
            }
        }
    }

    /**
     * Usuwanie
     * @author Piotr Bronisz <piotrekbronisz@gmail.com>
     */
    public function action_delete() {
        $item = Jelly::query('user', $this->request->param('id'))->select();

        if ($item->loaded()) {
            if ($this->request->param('id') == 1) {
                Hint::error('backend.common.error.delete');

                $this->request->redirect('admin');
            } else {
                Tracker::track('Delete Contact ID: ' . $item->id);

                $item->delete();

                Hint::success('backend.common.success.delete');

                $this->request->redirect('contacts');
            }
        } else {
            Hint::error('backend.common.error.delete');

            $this->request->redirect('admin');
        }
    }

    /**
     * Edycja profilu
     * @author Piotr Bronisz <piotrekbronisz@gmail.com>
     */
    public function action_profile() {
        $this->template->content = View::factory('frontend/contacts/profile.tpl')
                ->bind('errors', $errors)
                ->bind('item', $item);

        $item = Jelly::query('user', Auth::instance()->get_user()->id)->select();

        if ($_POST) {
            $post = Validation::factory($_POST)
                    ->rule('id', 'not_empty')
                    ->rule('firstname', 'not_empty')
                    ->rule('lastname', 'not_empty')
                    ->rule('email', 'not_empty')
                    ->rule('email', 'email')
                    ->rule('email', 'Model_Auth_User::email_change', array(':validation', ':field'))
                    ->rule('phonenumber', 'not_empty')
                    ->rule('address', 'not_empty')
                    ->rule('zip', 'not_empty')
                    ->rule('city', 'not_empty');
            

            if ($post->check()) {
                $item->email = $this->request->post('email');
                $item->firstname = $this->request->post('firstname');
                $item->lastname = $this->request->post('lastname');
                $item->phonenumber = $this->request->post('phonenumber');
                $item->address = $this->request->post('address');
                $item->zip = $this->request->post('zip');
                $item->city = $this->request->post('city');
                
                $item->save();

                Hint::success('backend.common.success.edit');

                $this->request->redirect('contacts/profile');
            } else {
                $errors = $post->errors('contacts/profile/form');
            }
        }
    }


    /**
     * Masowe akcje
     * @author Piotr Bronisz <piotrekbronisz@gmail.com>
     */
    public function action_checkbox() {
        if ($_POST) {
            $method_to_call = '_checkbox_' . $this->request->post('action');
            if (method_exists($this, $method_to_call)) {
                $this->$method_to_call();
            }
        }

        Hint::success('backend.common.success.checkbox');

        $this->request->redirect('contacts');
    }


}
