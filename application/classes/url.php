<?php defined('SYSPATH') or die('No direct script access.');

class URL extends Kohana_URL {
    /**
     * Extension of URL::site that adds a third parameter for setting a language key.
     * The current language (Request::$lang) is used by default.
     *
     * echo URL::site('foo/bar', FALSE, TRUE, 'fr'); // custom language
     * echo URL::site('foo/bar', FALSE, TRUE, FALSE); // no language
     *
     * @param string $uri Site URI to convert
     * @param mixed $protocol Protocol string or [Request] class to use protocol from
     * @param boolean $index Include the index_page in the URL
     * @param mixed $lang Language key to prepend to the URI, or FALSE to not prepend a language
     * @return string
     * @uses Lang::$default_prepended
     * @uses Request::$lang
     */
    public static function site($uri = '', $protocol = NULL, $index = TRUE, $lang = TRUE)
    {
        // Prepend language to URI if it needs to be prepended or it's not the default
        if ($lang === TRUE AND (Lang::$default_prepended OR Request::$lang !== Lang::$default))
        {
            // Prepend the current language to the URI
            $uri = Request::$lang.'/'.ltrim($uri, '/');
        }
        elseif (is_string($lang))
        {
            // Prepend a custom language to the URI
            $uri = $lang.'/'.ltrim($uri, '/');
        }

        return parent::site($uri, $protocol, $index);
    }

    public static function current_site($uri = '')
	{
        $current = Request::current()->uri();
        
        return empty($uri) ? $current : $current . '/' . $uri;
    }

    /**
     * Formats a URL to contain a protocol at the beginning.
     *
     * @param   string  possibly incomplete URL
     * @return  string
     */
    public static function format($str = '') {
        // Clear protocol-only strings like "http://"
        if ($str === '' or substr($str, -3) === '://') {
            return '';
        }

        // If no protocol given, prepend "http://" by default
        if (strpos($str, '://') === false) {
            return 'http://' . $str;
        }
        
        // Return the original URL
        return $str;
    }

    /**
     * Convert a phrase to a URL-safe title.
     *
     * @param   string   phrase to convert
     * @param   string   word separator (any single character)
     * @param   boolean  transliterate to ASCII
     * @return  string
     */
    public static function prepare_url($title, $separator = '-', $ascii_only = false) {
        
        if ($ascii_only === true) {
            // Transliterate non-ASCII characters
            $title = UTF8::transliterate_to_ascii($title);

            // Remove all characters that are not the separator, a-z, 0-9, or whitespace
            $title = preg_replace('![^' . preg_quote($separator) . 'a-z0-9\s]+!', '', strtolower($title));
        }
        else {
            // Remove all characters that are not the separator, letters, numbers, or whitespace
            $title = preg_replace('![^' . preg_quote($separator) . '\pL\pN\s]+!u', '', UTF8::strtolower($title));
        }

        // Replace all separator characters and whitespace by a single separator
        $title = preg_replace('![' . preg_quote($separator) . '\s]+!u', $separator, $title);

        $search = array(' ', '/', '\'', '&', '%', 'ć', 'ś', 'ą', 'ż', 'ó', 'ł', 'ś', 'ź', 'ń', 'ę');
        $replace = array('_', '-', '-', 'and', 'procent', 'c', 's', 'a', 'z', 'o', 'l', 's', 'z', 'n', 'e');

        $title = str_replace($search, $replace, $title);

        // Trim separators from the beginning and end
        return trim($title, $separator);
    }
    
    public static function convert_fck($text) {
        $server = URL::base(false, 'http');
        $attributes = array('src');
        
        foreach ($attributes as $attr) {
            $pattern[] = '/'.$attr.'=["'."'".']??(.*)["'."'".']??([ >])/Uis';
            $replacement[] = $attr.'="'.$server.'\1"\2';
        }
        
        $text = preg_replace('<--.*-->', '', $text); 
        $text = str_replace('', '"', $text);
        $text = str_replace('\"', '"', $text);
        $text = str_replace('="http', 'httpChuCiWDup', $text);
        $text = str_replace('="mailto', 'httpChuCiWOko', $text); 
        
        $text = preg_replace($pattern, $replacement, $text);
        $text = str_replace( 'httpChuCiWDup', '="http', $text);
        $text = str_replace( 'httpChuCiWOko','="mailto', $text);
        
        return $text;
    }
}
