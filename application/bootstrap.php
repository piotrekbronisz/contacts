<?php

defined('SYSPATH') or die('No direct script access.');

// -- Environment setup --------------------------------------------------------
// Load the core Kohana class
require SYSPATH . 'classes/kohana/core' . EXT;

if (is_file(APPPATH . 'classes/kohana' . EXT)) {
    // Application extends the core
    require APPPATH . 'classes/kohana' . EXT;
} else {
    // Load empty core extension
    require SYSPATH . 'classes/kohana' . EXT;
}

/**
 * Set the default time zone.
 *
 * @see  http://kohanaframework.org/guide/using.configuration
 * @see  http://php.net/timezones
 */
date_default_timezone_set('Europe/Warsaw');

/**
 * Set the default locale.
 *
 * @see  http://kohanaframework.org/guide/using.configuration
 * @see  http://php.net/setlocale
 */
setlocale(LC_ALL, 'pl_PL.utf-8');

/**
 * Enable the Kohana auto-loader.
 *
 * @see  http://kohanaframework.org/guide/using.autoloading
 * @see  http://php.net/spl_autoload_register
 */
spl_autoload_register(array('Kohana', 'auto_load'));

/**
 * Enable the Kohana auto-loader for unserialization.
 *
 * @see  http://php.net/spl_autoload_call
 * @see  http://php.net/manual/var.configuration.php#unserialize-callback-func
 */
ini_set('unserialize_callback_func', 'spl_autoload_call');

// -- Configuration and initialization -----------------------------------------

/**
 * Set the default language
 */
I18n::lang('pl-pl');

Cookie::$salt = 'contacts';

/**
 * Set Kohana::$environment if a 'KOHANA_ENV' environment variable has been supplied.
 *
 * Note: If you supply an invalid environment name, a PHP warning will be thrown
 * saying "Couldn't find constant Kohana::<INVALID_ENV_NAME>"
 */
if (isset($_SERVER['KOHANA_ENV'])) {
    Kohana::$environment = constant('Kohana::' . strtoupper($_SERVER['KOHANA_ENV']));
}

/**
 * Initialize Kohana, setting the default options.
 *
 * The following options are available:
 *
 * - string   base_url    path, and optionally domain, of your application   NULL
 * - string   index_file  name of your index file, usually "index.php"       index.php
 * - string   charset     internal character set used for input and output   utf-8
 * - string   cache_dir   set the internal cache directory                   APPPATH/cache
 * - boolean  errors      enable or disable error handling                   TRUE
 * - boolean  profile     enable or disable internal profiling               TRUE
 * - boolean  caching     enable or disable internal caching                 FALSE
 */
Kohana::init(array(
    'base_url' => '/',
    'index_file' => FALSE,
    'errors' => false,
    'profile' => Kohana::$environment === Kohana::DEVELOPMENT,
    'caching' => Kohana::$environment === Kohana::PRODUCTION
));

/**
 * Attach the file write to logging. Multiple writers are supported.
 */
Kohana::$log->attach(new Log_File(APPPATH . 'logs'));

/**
 * Attach a file reader to config. Multiple readers are supported.
 */
Kohana::$config->attach(new Config_File);

/**
 * Enable modules. Modules are referenced by a relative or absolute path.
 */
Kohana::modules(array(
    'profilertoolbar' => MODPATH . 'profilertoolbar',
    'sso' => MODPATH . 'sso',
    'jelly' => MODPATH . 'jelly',
    'jelly-auth' => MODPATH . 'jelly-auth',
    'jelly-mptt' => MODPATH . 'jelly-mptt',
    'auth' => MODPATH . 'auth',
    'cache' => MODPATH . 'cache',
    'database' => MODPATH . 'database',
    'minion' => MODPATH . 'minion',
    'smarty' => MODPATH . 'smarty',
    'image' => MODPATH . 'image',
    'purifier' => MODPATH . 'purifier',
    'tracker' => MODPATH . 'tracker',
    'flexiasset' => MODPATH . 'flexiasset',
    'flexilang' => MODPATH . 'flexilang',
    'pagination' => MODPATH . 'pagination',
    'email' => MODPATH . 'email',
    'hint' => MODPATH . 'hint',
    'google-maps' => MODPATH . 'google-maps',
    'breadcrumbs' => MODPATH . 'breadcrumbs',
    'cgffmpeg' => MODPATH . 'cgffmpeg',
    'pages/articles' => APPPATH . 'modules/pages/articles',
    'pages/news' => APPPATH . 'modules/pages/news',
    'pages/faq' => APPPATH . 'modules/pages/faq',
    'pages/contact' => APPPATH . 'modules/pages/contact'
));

/**
 * Konfiguracja globalna
 */
$settings = DB::select()->from('settings')
        ->execute()
        ->as_array();


foreach ($settings as $key => $row) {
    define('CFG_' . $row['key'], $row['value']);
}

if (!Route::cache()) {


    /**
     * Błędy
     */
    Route::set('error', 'error/<action>(/<message>)', array('action' => '[0-9]++', 'message' => '.+'))
            ->defaults(array(
                'controller' => 'error_handler'
    ));

    /**
     * Domyślne
     */
    Route::set('default', '(<controller>(/<action>(/<id>(/<stuff>(/<stuff2>(/<stuff3>(/<stuff4>)))))))', array('id' => '.*?', 'stuff' => '.*?', 'stuff2' => '.*?', 'stuff3' => '.*?', 'stuff4' => '.*?'))
            ->defaults(array(
                'controller' => 'index',
                'action' => 'index',
                'id' => NULL,
                'stuff' => NULL,
                'stuff2' => NULL,
                'stuff3' => NULL,
                'stuff4' => NULL
    ));

    Route::cache(Kohana::$environment === Kohana::PRODUCTION);
}

/**
 * Execute the main request. A source of the URI can be passed, eg: $_SERVER['PATH_INFO'].
 * If no source is specified, the URI will be automatically detected.
 */
echo Request::factory()
        ->execute()
        ->send_headers()
        ->body();
