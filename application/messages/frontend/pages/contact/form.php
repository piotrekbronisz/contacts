<?php defined('SYSPATH') OR die('No direct access allowed.'); 
 
return array(
    'name' => array(
        'not_empty' => 'Musisz podać imię i nazwisko'
    ),
    'email' => array(
        'not_empty' => 'Musisz podać e-mail',
        'email' => 'Musisz podać poprawny e-mail'
    ),
    'phone' => array(
    	'not_empty' => 'Musisz podać telefon'
    ),
    'subject' => array(
        'not_empty' => 'Musisz podać temat'
    ),
    'content' => array(
        'not_empty' => 'Musisz podać tresć'
    )
);