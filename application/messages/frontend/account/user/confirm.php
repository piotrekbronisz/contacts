<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(
    'firstname' => array(
        'not_empty' => 'Musisz podać imię'
    ),
    'lastname' => array(
        'not_empty' => 'Musisz nazwisko'
    ),
    'country' => array(
        'not_empty' => 'Musisz wybrać kraj'
    ),
    'province' => array(
        'not_empty' => 'Musisz wybrać województwo'
    ),
    'city' => array(
        'not_empty' => 'Musisz podać miejscowość'
    ),
    'postcode' => array(
        'not_empty' => 'Musisz podać kod pocztowy'
    ),
    'address' => array(
        'not_empty' => 'Musisz podać ulicę'
    ),
    'home' => array(
        'not_empty' => 'Musisz podać nr domu'
    )
);