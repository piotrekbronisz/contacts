<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(
    'password' => array(
        'not_empty' => 'Musisz podać hasło'
    ),
    'password_confirm' => array(
        'not_empty' => 'Musisz podać hasło',
        'matches' => 'Pole powtórz hasło musi być zgodne z polem hasło'
    )
);