<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(
    'username' => array(
        'not_empty' => 'Musisz podać login'
    ),
    'password' => array(
        'not_empty' => 'Musisz podać hasło'
    ),
);