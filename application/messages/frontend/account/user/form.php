<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(
    'email' => array(
        'not_empty' => 'Musisz podać adres e-mail',
        'email' => 'Musisz podać poprawny adres e-mail',
        'not_uniq' => 'Ten adres e-mail jest już zarejestrowany'
    ),
    'password' => array(
        'not_empty' => 'Musisz podać hasło'
    ),
    'password_confirm' => array(
        'not_empty' => 'Musisz podać hasło',
        'matches' => 'Pole powtórz hasło musi być zgodne z polem hasło'
    ),
    'firstname' => array(
        'not_empty' => 'Musisz podać imię'
    ),
    'lastname' => array(
        'not_empty' => 'Musisz nazwisko'
    ),
    'captcha' => array(
        'not_empty' => 'Musisz podać kod z obrazka',
        'Validation::captcha' => 'Podany kod jest błędny'
    ),
    'rules' => array(
        'not_empty' => 'Musisz zaakceptować regulamin'
    )
);