<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(
    'email' => array(
        'not_empty' => 'Musisz podać e-mail',
        'email' => 'Musisz podac poprawny e-mail'
    )
);