<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(
    'icon' => array(
        'not_empty' => 'Musisz wybrać zdjęcie',
        'Validation::image' => 'Minimalne wymiary zdjęcia to 200x200px'
    )
);