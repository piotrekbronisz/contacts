<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(
    'firstname' => array(
        'not_empty' => 'Musisz podać imię'
    ),
    'lastname' => array(
        'not_empty' => 'Musisz podać nazwisko'
    )
);