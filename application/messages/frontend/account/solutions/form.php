<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(
    'book' => array(
        'not_empty' => 'Wybierz podręcznik'
    ),
    'page' => array(
        'not_empty' => 'Wybierz stronę'
    ),
    'exercise' => array(
        'not_empty' => 'Wybierz zadanie'
    ),
    'name' => array(
        'not_empty' => 'Musisz podać tytuł'
    ),
    'content' => array(
        'not_empty' => 'Musisz podać treść',
        'at_least' => 'Musisz podać treść lub wybrać zdjęcie'
    ),
    'movie' => array(
        'not_empty' => 'Musisz podać link do filmu',
        'Validaiton::youtube' => 'Musisz podać poprawny adres url do youtube'
    ),
    'filename' => array(
        'not_empty' => 'Musisz wybrać załącznik',
        'at_least' => 'Musisz podać treść lub wybrać zdjęcie'
    )
);