<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(
    'subject' => array(
        'not_empty' => 'Musisz wybrać temat'
    ),
    'content' => array(
        'not_empty' => 'Musisz podać treść wiadomości'
    )
);