<?php defined('SYSPATH') OR die('No direct access allowed.');

return array(
    'backend' => array(
        'common' => array(
            'success' => array(
                'delete' => 'Usunięcie przebiegło pomyślnie',
                'add' => 'Dodanie przebiegło pomyślnie',
                'edit' => 'Edycja przebiegła pomyślnie',
                'checkbox' => 'Operacja przeprowadzona pomyślnie'
            ),
            'error' => array(
                'delete' => 'Wystąpił błąd podczas usuwania'
            )
        )
    )
);