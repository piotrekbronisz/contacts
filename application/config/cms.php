<?php defined('SYSPATH') or die('No direct access allowed.');

return array(
    'system' => array(
        'name' => 'COCO'
    ),
    'root' => array(
        'pl' => '1',
        'en' => '2'
    ),
    'facebook' => array(
        'keys' => array(
            'id' => (Kohana::$environment === Kohana::PRODUCTION) ? '154026617981084' : '320297504813448',
            'secret' => (Kohana::$environment === Kohana::PRODUCTION) ? '072454278c858a539149637a561b1f00' : '0990a821a8474eed602bdee77da905a6'
        )
    ),
    'google' => array(
        'keys' => array(
            'id' => (Kohana::$environment === Kohana::PRODUCTION) ? 'x' : '359414878130-dgkbk8ba509n6ec3ck0720jda8078qvm.apps.googleusercontent.com',
            'secret' => (Kohana::$environment === Kohana::PRODUCTION) ? 'x' : 'jN6FnG1dYJcfSpuUM2qv7Njs'
        )
    )
);