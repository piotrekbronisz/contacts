<?php /* Smarty version Smarty-3.1.5, created on 2015-09-10 16:41:27
         compiled from "C:\xampp\htdocs\coco2\application\views\frontend\contacts\edit.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1227355f18bd87040d4-26818905%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '18111876c57203ac5b72cba7b5f6a31c55ab8561' => 
    array (
      0 => 'C:\\xampp\\htdocs\\coco2\\application\\views\\frontend\\contacts\\edit.tpl',
      1 => 1441896082,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1227355f18bd87040d4-26818905',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.5',
  'unifunc' => 'content_55f18bd88484a',
  'variables' => 
  array (
    'item' => 0,
    'errors' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55f18bd88484a')) {function content_55f18bd88484a($_smarty_tpl) {?><div class="container-fluid">
    <div class="page-header">
        <div class="pull-left">
            <h1>Administratorzy</h1>
        </div>
    </div>
    <div class="breadcrumbs">
        <ul>
            <li>
                <a href="<?php echo URL::site('contacts');?>
">Contacts</a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a href="<?php echo URL::site('contacts');?>
">List</a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a href="javascript:void(0);">
                    Edytuj
                </a>
            </li>
        </ul>
        <div class="close-bread">
            <a href="#"><i class="icon-remove"></i></a>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="box box-bordered box-color">
                <div class="box-title">
                    <h3><i class="icon-th-list"></i> Edit</h3>
                </div>
                <div class="box-content nopadding">
                    <form class="form-vertical form-bordered" method="POST" action="" enctype="multipart/form-data">
                        <input type="hidden" name="id" value="<?php echo $_smarty_tpl->tpl_vars['item']->value->id;?>
" />
                        <div class="control-group <?php if ((($tmp = @$_smarty_tpl->tpl_vars['errors']->value['email'])===null||$tmp==='' ? '' : $tmp)){?>error<?php }?>">
                            <label class="control-label" for="email">E-mail</label>
                            <div class="controls">
                                <input type="text" name="email" value="<?php echo htmlspecialchars((($tmp = @$_POST['email'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['item']->value->email : $tmp));?>
" class="input-xlarge" id="email" >
                                <?php if ((($tmp = @$_smarty_tpl->tpl_vars['errors']->value['email'])===null||$tmp==='' ? '' : $tmp)){?><span class="help-block error"><?php echo (($tmp = @$_smarty_tpl->tpl_vars['errors']->value['email'])===null||$tmp==='' ? '' : $tmp);?>
</span><?php }?>
                            </div>
                        </div>
                        <div class="control-group <?php if ((($tmp = @$_smarty_tpl->tpl_vars['errors']->value['firstname'])===null||$tmp==='' ? '' : $tmp)){?>error<?php }?>">
                            <label class="control-label" for="firstname">Imię</label>
                            <div class="controls">
                                <input type="text" name="firstname" value="<?php echo (($tmp = @$_POST['firstname'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['item']->value->firstname : $tmp);?>
" class="input-xlarge" id="firstname" >
                                <?php if ((($tmp = @$_smarty_tpl->tpl_vars['errors']->value['firstname'])===null||$tmp==='' ? '' : $tmp)){?><span class="help-block error"><?php echo (($tmp = @$_smarty_tpl->tpl_vars['errors']->value['firstname'])===null||$tmp==='' ? '' : $tmp);?>
</span><?php }?>
                            </div>
                        </div>
                        <div class="control-group <?php if ((($tmp = @$_smarty_tpl->tpl_vars['errors']->value['lastname'])===null||$tmp==='' ? '' : $tmp)){?>error<?php }?>">
                            <label class="control-label" for="lastname">Nazwisko</label>
                            <div class="controls">
                                <input type="text" name="lastname" value="<?php echo (($tmp = @$_POST['lastname'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['item']->value->lastname : $tmp);?>
" class="input-xlarge" id="lastname" >
                                <?php if ((($tmp = @$_smarty_tpl->tpl_vars['errors']->value['lastname'])===null||$tmp==='' ? '' : $tmp)){?><span class="help-block error"><?php echo (($tmp = @$_smarty_tpl->tpl_vars['errors']->value['lastname'])===null||$tmp==='' ? '' : $tmp);?>
</span><?php }?>
                            </div>
                        </div>

                        <div class="control-group <?php if ((($tmp = @$_smarty_tpl->tpl_vars['errors']->value['phonenumber'])===null||$tmp==='' ? '' : $tmp)){?>error<?php }?>">
                            <label class="control-label" for="phonenumber">Phone number</label>
                            <div class="controls">
                                <input type="text" name="phonenumber" value="<?php echo (($tmp = @$_POST['phonenumber'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['item']->value->phonenumber : $tmp);?>
" class="input-xlarge" id="lastname" >
                                <?php if ((($tmp = @$_smarty_tpl->tpl_vars['errors']->value['phonenumber'])===null||$tmp==='' ? '' : $tmp)){?><span class="help-block error"><?php echo (($tmp = @$_smarty_tpl->tpl_vars['errors']->value['phonenumber'])===null||$tmp==='' ? '' : $tmp);?>
</span><?php }?>
                            </div>
                        </div>

                        <div class="control-group <?php if ((($tmp = @$_smarty_tpl->tpl_vars['errors']->value['address'])===null||$tmp==='' ? '' : $tmp)){?>error<?php }?>">
                            <label class="control-label" for="address">Address</label>
                            <div class="controls">
                                <input type="text" name="address" value="<?php echo (($tmp = @$_POST['address'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['item']->value->address : $tmp);?>
" class="input-xlarge" id="lastname" >
                                <?php if ((($tmp = @$_smarty_tpl->tpl_vars['errors']->value['address'])===null||$tmp==='' ? '' : $tmp)){?><span class="help-block error"><?php echo (($tmp = @$_smarty_tpl->tpl_vars['errors']->value['address'])===null||$tmp==='' ? '' : $tmp);?>
</span><?php }?>
                            </div>
                        </div>
                            
                        <div class="control-group <?php if ((($tmp = @$_smarty_tpl->tpl_vars['errors']->value['zip'])===null||$tmp==='' ? '' : $tmp)){?>error<?php }?>">
                            <label class="control-label" for="zip">Zip</label>
                            <div class="controls">
                                <input type="text" name="zip" value="<?php echo (($tmp = @$_POST['zip'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['item']->value->zip : $tmp);?>
" class="input-xlarge" id="lastname" >
                                <?php if ((($tmp = @$_smarty_tpl->tpl_vars['errors']->value['zip'])===null||$tmp==='' ? '' : $tmp)){?><span class="help-block error"><?php echo (($tmp = @$_smarty_tpl->tpl_vars['errors']->value['zip'])===null||$tmp==='' ? '' : $tmp);?>
</span><?php }?>
                            </div>
                        </div>
                            
                        <div class="control-group <?php if ((($tmp = @$_smarty_tpl->tpl_vars['errors']->value['city'])===null||$tmp==='' ? '' : $tmp)){?>error<?php }?>">
                            <label class="control-label" for="city">City</label>
                            <div class="controls">
                                <input type="text" name="city" value="<?php echo (($tmp = @$_POST['city'])===null||$tmp==='' ? $_smarty_tpl->tpl_vars['item']->value->city : $tmp);?>
" class="input-xlarge" id="lastname" >
                                <?php if ((($tmp = @$_smarty_tpl->tpl_vars['errors']->value['city'])===null||$tmp==='' ? '' : $tmp)){?><span class="help-block error"><?php echo (($tmp = @$_smarty_tpl->tpl_vars['errors']->value['city'])===null||$tmp==='' ? '' : $tmp);?>
</span><?php }?>
                            </div>
                        </div>
                            
                        <div class="form-actions">
                            <button class="btn btn-primary" type="submit">Zapisz</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div><?php }} ?>