<?php /* Smarty version Smarty-3.1.5, created on 2015-09-10 16:35:12
         compiled from "C:\xampp\htdocs\coco2\application\views\frontend\contacts\list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1754655f185be115763-00285041%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fc9a3ffbed40e9c307391b687cf7a29ec0fad3fd' => 
    array (
      0 => 'C:\\xampp\\htdocs\\coco2\\application\\views\\frontend\\contacts\\list.tpl',
      1 => 1441895710,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1754655f185be115763-00285041',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.5',
  'unifunc' => 'content_55f185be1f809',
  'variables' => 
  array (
    'helper' => 0,
    'sort' => 0,
    'items' => 0,
    'it' => 0,
    'pagination' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55f185be1f809')) {function content_55f185be1f809($_smarty_tpl) {?><?php echo Hint::render(null,true,'hint/frontend/toast');?>

<div class="container-fluid">
    <div class="page-header">
        <div class="pull-left">
            <h1>Contacts</h1>
        </div>
    </div>
    <div class="breadcrumbs">
        <ul>
            <li>
                <a href="<?php echo URL::site('contacts');?>
">Contacts</a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a href="<?php echo URL::site('contacts');?>
">List</a>
                <i class="icon-angle-right"></i>
            </li>
        </ul>
        <div class="close-bread">
            <a href="#"><i class="icon-remove"></i></a>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="box">
                <div class="box-content nopadding">
                    <div class="dataTables_wrapper">
                        <div class="dataTables_length">
                            <label>
                                <div class="btn-group" style="float: left;">
                                    <a href="#" data-toggle="dropdown" class="btn dropdown-toggle"><?php echo intval((($tmp = @$_GET['limit'])===null||$tmp==='' ? "10" : $tmp));?>
 <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="<?php echo URL::site("contacts");?>
<?php echo $_smarty_tpl->tpl_vars['helper']->value->query('limit','10');?>
">10</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo URL::site("contacts");?>
<?php echo $_smarty_tpl->tpl_vars['helper']->value->query('limit','25');?>
">25</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo URL::site("contacts");?>
<?php echo $_smarty_tpl->tpl_vars['helper']->value->query('limit','50');?>
">50</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo URL::site("contacts");?>
<?php echo $_smarty_tpl->tpl_vars['helper']->value->query('limit','100');?>
">100</a>
                                        </li>
                                    </ul>
                                </div>
                                <span>Elementów na strone</span>
                            </label>
                        </div>
                        <div style="float: right; margin: 10px 10px 5px 5px">
                            <a href="<?php echo URL::site("contacts/add");?>
" class="btn"><i class="icon-plus"></i> Dodaj</a>
                        </div>
                        <form action="<?php echo URL::site("contacts/checkbox");?>
" method="post">
                            <table class="table table-hover table-nomargin table-striped table-bordered" style="clear: both;">
                                <thead>
                                    <tr>
                                        <th style="text-align: center;"><input type="checkbox" value="check_none" onclick="javascript:check_all_box(this.form)" /></th>
                                        <th style="text-align: center;"><a href="<?php echo URL::site("contacts");?>
<?php echo $_smarty_tpl->tpl_vars['sort']->value['firstname'];?>
">First name</a></th>
                                        <th style="text-align: center;"><a href="<?php echo URL::site("contacts");?>
<?php echo $_smarty_tpl->tpl_vars['sort']->value['lastname'];?>
">Last name</a></th>
                                        <th style="text-align: center;"><a href="<?php echo URL::site("contacts");?>
<?php echo $_smarty_tpl->tpl_vars['sort']->value['email'];?>
">E-mail</a></th>
                                        <th style="text-align: center;"><a href="<?php echo URL::site("contacts");?>
<?php echo $_smarty_tpl->tpl_vars['sort']->value['phonenumber'];?>
">Phone number</a></th>
                                        <th style="text-align: center;"><a href="<?php echo URL::site("contacts");?>
<?php echo $_smarty_tpl->tpl_vars['sort']->value['address'];?>
">address</a></th>
                                        <th style="text-align: center;"><a href="<?php echo URL::site("contacts");?>
<?php echo $_smarty_tpl->tpl_vars['sort']->value['zip'];?>
">Zip</a></th>
                                        <th style="text-align: center;"><a href="<?php echo URL::site("contacts");?>
<?php echo $_smarty_tpl->tpl_vars['sort']->value['city'];?>
">city</a></th>
                                        <th style="text-align: center;" colspan="2">Opcje</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th colspan="10">
                                            <img src="/assets/img/arrow_ltr.png" />&nbsp;
                                            <select name="action">
                                                <option value="0">-- wybierz --</option>
                                                <option value="delete">Usuń</option>
                                            </select>&nbsp;<button class="btn btn-primary" type="submit">Wykonaj</button>
                                        </th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    <?php  $_smarty_tpl->tpl_vars['it'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['it']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['items']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['it']->key => $_smarty_tpl->tpl_vars['it']->value){
$_smarty_tpl->tpl_vars['it']->_loop = true;
?>
                                        <tr>
                                            <td style="width: 32px; text-align: center;">
                                                <input type="checkbox" name="list[]" value="<?php echo $_smarty_tpl->tpl_vars['it']->value->id;?>
" />
                                            </td>
                                            <td>
                                                <?php echo $_smarty_tpl->tpl_vars['it']->value->firstname;?>

                                            </td>
                                            <td>
                                                <?php echo $_smarty_tpl->tpl_vars['it']->value->lastname;?>

                                            </td>
                                            <td style="text-align: center;">
                                                <?php echo $_smarty_tpl->tpl_vars['it']->value->email;?>

                                            </td>
                                            <td style="text-align: center;">
                                                <?php echo $_smarty_tpl->tpl_vars['it']->value->phonenumber;?>

                                            </td>
                                            <td style="text-align: center;">
                                                <?php echo $_smarty_tpl->tpl_vars['it']->value->address;?>

                                            </td>
                                            <td style="text-align: center;">
                                                <?php echo $_smarty_tpl->tpl_vars['it']->value->zip;?>

                                            </td>
                                            <td style="text-align: center;">
                                                <?php echo $_smarty_tpl->tpl_vars['it']->value->city;?>

                                            </td>

                                            <td style="width: 32px; text-align: center;">
                                                <a href="<?php echo URL::site("contacts/edit/".($_smarty_tpl->tpl_vars['it']->value->id));?>
" rel="tooltip" title="Edytuj" class="btn">
                                                    <i class="icon-pencil"></i>
                                                </a>
                                            </td>
                                            <td style="width: 32px; text-align: center;">
                                                <a href="javascript:void(0);" onclick="javascript:confirm_action('<?php echo URL::site("contacts/delete/".($_smarty_tpl->tpl_vars['it']->value->id));?>
');" rel="tooltip" title="Usuń" class="btn btn-danger">
                                                    <i class="icon-minus-sign"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php }
if (!$_smarty_tpl->tpl_vars['it']->_loop) {
?>
                                        <tr>
                                            <td style="text-align: center;" colspan="7">
                                                Brak danych do wyświetlenia
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
            <?php echo $_smarty_tpl->tpl_vars['pagination']->value->render('pagination/backend/floating');?>

        </div>
    </div>
</div><?php }} ?>