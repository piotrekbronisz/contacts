<?php /* Smarty version Smarty-3.1.5, created on 2015-09-10 16:49:14
         compiled from "C:\xampp\htdocs\coco2\application\views\frontend\contacts\add.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3219655f185cf0e8706-95062375%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3310b8c5381d0514c58f4d5360aeeba4e60c32ac' => 
    array (
      0 => 'C:\\xampp\\htdocs\\coco2\\application\\views\\frontend\\contacts\\add.tpl',
      1 => 1441896524,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3219655f185cf0e8706-95062375',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.5',
  'unifunc' => 'content_55f185cf20984',
  'variables' => 
  array (
    'errors' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_55f185cf20984')) {function content_55f185cf20984($_smarty_tpl) {?><div class="container-fluid">
    <div class="page-header">
        <div class="pull-left">
            <h1>Contacts</h1>
        </div>
    </div>
    <div class="breadcrumbs">
        <ul>
            <li>
                <a href="<?php echo URL::site('contacts');?>
">Contacts</a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a href="<?php echo URL::site('contacts');?>
">List</a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a href="javascript:void(0);">
                    Dodaj
                </a>
            </li>
        </ul>
        <div class="close-bread">
            <a href="#"><i class="icon-remove"></i></a>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="box box-bordered box-color">
                <div class="box-title">
                    <h3><i class="icon-th-list"></i> Dodaj</h3>
                </div>
                <div class="box-content nopadding">
                    <form class="form-vertical form-bordered" method="POST" action="" enctype="multipart/form-data">
                        <div class="control-group <?php if ((($tmp = @$_smarty_tpl->tpl_vars['errors']->value['email'])===null||$tmp==='' ? '' : $tmp)){?>error<?php }?>">
                            <label class="control-label" for="email">E-mail</label>
                            <div class="controls">
                                <input type="text" name="email" value="<?php echo htmlspecialchars((($tmp = @$_POST['email'])===null||$tmp==='' ? '' : $tmp));?>
" class="input-xlarge" id="email" >
                                <?php if ((($tmp = @$_smarty_tpl->tpl_vars['errors']->value['email'])===null||$tmp==='' ? '' : $tmp)){?><span class="help-block error"><?php echo (($tmp = @$_smarty_tpl->tpl_vars['errors']->value['email'])===null||$tmp==='' ? '' : $tmp);?>
</span><?php }?>
                            </div>
                        </div>
                            
                        <div class="control-group <?php if ((($tmp = @$_smarty_tpl->tpl_vars['errors']->value['firstname'])===null||$tmp==='' ? '' : $tmp)){?>error<?php }?>">
                            <label class="control-label" for="firstname">First name</label>
                            <div class="controls">
                                <input type="text" name="firstname" value="<?php echo (($tmp = @$_POST['firstname'])===null||$tmp==='' ? '' : $tmp);?>
" class="input-xlarge" id="firstname" >
                                <?php if ((($tmp = @$_smarty_tpl->tpl_vars['errors']->value['firstname'])===null||$tmp==='' ? '' : $tmp)){?><span class="help-block error"><?php echo (($tmp = @$_smarty_tpl->tpl_vars['errors']->value['firstname'])===null||$tmp==='' ? '' : $tmp);?>
</span><?php }?>
                            </div>
                        </div>

                        <div class="control-group <?php if ((($tmp = @$_smarty_tpl->tpl_vars['errors']->value['lastname'])===null||$tmp==='' ? '' : $tmp)){?>error<?php }?>">
                            <label class="control-label" for="lastname">Last name</label>
                            <div class="controls">
                                <input type="text" name="lastname" value="<?php echo (($tmp = @$_POST['lastname'])===null||$tmp==='' ? '' : $tmp);?>
" class="input-xlarge" id="lastname" >
                                <?php if ((($tmp = @$_smarty_tpl->tpl_vars['errors']->value['lastname'])===null||$tmp==='' ? '' : $tmp)){?><span class="help-block error"><?php echo (($tmp = @$_smarty_tpl->tpl_vars['errors']->value['lastname'])===null||$tmp==='' ? '' : $tmp);?>
</span><?php }?>
                            </div>
                        </div>

                        <div class="control-group <?php if ((($tmp = @$_smarty_tpl->tpl_vars['errors']->value['phonenumber'])===null||$tmp==='' ? '' : $tmp)){?>error<?php }?>">
                            <label class="control-label" for="phonenumber">Phone number</label>
                            <div class="controls">
                                <input type="text" name="phonenumber" value="<?php echo (($tmp = @$_POST['phonenumber'])===null||$tmp==='' ? '' : $tmp);?>
" class="input-xlarge" id="lastname" >
                                <?php if ((($tmp = @$_smarty_tpl->tpl_vars['errors']->value['phonenumber'])===null||$tmp==='' ? '' : $tmp)){?><span class="help-block error"><?php echo (($tmp = @$_smarty_tpl->tpl_vars['errors']->value['phonenumber'])===null||$tmp==='' ? '' : $tmp);?>
</span><?php }?>
                            </div>
                        </div>

                        <div class="control-group <?php if ((($tmp = @$_smarty_tpl->tpl_vars['errors']->value['address'])===null||$tmp==='' ? '' : $tmp)){?>error<?php }?>">
                            <label class="control-label" for="address">Address</label>
                            <div class="controls">
                                <input type="text" name="address" value="<?php echo (($tmp = @$_POST['address'])===null||$tmp==='' ? '' : $tmp);?>
" class="input-xlarge" id="lastname" >
                                <?php if ((($tmp = @$_smarty_tpl->tpl_vars['errors']->value['address'])===null||$tmp==='' ? '' : $tmp)){?><span class="help-block error"><?php echo (($tmp = @$_smarty_tpl->tpl_vars['errors']->value['address'])===null||$tmp==='' ? '' : $tmp);?>
</span><?php }?>
                            </div>
                        </div>
                            
                        <div class="control-group <?php if ((($tmp = @$_smarty_tpl->tpl_vars['errors']->value['zip'])===null||$tmp==='' ? '' : $tmp)){?>error<?php }?>">
                            <label class="control-label" for="zip">Zip</label>
                            <div class="controls">
                                <input type="text" name="zip" value="<?php echo (($tmp = @$_POST['zip'])===null||$tmp==='' ? '' : $tmp);?>
" class="input-xlarge" id="lastname" >
                                <?php if ((($tmp = @$_smarty_tpl->tpl_vars['errors']->value['zip'])===null||$tmp==='' ? '' : $tmp)){?><span class="help-block error"><?php echo (($tmp = @$_smarty_tpl->tpl_vars['errors']->value['zip'])===null||$tmp==='' ? '' : $tmp);?>
</span><?php }?>
                            </div>
                        </div>
                            
                        <div class="control-group <?php if ((($tmp = @$_smarty_tpl->tpl_vars['errors']->value['city'])===null||$tmp==='' ? '' : $tmp)){?>error<?php }?>">
                            <label class="control-label" for="city">City</label>
                            <div class="controls">
                                <input type="text" name="city" value="<?php echo (($tmp = @$_POST['city'])===null||$tmp==='' ? '' : $tmp);?>
" class="input-xlarge" id="lastname" >
                                <?php if ((($tmp = @$_smarty_tpl->tpl_vars['errors']->value['city'])===null||$tmp==='' ? '' : $tmp)){?><span class="help-block error"><?php echo (($tmp = @$_smarty_tpl->tpl_vars['errors']->value['city'])===null||$tmp==='' ? '' : $tmp);?>
</span><?php }?>
                            </div>
                        </div>
                            
                        <div class="form-actions">
                            <button class="btn btn-primary" type="submit">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div><?php }} ?>