<?php
function smarty_block_lang($params, $content, &$smarty, &$repeat) {
    if($repeat)
    {
        return;
    }
    
    $lang = Arr::get($params, 'lang', 'pl-pl');
    
    return __($content, $params, $lang);
}