<?php

/**
 * @version 1.0
 * @author Piotr Bronisz <piotrekbronisz@gmail.com>
 * @return string
 */
function smarty_function_profilertoolbar()
{
    return ProfilerToolbar::render(false);
}