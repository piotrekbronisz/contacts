<?php
function smarty_modifier_format_config($type, $value, $id) {    
    if ($type == 'input') {
        return '<input id="config_'.$id.'" name="config['.$id.']" type="text" value="'.$value.'" class="input-xlarge" />';
	}
	elseif ($type == 'yesno') {
		$checked_yes = ($value == 'true' OR $value == '1') ? 'checked="checked"' : '';
		$checked_no = ($value == 'false' OR $value == '0') ? 'checked="checked"' : '';
        
        
        $return = '<div class="span8 controls">';
        $return .= "\t".'<div class="marginT5">';
        $return .= "\t\t".'<input type="radio" name="config['.$id.']" id="'.$id.'_1" value="1" '.$checked_yes.' />';
        $return .= "\t\t".'Tak';
        $return .= "\t".'</div>';
        $return .= "\t".'<div class="marginT5">';
        $return .= "\t\t".'<input type="radio" name="config['.$id.']" id="'.$id.'_0" value="0" '.$checked_no.' />';
        $return .= "\t\t".'Nie';
        $return .= "\t".'</div>';
        $return .= '</div>';

		return $return;
	}
	elseif ($type == 'textarea') {
	    return '<textarea id="config_'.$id.'" name="config['.$id.']" rows="3" class="input-xlarge">'.$value.'</textarea>';
	}
	elseif ($type == 'fck') {
		$return = '<textarea name="config['.$id.']" id="config_'.$id.'" class="mceEditor" style="width: 100%;">'.$value.'</textarea>';
        
		return $return;
	}
    elseif ($type == 'upload') {
        $return = '<input type="hidden" name="config['.$id.']" value="'.$value.'" id="file_upload_hidden_'.$id.'">';
        $return .= '<input type="file" name="file_upload_'.$id.'" id="file_upload_'.$id.'" data-url="'.URL::site("uploadify.php").'" />';
        if ($value) {
            $return .= '<div id="file_upload_'.$id.'-queue" class="uploadify-queue">';
            $return .= '    <div class="uploadify-queue-item" id="SWFUpload_0_0">';
            $return .= '        <div class="cancel">';
            $return .= '            <a href="javascript:void(0);" onclick="javascript:delete_setting_upload(\''.$id.'\');">X</a>';
            $return .= '        </div>';
            $return .= '    <span class="fileName"><a href="upload/'.$value.'">Podgląd pliku</a></span>';
            $return .= '    </div>';
            $return .= '</div>';
        }
        
        return $return;
    }
}