<?php
function smarty_modifier_poldate($string) {
	
	$lang['dateconversion'] = array(
		'Monday'    => 'Poniedziałek', 
		'Tuesday'   => 'Wtorek', 
		'Wednesday' => 'Środa', 
		'Thursday'  => 'Czwartek', 
		'Friday'    => 'Piątek', 
		'Saturday'  => 'Sobota', 
		'Sunday'    => 'Niedziela', 
		'Mon'       => 'Pon', 
		'Tue'       => 'Wto', 
		'Wed'       => 'Sro', 
		'Thu'       => 'Czw', 
		'Fri'       => 'Pt', 
		'Sat'       => 'Sob', 
		'Sun'       => 'Nie', 
		'January'   => 'Styczeń', 
		'February'  => 'Luty',
		'March'     => 'Marzec', 
		'April'     => 'Kwiecień', 
		'May'       => 'Maj', 
		'June'      => 'Czerwiec', 
		'July'      => 'Lipiec', 
		'August'    => 'Sierpień', 
		'September' => 'Wrzesień', 
		'October'   => 'Październik', 
		'November'  => 'Listopad', 
		'December'  => 'Grudzień', 
		'Jan'       => 'Sty', 
		'Feb'       => 'Lut', 
		'Mar'       => 'Mar', 
		'Apr'       => 'Kwi', 
		'May'       => 'Maj', 
		'Jun'       => 'Cze', 
		'Jul'       => 'Lip', 
		'Aug'       => 'Sie', 
		'Sep'       => 'Wrz', 
		'Oct'       => 'Paź', 
		'Nov'       => 'Lis',
		'Dec'       => 'Gru',
		//
		'Styczeń'     => 'stycznia',
		'Luty'        => 'lutego',
		'Marzec'      => 'marca',
		'Kwiecień'    => 'kwietnia',
		'Maj'         => 'maja',
		'Czerwiec'    => 'czerwca',
		'Lipiec'      => 'lipca',
		'Sierpień'    => 'sierpnia',
		'Wrzesień'    => 'września',
		'Październik' => 'października',
		'Listopad'    => 'listopada',
		'Grudzień'    => 'grudnia'
	);
	
	foreach($lang['dateconversion'] as $date => $translate) 
	{
		$string = str_replace($date, $translate, $string);
	}
	
	return $string;
}

?>