        <div class="clear"></div>
       	<div id="footer" style="padding-left: 20px; padding-right: 20px;">
           <div style="float: left;">
                Copyright &copy; {$smarty.now|date_format:"%Y"} - MicroJobs
           </div>
           <div style="float: right;">
               
           </div>
       	</div>
    </div>
    <script type="text/javascript">
        if (document.getElementById('tree')) {
            var tree = new DynamicTree("tree");
            tree.foldersAsLinks = true;
            tree.enableSetActive = true;
            tree.path = '/assets/img/dynamictree/';
            tree.init();
            {if $tree_active|default:""}
            tree.setActive('item_{$tree_active|default:""}');
            tree.openTo('item_{$tree_active|default:""}');
            {/if}
        }
        
        menu_do_global_init();
    </script>
    {profilertoolbar}
</body>
</html>