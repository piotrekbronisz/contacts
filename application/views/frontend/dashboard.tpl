<div class="container-fluid">
    <div class="page-header">
        <div class="pull-left">
            <h1>Kokpit</h1>
        </div>
        <div class="pull-right">
            <ul class="minitiles">
				<li class='lightgrey'>
                    <a href="{URL::site('/')}" target="_blank" rel='tooltip' title="Otwórz stronę" data-placement="bottom">
                        <i class="icon-globe"></i>
                    </a>
				</li>
            </ul>
            <ul class="stats">
                <li class='lightred'>
                    <i class="icon-calendar"></i>
                    <div class="details">
                        <span class="big">{$smarty.now|date_format:"%B"} {$smarty.now|date_format:"%d"}, {$smarty.now|date_format:"%Y"}</span>
                        <span>{$smarty.now|date_format:"%A"}, {$smarty.now|date_format:"%H"}:{$smarty.now|date_format:"%M"}</span>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="breadcrumbs">
        <ul>
            <li>
                <a href="{URL::site('admin')}">Home</a>
				<i class="icon-angle-right"></i>
            </li>
            <li>
                <a href="{URL::site('admin')}">Kokpit</a>
            </li>
        </ul>
        <div class="close-bread">
            <a href="#"><i class="icon-remove"></i></a>
        </div>
    </div>
    <div class="row-fluid">
        <br /><br />
        <div class="alert alert-info">
            <button data-dismiss="alert" class="close" type="button">×</button>
            Witaj w systemie zarządzania Twoja stroną internetową
        </div>
    </div>
</div>