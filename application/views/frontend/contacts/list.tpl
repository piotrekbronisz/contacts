{Hint::render(NULL, TRUE, 'hint/frontend/toast')}
<div class="container-fluid">
    <div class="page-header">
        <div class="pull-left">
            <h1>Contacts</h1>
        </div>
    </div>
    <div class="breadcrumbs">
        <ul>
            <li>
                <a href="{URL::site('contacts')}">Contacts</a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a href="{URL::site('contacts')}">List</a>
                <i class="icon-angle-right"></i>
            </li>
        </ul>
        <div class="close-bread">
            <a href="#"><i class="icon-remove"></i></a>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="box">
                <div class="box-content nopadding">
                    <div class="dataTables_wrapper">
                        <div class="dataTables_length">
                            <label>
                                <div class="btn-group" style="float: left;">
                                    <a href="#" data-toggle="dropdown" class="btn dropdown-toggle">{intval($smarty.get.limit|default:"10")} <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="{URL::site("contacts")}{$helper->query('limit', '10')}">10</a>
                                        </li>
                                        <li>
                                            <a href="{URL::site("contacts")}{$helper->query('limit', '25')}">25</a>
                                        </li>
                                        <li>
                                            <a href="{URL::site("contacts")}{$helper->query('limit', '50')}">50</a>
                                        </li>
                                        <li>
                                            <a href="{URL::site("contacts")}{$helper->query('limit', '100')}">100</a>
                                        </li>
                                    </ul>
                                </div>
                                <span>Elementów na strone</span>
                            </label>
                        </div>
                        <div style="float: right; margin: 10px 10px 5px 5px">
                            <a href="{URL::site("contacts/add")}" class="btn"><i class="icon-plus"></i> Dodaj</a>
                        </div>
                        <form action="{URL::site("contacts/checkbox")}" method="post">
                            <table class="table table-hover table-nomargin table-striped table-bordered" style="clear: both;">
                                <thead>
                                    <tr>
                                        <th style="text-align: center;"><input type="checkbox" value="check_none" onclick="javascript:check_all_box(this.form)" /></th>
                                        <th style="text-align: center;"><a href="{URL::site("contacts")}{$sort.firstname}">First name</a></th>
                                        <th style="text-align: center;"><a href="{URL::site("contacts")}{$sort.lastname}">Last name</a></th>
                                        <th style="text-align: center;"><a href="{URL::site("contacts")}{$sort.email}">E-mail</a></th>
                                        <th style="text-align: center;"><a href="{URL::site("contacts")}{$sort.phonenumber}">Phone number</a></th>
                                        <th style="text-align: center;"><a href="{URL::site("contacts")}{$sort.address}">address</a></th>
                                        <th style="text-align: center;"><a href="{URL::site("contacts")}{$sort.zip}">Zip</a></th>
                                        <th style="text-align: center;"><a href="{URL::site("contacts")}{$sort.city}">city</a></th>
                                        <th style="text-align: center;" colspan="2">Opcje</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th colspan="10">
                                            <img src="/assets/img/arrow_ltr.png" />&nbsp;
                                            <select name="action">
                                                <option value="0">-- wybierz --</option>
                                                <option value="delete">Usuń</option>
                                            </select>&nbsp;<button class="btn btn-primary" type="submit">Wykonaj</button>
                                        </th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    {foreach $items as $it}
                                        <tr>
                                            <td style="width: 32px; text-align: center;">
                                                <input type="checkbox" name="list[]" value="{$it->id}" />
                                            </td>
                                            <td>
                                                {$it->firstname}
                                            </td>
                                            <td>
                                                {$it->lastname}
                                            </td>
                                            <td style="text-align: center;">
                                                {$it->email}
                                            </td>
                                            <td style="text-align: center;">
                                                {$it->phonenumber}
                                            </td>
                                            <td style="text-align: center;">
                                                {$it->address}
                                            </td>
                                            <td style="text-align: center;">
                                                {$it->zip}
                                            </td>
                                            <td style="text-align: center;">
                                                {$it->city}
                                            </td>

                                            <td style="width: 32px; text-align: center;">
                                                <a href="{URL::site("contacts/edit/`$it->id`")}" rel="tooltip" title="Edytuj" class="btn">
                                                    <i class="icon-pencil"></i>
                                                </a>
                                            </td>
                                            <td style="width: 32px; text-align: center;">
                                                <a href="javascript:void(0);" onclick="javascript:confirm_action('{URL::site("contacts/delete/`$it->id`")}');" rel="tooltip" title="Usuń" class="btn btn-danger">
                                                    <i class="icon-minus-sign"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    {foreachelse}
                                        <tr>
                                            <td style="text-align: center;" colspan="7">
                                                Brak danych do wyświetlenia
                                            </td>
                                        </tr>
                                    {/foreach}
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
            {$pagination->render('pagination/backend/floating')}
        </div>
    </div>
</div>