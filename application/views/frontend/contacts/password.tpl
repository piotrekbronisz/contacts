{Hint::render(NULL, TRUE, 'hint/frontend/toast')}
<div class="container-fluid">
    <div class="page-header">
        <div class="pull-left">
            <h1>Moje konto</h1>
        </div>
    </div>
    <div class="breadcrumbs">
        <ul>
            <li>
                <a href="{URL::site('contacts')}">Contacts</a>
				<i class="icon-angle-right"></i>
            </li>
            <li>
                <a href="{URL::site('contacts/profile')}">Moje konto</a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a href="javascript:void(0);">
                    Zmień hasło
                </a>
            </li>
        </ul>
        <div class="close-bread">
            <a href="#"><i class="icon-remove"></i></a>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="box box-bordered box-color">
                <div class="box-title">
                    <h3><i class="icon-th-list"></i> Zmień hasło</h3>
				</div>
				<div class="box-content nopadding">
                    <form class="form-vertical form-bordered" method="POST" action="" enctype="multipart/form-data" autocomplete="off">
                        <div class="control-group {if $errors.password|default:""}error{/if}">
                            <label class="control-label" for="password">Hasło</label>
                            <div class="controls">
                                <input type="password" name="password" value="{$smarty.post.password|default:""}" class="input-xlarge" id="password" >
                                {if $errors.password|default:""}<span class="help-block error">{$errors.password|default:""}</span>{/if}
                            </div>
                        </div>
				        <div class="control-group {if $errors.password_confirm|default:""}error{/if}">
                            <label class="control-label" for="firstname">Powtórz hasło</label>
                            <div class="controls">
                                <input type="password" name="password_confirm" value="{$smarty.post.password_confirm|default:""}" class="input-xlarge" id="password_confirm" >
                                {if $errors.password_confirm|default:""}<span class="help-block error">{$errors.password_confirm|default:""}</span>{/if}
                            </div>
                        </div>
                        <div class="form-actions">
                            <button class="btn btn-primary" type="submit">Zapisz</button>
                            <button class="btn" type="button">Resetuj</button>
                        </div>
				    </form>
                </div>
            </div>
        </div>
    </div>
</div>