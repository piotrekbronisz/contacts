<div class="container-fluid">
    <div class="page-header">
        <div class="pull-left">
            <h1>Contacts</h1>
        </div>
    </div>
    <div class="breadcrumbs">
        <ul>
            <li>
                <a href="{URL::site('contacts')}">Contacts</a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a href="{URL::site('contacts')}">List</a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a href="javascript:void(0);">
                    Dodaj
                </a>
            </li>
        </ul>
        <div class="close-bread">
            <a href="#"><i class="icon-remove"></i></a>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="box box-bordered box-color">
                <div class="box-title">
                    <h3><i class="icon-th-list"></i> Dodaj</h3>
                </div>
                <div class="box-content nopadding">
                    <form class="form-vertical form-bordered" method="POST" action="" enctype="multipart/form-data">
                        <div class="control-group {if $errors.email|default:""}error{/if}">
                            <label class="control-label" for="email">E-mail</label>
                            <div class="controls">
                                <input type="text" name="email" value="{$smarty.post.email|default:""|htmlspecialchars}" class="input-xlarge" id="email" >
                                {if $errors.email|default:""}<span class="help-block error">{$errors.email|default:""}</span>{/if}
                            </div>
                        </div>
                            
                        <div class="control-group {if $errors.firstname|default:""}error{/if}">
                            <label class="control-label" for="firstname">First name</label>
                            <div class="controls">
                                <input type="text" name="firstname" value="{$smarty.post.firstname|default:""}" class="input-xlarge" id="firstname" >
                                {if $errors.firstname|default:""}<span class="help-block error">{$errors.firstname|default:""}</span>{/if}
                            </div>
                        </div>

                        <div class="control-group {if $errors.lastname|default:""}error{/if}">
                            <label class="control-label" for="lastname">Last name</label>
                            <div class="controls">
                                <input type="text" name="lastname" value="{$smarty.post.lastname|default:""}" class="input-xlarge" id="lastname" >
                                {if $errors.lastname|default:""}<span class="help-block error">{$errors.lastname|default:""}</span>{/if}
                            </div>
                        </div>

                        <div class="control-group {if $errors.phonenumber|default:""}error{/if}">
                            <label class="control-label" for="phonenumber">Phone number</label>
                            <div class="controls">
                                <input type="text" name="phonenumber" value="{$smarty.post.phonenumber|default:""}" class="input-xlarge" id="lastname" >
                                {if $errors.phonenumber|default:""}<span class="help-block error">{$errors.phonenumber|default:""}</span>{/if}
                            </div>
                        </div>

                        <div class="control-group {if $errors.address|default:""}error{/if}">
                            <label class="control-label" for="address">Address</label>
                            <div class="controls">
                                <input type="text" name="address" value="{$smarty.post.address|default:""}" class="input-xlarge" id="lastname" >
                                {if $errors.address|default:""}<span class="help-block error">{$errors.address|default:""}</span>{/if}
                            </div>
                        </div>
                            
                        <div class="control-group {if $errors.zip|default:""}error{/if}">
                            <label class="control-label" for="zip">Zip</label>
                            <div class="controls">
                                <input type="text" name="zip" value="{$smarty.post.zip|default:""}" class="input-xlarge" id="lastname" >
                                {if $errors.zip|default:""}<span class="help-block error">{$errors.zip|default:""}</span>{/if}
                            </div>
                        </div>
                            
                        <div class="control-group {if $errors.city|default:""}error{/if}">
                            <label class="control-label" for="city">City</label>
                            <div class="controls">
                                <input type="text" name="city" value="{$smarty.post.city|default:""}" class="input-xlarge" id="lastname" >
                                {if $errors.city|default:""}<span class="help-block error">{$errors.city|default:""}</span>{/if}
                            </div>
                        </div>
                            
                        <div class="form-actions">
                            <button class="btn btn-primary" type="submit">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>