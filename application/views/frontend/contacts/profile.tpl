{Hint::render(NULL, TRUE, 'hint/frontend/toast')}
<div class="container-fluid">
    <div class="page-header">
        <div class="pull-left">
            <h1>Moje konto</h1>
        </div>
    </div>
    <div class="breadcrumbs">
        <ul>
            <li>
                <a href="{URL::site('contacts')}">Contacts</a>
				<i class="icon-angle-right"></i>
            </li>
            <li>
                <a href="{URL::site('admin/admins/profile')}">Moje konto</a>
                <i class="icon-angle-right"></i>
            </li>
            <li>
                <a href="javascript:void(0);">
                    Edytuj
                </a>
            </li>
        </ul>
        <div class="close-bread">
            <a href="#"><i class="icon-remove"></i></a>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="box box-bordered box-color">
                <div class="box-title">
                    <h3><i class="icon-th-list"></i> Edytuj</h3>
				</div>
				<div class="box-content nopadding">
                    <form class="form-vertical form-bordered" method="POST" action="" enctype="multipart/form-data">
                        <input type="hidden" name="id" value="{$item->id}" />
                        <div class="control-group {if $errors.email|default:""}error{/if}">
                            <label class="control-label" for="email">E-mail</label>
                            <div class="controls">
                                <input type="text" name="email" value="{$smarty.post.email|default:$item->email}" class="input-xlarge" id="email" >
                                {if $errors.email|default:""}<span class="help-block error">{$errors.email|default:""}</span>{/if}
                            </div>
                        </div>
				        <div class="control-group {if $errors.firstname|default:""}error{/if}">
                            <label class="control-label" for="firstname">Imię</label>
                            <div class="controls">
                                <input type="text" name="firstname" value="{$smarty.post.firstname|default:$item->firstname}" class="input-xlarge" id="email" >
                                {if $errors.firstname|default:""}<span class="help-block error">{$errors.firstname|default:""}</span>{/if}
                            </div>
                        </div>
                        <div class="control-group {if $errors.lastname|default:""}error{/if}">
                            <label class="control-label" for="lastname">Nazwisko</label>
                            <div class="controls">
                                <input type="text" name="lastname" value="{$smarty.post.lastname|default:$item->lastname}" class="input-xlarge" id="email" >
                                {if $errors.lastname|default:""}<span class="help-block error">{$errors.lastname|default:""}</span>{/if}
                            </div>
                        </div>
                        <div class="form-actions">
                            <button class="btn btn-primary" type="submit">Zapisz</button>
                            <button class="btn" type="button">Resetuj</button>
                        </div>
				    </form>
                </div>
            </div>
        </div>
    </div>
</div>