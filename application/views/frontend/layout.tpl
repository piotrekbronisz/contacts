{include file="frontend/header.tpl"}
<body class="theme-grey">
    <div id="navigation">
        <div class="container-fluid">
            <a href="#" id="brand">Contacts</a>
            <a href="#" class="toggle-nav" rel="tooltip" data-placement="bottom" title="Toggle navigation">
                <i class="icon-reorder"></i>
            </a>

            <a href="#" class='toggle-mobile'><i class="icon-reorder"></i></a>
        </div>
    </div>

    <div class="container-fluid" id="content">
        <div id="left">

            <div class="subnav">
                <div class="subnav-title">
                    <a href="#" class='toggle-subnav'>
                        <i class="icon-angle-down"></i>
                        <span>Contacts</span>
                    </a>
                </div>
                <ul class="subnav-menu">
                    <li>
                        <a href="{URL::site('contacts')}">List</a>
                    </li>

                </ul>
            </div>
        </div>
        <div id="main">
            {$content}
        </div>
    </div>

</body>
</html>