<script type="text/javascript">
    jQuery(document).ready(function() {
        <?php foreach ($messages as $message): ?>
        jQuery().toastmessage('showToast', {
            text     : '<?php echo $message['text'] ?>',
            position : 'top-right',
            sticky   : false,
            stayTime: 2000,
            type     : '<?php echo $message['type'] ?>'
        });
        <?php endforeach ?>
    });
</script>