SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


CREATE TABLE IF NOT EXISTS `languages` (
  `id` int(11) NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `created_on` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

INSERT INTO `languages` (`id`, `name`, `updated_on`, `created_on`) VALUES
(1, 'Polski', '2014-11-13 22:49:12', '2014-11-13 22:49:12'),
(2, 'Angielski', '2014-11-13 22:49:20', '2014-11-13 22:49:20'),
(3, 'Francuski', '2014-11-13 22:49:31', '2014-11-13 22:49:31'),
(5, 'Hiszpański', '2014-11-14 09:24:05', '2014-11-13 22:50:27');

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) NOT NULL,
  `posy` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `roles` (`id`, `name`, `description`, `posy`) VALUES
(1, 'login', 'Login privileges, granted after account confirmation', 1),
(2, 'admin', 'Administrator', 3),
(3, 'user', 'Klient', 2);

CREATE TABLE IF NOT EXISTS `roles_users` (
  `user_id` int(11) unsigned NOT NULL,
  `role_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `roles_users` (`user_id`, `role_id`) VALUES
(1, 1),
(24, 1),
(83, 1),
(84, 1),
(96, 1),
(97, 1),
(98, 1),
(99, 1),
(100, 1),
(101, 1),
(102, 1),
(103, 1),
(104, 1),
(105, 1),
(106, 1),
(107, 1),
(108, 1),
(109, 1),
(110, 1),
(1, 2),
(84, 2),
(109, 2),
(1, 3),
(24, 3),
(83, 3),
(114, 1),
(114, 2),
(115, 1),
(115, 2),
(116, 1),
(116, 2);

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL DEFAULT '0',
  `subsection_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `key` varchar(250) NOT NULL,
  `value` text NOT NULL,
  `description` text NOT NULL,
  `type` varchar(250) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `settings` (`id`, `section_id`, `subsection_id`, `name`, `key`, `value`, `description`, `type`) VALUES
(96, 1, 1, 'Główny adres e-mail', 'WEBMASTER_EMAIL', 'piotrekbronisz@gmail.com', 'Jest to adres e-mail z którego wysyłane są wszystkie wiadomości e-mail wysyłane z systemu. Na ten adres będą także dostarczane e-maile z formularza kontaktowego', 'input'),
(113, 1, 2, 'Tytuł strony', 'META_TITLE', 'Contact', '', 'input'),
(114, 1, 2, 'Opis strony', 'META_DESC', 'Contact', '', 'textarea'),
(115, 1, 2, 'Słowa kluczowe', 'META_KEYS', '', '', 'textarea'),
(170, 1, 3, 'Facebook', 'SOCIAL_FB', 'https://www.facebook.com/', '', 'input'),
(171, 1, 3, 'Twitter', 'SOCIAL_TW', 'https://twitter.com', '', 'input'),
(172, 1, 3, 'G+', 'SOCIAL_GPLUS', '', '', 'input'),
(173, 1, 3, 'LinkedIn', 'SOCIAL_IN', 'https://pl.linkedin.com', '', 'input'),
(174, 1, 1, 'Kod systemu statystyk', 'STAT_TRACK_CODE', '', '', 'textarea');

CREATE TABLE IF NOT EXISTS `trackers` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_ip` varchar(250) NOT NULL,
  `created_on` datetime NOT NULL,
  `post` longtext NOT NULL,
  `get` longtext NOT NULL,
  `message` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `trackers` (`id`, `user_id`, `user_ip`, `created_on`, `post`, `get`, `message`) VALUES
(0, 0, '127.0.0.1', '2015-09-10 15:54:18', '<small>array</small><span>(5)</span> <span>(\n    "email" => <small>string</small><span>(10)</span> "dsa@dsa.pl"\n    "firstname" => <small>string</small><span>(3)</span> "dsa"\n    "lastname" => <small>string</small><span>(3)</span> "dsa"\n    "password" => <small>string</small><span>(3)</span> "123"\n    "password_confirm" => <small>string</small><span>(3)</span> "123"\n)</span>', '', 'Dodano nowego administratora ID: 115'),
(0, 0, '127.0.0.1', '2015-09-10 15:54:23', '', '', 'Usunięto administratora ID: 114'),
(0, 0, '127.0.0.1', '2015-09-10 15:55:01', '', '', 'Usunięto administratora ID: 114'),
(0, 0, '127.0.0.1', '2015-09-10 15:55:07', '', '', 'Usunięto administratora ID: 115'),
(0, 0, '127.0.0.1', '2015-09-10 15:55:31', '<small>array</small><span>(5)</span> <span>(\n    "email" => <small>string</small><span>(10)</span> "asd@asd.pl"\n    "firstname" => <small>string</small><span>(3)</span> "asd"\n    "lastname" => <small>string</small><span>(3)</span> "asd"\n    "password" => <small>string</small><span>(3)</span> "123"\n    "password_confirm" => <small>string</small><span>(3)</span> "123"\n)</span>', '', 'Dodano nowego administratora ID: 116'),
(0, 0, '127.0.0.1', '2015-09-10 15:55:41', '<small>array</small><span>(6)</span> <span>(\n    "id" => <small>string</small><span>(3)</span> "116"\n    "email" => <small>string</small><span>(10)</span> "asd@asd.pl"\n    "firstname" => <small>string</small><span>(4)</span> "asd1"\n    "lastname" => <small>string</small><span>(4)</span> "asd1"\n    "password" => <small>string</small><span>(0)</span> ""\n    "password_confirm" => <small>string</small><span>(0)</span> ""\n)</span>', '', 'Edycja administratora ID: 116'),
(0, 0, '127.0.0.1', '2015-09-10 16:41:39', '<small>array</small><span>(8)</span> <span>(\n    "id" => <small>string</small><span>(3)</span> "116"\n    "email" => <small>string</small><span>(10)</span> "asd@asd.pl"\n    "firstname" => <small>string</small><span>(4)</span> "asd1"\n    "lastname" => <small>string</small><span>(4)</span> "asd1"\n    "phonenumber" => <small>string</small><span>(6)</span> "345345"\n    "address" => <small>string</small><span>(14)</span> "sdfg sdfgs dfg"\n    "zip" => <small>string</small><span>(8)</span> "34534534"\n    "city" => <small>string</small><span>(14)</span> "sd fgsdfg sdfg"\n)</span>', '', 'Edit Contact ID: 116');

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL,
  `province_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `facebook_id` varchar(250) DEFAULT NULL,
  `twitter_id` varchar(250) DEFAULT NULL,
  `yahoo_id` varchar(250) DEFAULT NULL,
  `google_id` varchar(250) DEFAULT NULL,
  `nk_id` varchar(250) DEFAULT NULL,
  `email` varchar(250) NOT NULL,
  `username` varchar(32) DEFAULT NULL,
  `password` char(50) DEFAULT NULL,
  `firstname` varchar(250) DEFAULT NULL,
  `lastname` varchar(250) DEFAULT NULL,
  `city` varchar(250) DEFAULT NULL,
  `icon` varchar(250) DEFAULT NULL,
  `code` varchar(250) DEFAULT NULL,
  `token` varchar(250) DEFAULT NULL,
  `logins` int(10) unsigned DEFAULT '0',
  `last_login` int(10) unsigned DEFAULT NULL,
  `birth_on` date DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `active` int(11) NOT NULL,
  `phonenumber` varchar(32) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `is_friend` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id`, `province_id`, `country_id`, `facebook_id`, `twitter_id`, `yahoo_id`, `google_id`, `nk_id`, `email`, `username`, `password`, `firstname`, `lastname`, `city`, `icon`, `code`, `token`, `logins`, `last_login`, `birth_on`, `created_on`, `active`, `phonenumber`, `address`, `zip`, `is_friend`) VALUES
(116, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'asd@asd.pl', 'asd@asd.pl', 'd064aa4ae304737d93971815f84ab654ae3de02b', 'asd1', 'asd1', 'sd fgsdfg sdfg', '', '', '', 0, NULL, NULL, '2015-09-10 15:55:31', 1, '345345', 'sdfg sdfgs dfg', '34534534', 0);

CREATE TABLE IF NOT EXISTS `user_tokens` (
  `id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `user_agent` varchar(40) NOT NULL,
  `token` varchar(32) NOT NULL,
  `type` varchar(250) DEFAULT NULL,
  `created` int(10) unsigned NOT NULL,
  `expires` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `users`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `uniq_email` (`email`), ADD UNIQUE KEY `uniq_username` (`username`);


ALTER TABLE `languages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
ALTER TABLE `users`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=117;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
