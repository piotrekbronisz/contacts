$.datepicker.regional['pl'] = {
	closeText: 'Zamknij',
    prevText: '&#x3c;Poprzedni',
    nextText: 'Następny&#x3e;',
    currentText: 'Dziś',
    monthNames: ['Styczeń','Luty','Marzec','Kwiecień','Maj','Czerwiec','Lipiec','Sierpień','Wrzesień','Październik','Listopad','Grudzień'],
    monthNamesShort: ['Sty','Lut','Mar','Kwi','Maj','Cze','Lip','Sie','Wrz','Paź','Lis','Gru'],
    dayNames: ['Niedziela','Poniedziałek','Wtorek','Środa','Czwartek','Piątek','Sobota'],
    dayNamesShort: ['Nd','Pn','Wt','Śr','Czw','Pt','So'],
    dayNamesMin: ['Nd','Pn','Wt','Śr','Cz','Pt','So'],
    weekHeader: 'Tydz',
    dateFormat: 'yy-mm-dd',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''
};
$.datepicker.setDefaults($.datepicker.regional['pl']);

(function( $ ){
		$.fn.retina = function(retina_part) {
		// Set default retina file part to '-2x'
		// Eg. some_image.jpg will become some_image-2x.jpg
		var settings = {'retina_part': '-2x'};
		if(retina_part) jQuery.extend(settings, { 'retina_part': retina_part });
		if(window.devicePixelRatio >= 2) {
			this.each(function(index, element) {
				if(!$(element).attr('src')) return;

				var checkForRetina = new RegExp("(.+)("+settings['retina_part']+"\\.\\w{3,4})");
				if(checkForRetina.test($(element).attr('src'))) return;

				var new_image_src = $(element).attr('src').replace(/(.+)(\.\w{3,4})$/, "$1"+ settings['retina_part'] +"$2");
				$.ajax({url: new_image_src, type: "HEAD", success: function() {
					$(element).attr('src', new_image_src);
				}});
			});
		}
		return this;
	}
})( jQuery );

(function($){
    $.fn.extend({ 
        elFinderFtp: function(options) {
            var defaults = {}
                 
            var options =  $.extend(defaults, options);
            
            return this.each(function() {
                var o = options;
                var obj = $(this);
                var holder = obj.data('file-holder');
                
                obj.find('.upload').click(function(e) {
                    $.colorbox({
                        iframe:true,
                        width:'800',
                        height:'600',
                        href:'/admin/elfinder/index?holder='+holder
                    });
                    
                    return false;
                });
                    
                obj.find('.delete').click(function(e) {
                    jConfirm('Czy na pewno chcesz usunąć plik?', 'UWAGA', function(r) {
                        if (r == true) {
                            obj.find('#'+holder).val('');
                            obj.find('.file-info').attr('src', '');
                            obj.find('.ftp-browser-preview').hide();
                        }
                    });
                    
                    return false;
                });
            });
        }
    });
})(jQuery);

function confirm_action(redirect_uri) {
    bootbox.dialog('Czy na pewno chesz wykonać wybraną akcje?', [
        {
            "label" : "OK",
            "class" : "btn-primary",
            "callback": function() {
                window.location.href = redirect_uri;
            }
        }, {
            "label" : "Anuluj",
            "class": "btn"
        }
    ], {
        "header": "Uwaga",
        onEscape: function() {}
    });
}

function confirm_action_custom(message, redirect_uri) {
    bootbox.dialog(message, [
        {
            "label" : "OK",
            "class" : "btn-primary",
            "callback": function() {
                window.location.href = redirect_uri;
            }
        }, {
            "label" : "Anuluj",
            "class": "btn"
        }
    ], {
        "header": "Uwaga",
        onEscape: function() {}
    });
}

function display_alert(message, title) {
    bootbox.dialog(message, [
        {
            "label" : "Zamknij",
            "class" : "btn"
        }
    ], {
        "header": title,
        onEscape: function() {}
    });
}

function delete_setting_upload(id) {
    $('#file_upload_hidden_'+id).val('');
    $('#iconQueue_'+id).remove();
}

function f(x) {
	if (document.getElementById(x).style.display == 'none') {
		document.getElementById(x).style.display = "block";
	}
	else {
		document.getElementById(x).style.display = "none";
	}
}

function finline(x) {
	if (document.getElementById(x).style.display == 'none') { document.getElementById(x).style.display = ""; } else { document.getElementById(x).style.display = "none"; }
}

function z1(x,w) {
    if (document.getElementById(x)) { document.getElementById(x).style.display='block'; }
    if (document.getElementById(w)) { document.getElementById(w).style.display='none'; }
}

function z2(x,w,e) {
    if (document.getElementById(x)) { document.getElementById(x).style.display='block'; }
    if (document.getElementById(w)) { document.getElementById(w).style.display='none'; }
    if (document.getElementById(e)) { document.getElementById(e).style.display='none'; }
}

function z3(x,w,e,b) {
    if (document.getElementById(x)) { document.getElementById(x).style.display='block'; }
    if (document.getElementById(w)) { document.getElementById(w).style.display='none'; }
    if (document.getElementById(e)) { document.getElementById(e).style.display='none'; }
    if (document.getElementById(b)) { document.getElementById(b).style.display='none'; }
}

$(document).ready(function() {
    jQuery(document).controls();

	$('.colorbox').colorbox({
		maxWidth: '100%',
		maxHeight: '100%'
	});

    jQuery(".tooltips").tipsy();
    jQuery("[rel=tooltip]").tipsy();
	
    $('.resource_gallery').colorbox({
    	iframe:true,
    	width:'800',
    	height:'600',
        href:'/admin/elfinder/gallery'
    });
    
    if (isdefined('resource_type')) {
        $('.resource_attachments').colorbox({
        	iframe:true,
        	width:'800',
        	height:'600',
            href:'/admin/elfinder/attachments?resource_type='+resource_type+'&resource_id='+resource_id
        });
    }
    
    $('.preview_icon').colorbox({
    	maxWidth:'800',
    	maxHeight:'600'
    });
    
    jQuery('.ftp-browser').elFinderFtp();

    jQuery(document).on('click', 'a.ajaxStatus', function(e) {
        var href = $(this);
        var img = jQuery(this).find('img');

        jQuery.ajax({
			url: href.attr('href'),
			cache: false,
			dataType: 'json',
			success: function(data) {
				if (data.status == 'success') {
					img.attr('src', '/assets/backend/img/'+data.img);
					href.attr('href', data.uri);
                    
                    jQuery().toastmessage('showToast', {
                        text     : 'Zapisano poprawnie',
                        position : 'middle-center',
                        sticky   : true,
                        type     : 'success'
                    });
				}
				else {
					jQuery().toastmessage('showToast', {
                        text     : 'Błąd',
                        position : 'middle-center',
                        sticky   : true,
                        type     : 'error'
                    });
				}
			}
		});
		
        e.preventDefault();
    });
    
    $(".retina-ready").retina("@2x");
});

/* Check all table rows */
var checkflag = "false";
function check(field) {
    if (checkflag == "false") {
        for (i = 0; i < field.length; i++) {
            field[i].checked = true;
        }
        checkflag = "true";
        return "check_all";
    }
    else {
        for (i = 0; i < field.length; i++) {
            field[i].checked = false;
        }
        checkflag = "false";
        return "check_none"; 
    }
}

function check_all_box(form) {
	var checked=0;
	if (form) {
	   for (var i=0;i<form.length;i++) {
	       if (form[i].value != 'check_none') {
	           if (form[i].type=='checkbox') {
	               form[i].checked=!form[i].checked;
                }
            }
		}
	}
}