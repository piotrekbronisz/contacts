jQuery.fn.exists = function(){return this.length>0;}

function RemoveExtension(name){
	var varFile = name.split('.');
	var userFriendlyName = '';
	for(i in varFile) {
		if(i == (varFile.length - 1)){
			break;
		}
		userFriendlyName += varFile[i];
	}
	return userFriendlyName;
}

var OriginalTextValue = '';
var ActualUrl = '';
var checked_ids = [];
var AdminImageManager = {
	GetImageRow: function() {
		return '	<span style="margin-bottom:20px;" class="ManageImageBox" id="file_%%image_id%%" > <input class="TemplateHeading inPlaceImageBoxDefault" id="%%image_id%%_name" value="%%image_name%%" />	<input type="hidden" id="%%image_id%%_realname" value="%%image_realname%%" />		<br /> <div style="width: 200px; height: 50px; margin-top: 5px;">			<img src=\'%%image_url%%\' style=" border: solid 1px #CACACA;"  id="%%image_id%%_image" width="32" height="32" /><br />		</div> <br />		<input type="button"  class="Field150" id="%%image_id%%_delete" value="Usuń plik" />	</span>';
	},
	CheckDelete: function() {
		if (!$('#imagesList .ManageImageBox', window.parent.document).exists()) {
			$('#hasImages', window.parent.document).hide();
			$('#hasNoImages', window.parent.document).show();
			$('#deleteButton', window.parent.document).hide();
		} else {
			$('#hasImages', window.parent.document).show();
			$('#hasNoImages', window.parent.document).hide();
			$('#deleteButton', window.parent.document).show();
		}
	},
	CheckAllCheckBoxes: function(checkBox) {
		if($('#toggleAllChecks', window.parent.document).attr('checked')){
			$('#imagesList input:checkbox', window.parent.document).attr('checked', 'checked');
		}else{
			$('#imagesList input:checkbox', window.parent.document).removeAttr('checked');
		}
	},
	AddImage: function(name, url, id) {
		$('#hasImages', window.parent.document).show();
		$('#hasNoImages', window.parent.document).hide();
		$('#deleteButton', window.parent.document).show();
		var html = AdminImageManager.GetImageRow();
		var varFile = name.split('.');
		var extension = varFile[ varFile.length -1 ];
		var userFriendlyName = '';
		for(i in varFile) {
			if(i == (varFile.length - 1)){
				break;
			}
			userFriendlyName += varFile[i];
		}

		html = html.replace(/%%image_name%%/g, name);
		html = html.replace(/%%image_realname%%/g, name);
		html = html.replace(/%%image_id%%/g, id);
		html = html.replace(/%%image_url%%/g, url);
        
        checked_ids.push(id);
        
        $('#attachments_id', window.parent.document).val(checked_ids.join(","));
        
		$(html).appendTo('#imagesList', window.parent.document);

		$('#'+id+'_delete', window.parent.document).bind('click', function () {
			var idBits = jQuery(this).attr('id').split('_');
			var id = idBits[0];
			var animate = true;
            console.log(id);
			if(confirm('Czy na pewno chcesz skasować wybrany plik?')) {
				var sendPOST = 'deleteimages[]='+id;
				$.post('/admin/filemanager/delete', sendPOST,
				function(result){
					if(result.success){
						for(i in result.successimages) {
							var str = result.successimages[i];
							if (str.length > 1) {
								$('input:text[value=' + result.successimages[i] + ']').parent().hide('slow');
								$('input:text[value=' + result.successimages[i] + ']').parent().remove();
							}
						}
						AdminImageManager.ImageManagerManage(ActualUrl, '', '');
						AdminImageManager.CheckDelete();
					}
				}, "json");
			}
		}
		);

		$('#'+id+'_name', window.parent.document).bind('mouseover', function () {
            if(!$(this).hasClass("inPlaceFieldFocus")) {
				$(this).addClass("inPlaceImageBoxFieldHover");
			}
		});

		$('#'+id+'_name', window.parent.document).bind('mouseout', function () {
		      $(this).removeClass("inPlaceImageBoxFieldHover");
        });

		$('#'+id+'_name', window.parent.document).bind('focus',
		function () {
			$('.inPlaceFieldFocus', window.parent.document).each(function(){
				cancelEditName($(this));
				$(this).removeClass('inPlaceFieldFocus');
			});
			$(this).removeClass("inPlaceImageBoxFieldHover");
			$(this).addClass("inPlaceFieldFocus");
			OriginalTextValue = this.value;
			this.select();
			$('<div style="background-color: #F9F9F9; width: 205px; position: relative; margin-top: 5px; padding-bottom: 3px; text-align: left;" id="EditNameButtons"><input type="button" class="Field" name="saveEdit" value="Zapisz"  style="float: right;" onclick="saveEditName($(\'#' + this.id + '\'));" /><input type="button" class="Field" name="cancelEdit" value="Anuluj" style="float: left;"  onclick="cancelEditName($(\'#' + this.id + '\'));" /> </div>').insertAfter(this);
		}
		);


		if ($.browser.mozilla) {
			var event = "keypress";
		} else {
			var event = "keydown";
		}

		$('#'+id+'_name', window.parent.document).bind(event, function(e) {
			if (e.keyCode == 13) {
				$('#'+id+'_name', window.parent.document).blur();
			}
		});
        
        $("#imagesList", window.parent.document).sortable({
            items: 'span',
            update: function(event, ui) {
                $.ajax({
                    type: 'post',
                    url: base_uri('/admin/filemanager/sort'),
                    data: $("#imagesList").sortable("serialize"),
                });
            }
        });
	},
	ImageManagerManage: function(params, div, mesg) {
	    ActualUrl = params;
		$.ajax({
			type: 'post',
			url: params,
			success: function(resp) {
			     $("#ImageManager", window.parent.document).html(resp);
			}
		});
	}
};

function saveEditName(field) {
	$(field, window.parent.document).attr('disabled', true);

	var idBits = field.attr('id');
	idBits = idBits.split('_');
	var id = idBits[0];
	$('#EditNameButtons', window.parent.document).remove();

	field.removeClass("inPlaceFieldFocus");
	if(field.val() != OriginalTextValue){
		$.post('/admin/filemanager/rename', 'image_id='+id+'&fromName=' + encodeURIComponent($('#' + id + '_realname').val()) + '&toName=' + encodeURIComponent(field.val()),
		function(result){
			if(result.success) {
				$('#' + id + '_realname', window.parent.document).val(result.newrealname);
				$('#' + id + '_name', window.parent.document).val(result.newname);
                				
                alert('Opis pliku został zmieniony pomyślnie');
			}
            else{
                alert('Wystąpił problem podczas zmiany opisu pliku. Spróbuj ponownie poźniej');
                
				$('#'+id+'_name', window.parent.document).val(OriginalTextValue);
			}
		}, "json");
	}
    
	$(field, window.parent.documents).attr('disabled', false);
}

function cancelEditName(field) {
	$(field, window.parent.document).val(OriginalTextValue);
	$(field, window.parent.document).removeClass("inPlaceFieldFocus");
	$('#EditNameButtons', window.parent.document).remove();
}