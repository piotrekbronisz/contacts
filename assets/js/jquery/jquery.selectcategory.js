var CategorySelect = {

    target: null,
    mainCategories: {},
    categoriesCache: {},
    categoriesNames: {},
    categoriesParents: {},
    categoriesTree: [],
    selectedCategory: 0,
    config: {},

    init: function(target, mainCategories, config) {
        this.target = target;
        this.mainCategories = mainCategories;
        this.config = config;

        this.loadMainCategories();

        this.target
            .delegate('select[name="selectFirst"], select[name="selectSecond"], select[name="selectThird"]', 'change', $.proxy(this.events.changeSelect, this))
            .delegate('input[name="category"]', 'change', $.proxy(this.events.changeCategory, this));
    },

    loadMainCategories: function() {
        var categories = [];

        $.each(CategorySelect.mainCategories, function(key, data) {
            categories.push({
                id: data.id,
                name: data.name,
                isLeaf: data.is_leaf,
                parent: 0,
                prevParent: 0
            });
        });

        CategorySelect.categoriesCache[0] = categories;

        CategorySelect.changeSelectOptions(categories, $(this.target).find('select[name="selectFirst"]'));

        //CategorySelect.fixWidth();
    },

    changeSelectOptions: function(categories, selectTarget) {
        var output = [];

        $.each(categories, function(key, data) {
            if (CategorySelect.config.disabled === undefined || (CategorySelect.config.disabled !== undefined && CategorySelect.config.disabled.indexOf(data.id) < 0)) {
                CategorySelect.categoriesNames[data.id] = data.name;
                output.push('<option value="' + data.id + '" data-is-leaf="' + data.isLeaf + '" data-parent="' + data.parent + '" data-prev-parent="' + data.prevParent + '">' + data.name + '</option>');
            }
        });

        $(selectTarget).html(output.join(''));
    },

    buildCategoryTree: function(category, categoryName) {
        CategorySelect.categoriesTree.push({
            id: parseInt(category),
            name: categoryName
        });

        var parentCategory = CategorySelect.categoriesParents[category];

        if (CategorySelect.categoriesNames[parentCategory] !== undefined) {
            CategorySelect.buildCategoryTree(parentCategory, CategorySelect.categoriesNames[parentCategory]);
        }
    },

    setSelectedCategory: function(selectedCategory, categoryName) {
        CategorySelect.categoriesTree = [];

        if (selectedCategory > 0) {
            CategorySelect.selectedCategory = selectedCategory;
            CategorySelect.buildCategoryTree(selectedCategory, categoryName);
            $(CategorySelect.target).find('input[name=categoryTree]').val($.stringify(CategorySelect.categoriesTree.reverse())).change();
            $(CategorySelect.target).find('input[name=category]').val(selectedCategory).trigger('change');
        } else {
            CategorySelect.selectedCategory = 0;
            $(CategorySelect.target).find('input[name=category]').val('');
        }
    },

    loadCategories: function(target, selectedCategory, isLeaf) {
        var selectTarget = $(target).parent('select'),
            nextSelectsTarget = $(target).parentsUntil('div#categoryHandSelect').nextAll().find('select'),
            prevSelectsTarget = $(target).parentsUntil('div#categoryHandSelect').prevAll().find('select'),
            countOfSelectsTarget = ($(target).parentsUntil('div#categoryHandSelect').siblings().find('select').length + 1),
            categories = CategorySelect.categoriesCache[selectedCategory],
            parentCategory = $(target).data('parent'),
            prevParentCategory = $(target).data('prevParent');

        if (isLeaf) {
            if (nextSelectsTarget.length > 0) {
                $(nextSelectsTarget).each(function() {
                    $(this).html('');
                });
            }
            CategorySelect.setSelectedCategory(selectedCategory, $(target).text());
        } else {
            CategorySelect.setSelectedCategory('');

            if (prevSelectsTarget.length == 0 && parentCategory > 0) {
                CategorySelect.changeSelectOptions(CategorySelect.categoriesCache[prevParentCategory], selectTarget);
                CategorySelect.changeSelectOptions(CategorySelect.categoriesCache[parentCategory], $(nextSelectsTarget).first());
                CategorySelect.changeSelectOptions(CategorySelect.categoriesCache[selectedCategory], $(nextSelectsTarget).last());

                $(selectTarget).scrollTop(0);
                $(nextSelectsTarget).last().scrollTop(0);

                $(nextSelectsTarget).first().focus();
                $(nextSelectsTarget).first().val(selectedCategory).prop("selected", true);

            } else if (prevSelectsTarget.length == (countOfSelectsTarget - 1)) {
                for (var i = prevSelectsTarget.length - 1; i >= 0; i--) {
                    if (prevSelectsTarget[i - 1] !== undefined) {
                        $(prevSelectsTarget[i]).html($(prevSelectsTarget[i - 1]).html());
                    } else {
                        $(prevSelectsTarget[i]).html($(selectTarget).html());
                    }
                }

                $(prevSelectsTarget).last().scrollTop(0);
                $(prevSelectsTarget).first().focus();
                $(prevSelectsTarget).first().val(selectedCategory).prop("selected", true);

                CategorySelect.changeSelectOptions(categories, selectTarget);
            } else if (nextSelectsTarget.length > 0) {
                CategorySelect.changeSelectOptions(categories, $(nextSelectsTarget).first());
                if (nextSelectsTarget.length > 1) {
                    $(nextSelectsTarget).last().html('');
                }
            }
        }

        //CategorySelect.fixWidth();
    },

    changeCategory: function(params) {
        CategorySelect.categoriesTree = [];

        var categoryValue = params.category;

        var selects = $('#categoryHandSelect').find('select'),
            selectedCategoryName = '',
            selectedCategoryIsLeaf = false,
            categoryChanged = false,
            selectKey = 0;

        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: '/Utils/Category.php/getParentsAndSiblings/category,' + categoryValue,
            success: function(jsonData) {
                $.each(jsonData, function(catKey, childs) {

                    var categories = [],
                        lastParent = 0;

                    $.each(childs, function(key, data) {
                        var isLeaf = true;

                        if (data.childsCount > 0) {
                            isLeaf = false;
                        }

                        if (data.id == categoryValue) {
                            selectedCategoryName = data.name;
                            selectedCategoryIsLeaf = isLeaf;
                        }

                        CategorySelect.categoriesNames[data.id] = data.name;
                        CategorySelect.categoriesParents[data.id] = data.parent;
                        categories.push({
                            id: data.id,
                            name: data.name,
                            isLeaf: isLeaf,
                            parent: data.parent,
                            prevParent: data.prevParent
                        });

                        lastParent = data.parent;
                    });

                    CategorySelect.categoriesCache[lastParent] = categories;

                    if (jsonData.length > 3 && catKey >= (jsonData.length - 3)) {
                        CategorySelect.changeSelectOptions(categories, selects[selectKey]);
                        selectKey++;
                    } else if (jsonData.length <= 3) {
                        CategorySelect.changeSelectOptions(categories, selects[selectKey]);
                        selectKey++;
                    }
                });
                selectKey--;

                if (selectKey < 2) {
                    for (var i = selectKey + 1; i <= 2; i++) {
                        $(selects[i]).html('');
                    }
                }

                $(selects[selectKey]).focus();
                $(selects[selectKey]).val(categoryValue).prop("selected", true);

                if (selectedCategoryIsLeaf) {
                    CategorySelect.selectedCategory = categoryValue;
                    CategorySelect.buildCategoryTree(categoryValue, selectedCategoryName);
                    $(CategorySelect.target).find('input[name=categoryTree]').val($.stringify(CategorySelect.categoriesTree.reverse())).change();
                    categoryChanged = true;
                } else {
                    $(selects[selectKey]).find("option:selected").trigger('click');
                }

                //CategorySelect.fixWidth();

                if (params.success !== undefined && typeof params.success === 'function') {
                    params.success(categoryChanged);
                }
            }
        });
    },

    fixWidth: function() {
        var parentWidth = $('#categoryHandSelect').parent().actual('innerWidth'),
            selects = $('#categoryHandSelect').find('li'),
            selectsWidth = 0,
            selectsNormalWidth = 0;

        $(selects).each(function() {
            selectsWidth += $(this).actual('outerWidth', {
                includeMargin: true
            });
            selectsNormalWidth += $(this).actual('width');
        });

        if (selectsWidth >= parentWidth) {
            var fixWidthBy = Math.ceil(((selectsWidth - parentWidth) / selects.length));
            var marginFix = Math.ceil(((selectsWidth - selectsNormalWidth) / selects.length));
            $(selects).each(function() {
                var newWidth = $(this).actual('width') - (fixWidthBy + marginFix);
                $(this).find('select').width(newWidth);
            });
        }
    },

    events: {
        changeCategory: function(event, productChange) {
            var categoryValue = parseInt($(event.target).val());

            if (productChange === true || categoryValue != parseInt(CategorySelect.selectedCategory)) {
                CategorySelect.changeCategory({
                    category: categoryValue,
                    success: function(categoryChanged) {
                        if (categoryChanged) {

                        }
                    }
                });
            }
        },

        changeSelect: function(event) {
            if (event.target !== undefined) {
                var target = event.target;
                if (target.tagName == "SELECT") {
                    target = $(target).children('option:selected');
                }

                var selectedCategory = $(target).parent().val(),
                    parentCategory = $(target).data('parent');

                if ($(target).data('isLeaf') > 0) {
                    CategorySelect.categoriesParents[selectedCategory] = parentCategory;
                    CategorySelect.loadCategories(target, selectedCategory, true);
                } else {
                    if (CategorySelect.categoriesCache[selectedCategory] === undefined) {
                        $.ajax({
                            type: 'GET',
                            dataType: 'json',
                            url: base_uri('ajax/offers-categories/child?id=' + selectedCategory),
                            success: function(data) {
                                var categories = [];
                                $.each(data.children, function(key, data) {
                                    var isLeaf = true;

                                    if (data.childsCount > 0) {
                                        isLeaf = false;
                                    }

                                    CategorySelect.categoriesNames[data.id] = data.name;
                                    categories.push({
                                        id: data.id,
                                        name: data.name,
                                        isLeaf: isLeaf,
                                        parent: selectedCategory,
                                        prevParent: parentCategory
                                    });
                                });

                                CategorySelect.categoriesCache[selectedCategory] = categories;
                                CategorySelect.categoriesParents[selectedCategory] = parentCategory;
                                CategorySelect.loadCategories(target, selectedCategory, false);
                            }
                        });
                    } else {
                        CategorySelect.loadCategories(target, selectedCategory, false);
                    }
                }
            }
        }
    }
};