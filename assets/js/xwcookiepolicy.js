// Getting, Setting and Deleteing cookies.
// Author: Cezary Tomczak [www.gosu.pl]
// Note: name cannot contain 2 chars: =;
function Cookie() {
    this.get = function(name) {
        var cookies = document.cookie.split(";");
        for (var i = 0; i < cookies.length; ++i) {
            var a = cookies[i].split("=");
            if (a.length == 2) {
                a[0] = a[0].trim();
                a[1] = a[1].trim();
                if (a[0] == name) {
                    return unescape(a[1]);
                }
            }
        }
        return "";
    };
    this.set = function(name, value, seconds, path) {
        var cookie = (name + "=" + escape(value));
        if (seconds) {
            var date = new Date(new Date().getTime()+seconds*1000);
            cookie += ("; expires="+date.toGMTString());
        }
        cookie += (path    ? "; path="+path : "");
        document.cookie = cookie;
    };
    this.del = function(name, path) {
        var cookie = (name + "=");
        cookie += (path    ? "; path="+path : "");
        cookie += "; expires=Thu, 01-Jan-70 00:00:01 GMT";
        document.cookie = cookie;
    };
}
/**
* Example of initialization
*
* window.onload = function() {
* 	xCookiePolicy.theme.backgroundColor = '373737';
* 	xCookiePolicy.theme.textColor = 'fff';
* 	xCookiePolicy.theme.linkColor = '00A8FF';
* 	xCookiePolicy.theme.opacity = '0.85';
* 	xCookiePolicy.theme.imgPath = '/public/design/frontend/images/';
* 	xCookiePolicy.theme.position = 'top';
* 	xCookiePolicy.init();
* }
*/
var xCookiePolicy = {
    cookie: "",
	alreadyClicked: false,
    storedName: "xCookiePolicy",
	browser: "",
	theme: {
		backgroundColor: '3d2c24',
		textColor: 'fff',
		linkColor: 'F2E2AC',
		opacity: '0.95',
		imgPath: 'public/design/frontend/images/',
		position: 'bottom',
		zindex: '1'
	},
	url: {
		'Chrome': 'http://support.google.com/chrome/bin/answer.py?hl=pl&answer=95647',
		'Firefox': 'http://support.mozilla.org/pl/kb/W%C5%82%C4%85czanie%20i%20wy%C5%82%C4%85czanie%20obs%C5%82ugi%20ciasteczek',
		'Opera': 'http://help.opera.com/Linux/9.60/pl/cookies.html',
		'Safari': 'http://support.apple.com/kb/ph5042',
		'MSIE': 'http://windows.microsoft.com/pl-pl/windows7/how-to-manage-cookies-in-internet-explorer-9'
	},
    init: function () {
		this.detect_browser();
			
		//setup cookie manager
        this.cookie = new Cookie();
		
		//check & add info
		this.add_info();
		
		//add onclick bind
        if ( !this.alreadyClicked ) {
    		var btn = document.getElementById('cookieDismiss');
    		if (btn.addEventListener) {
    			// DOM2 standard
    			btn.addEventListener('click', this.hide_info, false);
    		}
    		else if (btn.attachEvent) {
    			// IE (IE9 finally supports the above, though)
    			btn.attachEvent('onclick', this.hide_info);
    		}
    		else {
    			// Really old or non-standard browser, try DOM0
    			btn.onclick = this.hide_info;
    		}
        }
    },
	add_info: function() {
	
		if ( this.supportLocalStorage() ) {
			if ( localStorage.getItem(this.storedName) ) {
				this.alreadyClicked = true;
			}
		} else if( this.cookie.get(this.storedName) ) {
			this.alreadyClicked = true;
		}
		
		if ( !this.alreadyClicked ) {
			var template = '<div id="cookiePolicy" style=" padding:0.5em; text-align:center; position:fixed; '+this.theme.position+':0; width:100%; background:#'+this.theme.backgroundColor+'; opacity:'+this.theme.opacity+'; color:#'+this.theme.textColor+'; z-index:'+this.theme.zindex+'"><p style="margin:0; padding:0 2em; font-size:12px;"><a style="float:right; margin:0.5em 0 0 1em; padding-left:17px; background:url('+this.theme.imgPath+'/close.png) no-repeat 0px 0px; color:#'+this.theme.linkColor+'; text-decoration:none;" id="cookieDismiss" href="#">Rozumiem, zamknij to okno</a></p>\
			<p style="padding:0 2em; margin:0.5em auto; font-size:12px;">Aby świadczyć naszym Klientom usługi na najwyższym poziomie, w ramach naszego serwisu stosujemy pliki cookies zgodnie z Polityką prywatności i wykorzystywania plików cookies. Korzystanie z serwisu bez zmiany ustawień dotyczących cookies oznacza, że będą one zamieszczane w urządzeniu końcowym użytkowników. W każdej chwili istnieje możliwość zmiany ustawień dotyczących cookies.</p></div>';
			document.body.insertAdjacentHTML('afterbegin', template.replace('{settings}', this.url[this.browser]));
		}
	},
	hide_info: function() {
		var val = new Date().getTime();

		if (xCookiePolicy.supportLocalStorage()) {
			localStorage.setItem(xCookiePolicy.storedName, val);
		} else {
			xCookiePolicy.cookie.set(xCookiePolicy.storedName, val, 3650, '/');
		}

		var elem = document.getElementById('cookiePolicy');
		elem.parentNode.removeChild(elem);
		
		return false;
	},
	detect_browser: function() {
		var N= navigator.appName, ua= navigator.userAgent, tem;
		var M= ua.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);
		if(M && (tem= ua.match(/version\/([\.\d]+)/i))!= null) M[2]= tem[1];
		M = M ? [M[1]]: [N];
	   
	   this.browser = M;
	},
	supportLocalStorage: function() {
		try {
			return 'localStorage' in window && window['localStorage'] !== null;
		} catch(e){
			return false;
		}
	}
}