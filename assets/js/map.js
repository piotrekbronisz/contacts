(function($){
	
	$.fn.gmaps = function(options) {
		var d = {
			show: 'fade',
            width: 225,
            height: 180
		}; // default settings
		
		var s = $.extend({}, d, options); 
        var included = false;
        
		return this.each(function(){
            
            var $t = $(this);
            
            var data = $t.data();
            
            var routePoints = new Array(0);
            var radiusWidgets = new Array(0);
            
            var plugin = {
			    init: function(){
			        plugin.initSize(); 
                    plugin.createMap(data.lat, data.lng, data.zoom);
                    plugin.antiBug();
                    
                    var markerOptions = {title: data.title, content: data.content};
                    
                    if(data.draggable){
                        markerOptions.draggable = true;
                    }
                    
                    if(data.geocodeButton){
                        plugin.geocodeBtn(data.geocodeButton);
                    }
                    
                    if(data.lat && data.lng){
                        plugin.addMarker(data.lat, data.lng, markerOptions);    
                    }
                      
				},
        		createMap: function(lat, lng, zoom){  
                    var center_lat = lat, 
                        center_lng = lng;
                        
                    // tworzymy mapę satelitarną i centrujemy w okolicy Szczecina na poziomie zoom = 10
                    var wspolrzedne = new google.maps.LatLng(center_lat, center_lng);
                    var opcjeMapy = {
                        zoom: zoom,
                        center: wspolrzedne,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };
                    this.map = new google.maps.Map($t.get(0), opcjeMapy);     

                },
                
                initSize: function(){                    
                    if($t.width()<10){
                        $t.width(320);
                    }
                    
                    if($t.height()<10){
                        $t.height(240);
                    } 
                },
                                
                antiBug: function(){
                    
                    google.maps.event.addListener(this.map, "idle", function(){
                    	google.maps.event.trigger(this.map, 'resize'); 
                    });	
                    
                    this.map.setZoom( this.map.getZoom() - 1);
                    this.map.setZoom( this.map.getZoom() + 1);  
                },
                setValues: function(position){
                    $(data.latInput).val( position.lat() );
                    $(data.lngInput).val( position.lng() );
                    data.lat = position.lat();
                    data.lng = position.lng();
                },
                setZoom: function(zoom){
                    if(data.zoomInput){
                        $(data.zoomInput).val( zoom );
                        data.zoom = zoom;
                    }
                    
                },
                
                
                geocode: function(address){
                    
                    var geokoder = new google.maps.Geocoder();
                    geokoder.geocode({address: address}, function(results, status){
                        if(status == google.maps.GeocoderStatus.OK){
                            
                            plugin.marker.setPosition(results[0].geometry.location);
                            plugin.setValues(results[0].geometry.location);
                            plugin.map.setCenter(results[0].geometry.location);
                            plugin.map.setZoom(12);

                        }else{
                            alert('Nie znaleziono adresu: '+address);
                        }
                    });
                    
                },
                
                geocodeBtn: function(target){
                   
                   $(target).click(function(){
                        var btn = $(this);
                        //alert('btn');
                        var address = '';
                        
                        if(btn.data('city')){
                            address += $(btn.data('city')).val();
                        }
                        if(btn.data('address')){
                            
                            if($(btn.data('address')).val()){
                                if(address){
                                    address += ',';
                                }
                                address += $(btn.data('address')).val();
                            }
                            
                        }
                        
                        if(address == ''){
                            alert('Nie podano adresu');
                        }else{
                            plugin.geocode(address);    
                        }

                        return false;
                   });
                    
                },
                
                addMarker: function(lat, lng, markerOptions){
                    markerOptions.position = new google.maps.LatLng(lat,lng);
                    markerOptions.map = this.map;
                    
                    plugin.marker = new google.maps.Marker(markerOptions);
                    
                    if(markerOptions.text){ 
                        var dymek = new google.maps.InfoWindow({
                            content: markerOptions.content
                        });

                        google.maps.event.addListener(plugin.marker, "click", function(){
                            dymek.open(plugin.map, plugin.marker);
                        });
                        //google.maps.event.trigger(marker, 'click');
                    }
                    
                    if(markerOptions.draggable){
                        google.maps.event.addListener(plugin.marker, "dragend", function(event) {
                            plugin.setValues(plugin.marker.getPosition());
                        });
                    }
                    
                    google.maps.event.addListener(this.map, "click", function(event) {
                        if (data.radius) {
                            var givenRad = $(data.radiusInput).val();
                            var colour = "#FFFFFF";
                            var fillcolour = "#00FF00";
                            var thick = 1;
                            
                            distanceWidget = new DistanceWidget(this.map, event.latLng, givenRad, colour, thick, 1, fillcolour);
                            radiusWidgets.push(distanceWidget);
                            routePoints.push(event.latLng);
                            
                            google.maps.event.addListener(distanceWidget, 'distance_changed', function () {
                                //displayInfo(distanceWidget)
                            });
                            
                            google.maps.event.addListener(distanceWidget, 'position_changed', function () {
                                //displayInfo(distanceWidget)
                            });
                        }
                        else {        
                            plugin.marker.setPosition(event.latLng);
                            //plugin.map.setCenter(event.latLng);
                            plugin.setValues(event.latLng);
                        }
                    });
                    
                    google.maps.event.addListener(this.map, "zoom_changed", function(){
                        plugin.setZoom( plugin.map.getZoom() );
                    });
                    
   
                }
        	};
			plugin.init();
			
		});
	}
	
})(jQuery); 